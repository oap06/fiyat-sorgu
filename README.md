## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Tue Feb 22 2022 10:03:29 GMT+0300 (GMT+03:00)|
|**App Generator**<br>@sap/generator-fiori-freestyle|
|**App Generator Version**<br>1.5.0|
|**Generation Platform**<br>Visual Studio Code|
|**Floorplan Used**<br>simple|
|**Service Type**<br>SAP System (ABAP On Premise)|
|**Service URL**<br>http://sapfioridev.dogtaskelebek.com:8000//sap/opu/odata/sap/ZCRM_PRODUCT_SRV
|**Module Name**<br>zcrm_inquire_price|
|**Application Title**<br>App Title|
|**Namespace**<br>zcrm|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>1.65.0|
|**Enable Code Assist Libraries**<br>False|
|**Add Eslint configuration**<br>False|
|**Enable Telemetry**<br>True|

## zcrm_inquire_price

A Fiori application.

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```

#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


