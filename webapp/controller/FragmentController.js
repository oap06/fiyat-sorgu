sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/core/BusyIndicator"
], function (JSONModel, Filter, FilterOperator,BusyIndicator) {
	"use strict";

	function FragmentController() {}

	Object.assign(FragmentController.prototype, {
		setDialog: function (oDialog) {
			this.oDialog = oDialog;
		},
		setView: function (oView) {
			this.oView = oView;
		},
		setModel: function (oModel, sModelName) {
			this.getDialog().setModel(oModel, sModelName);
		},
		setSource: function (oControl) {
			//eslint-disable-next-line sap-no-ui5base-prop
			this.oSource = oControl;
		},
		getOwnerComponent: function () {
			return this.getView().getController().getOwnerComponent();
		},
		getView: function () {
			return this.oView;
		},
		getSource: function () {
			//eslint-disable-next-line sap-no-ui5base-prop
			return this.oSource;
		},
		getDialog: function () {
			return this.oDialog;
		},
		log: function (oEvent) {
			// eslint-disable-next-line
			console.log("log triggered");
		},
		open: function (sFragmentName) {
			return this.getView().getController().open(sFragmentName);
		},
		onClose: function (oEvent) {
			this.oDialog.close();
		},
		getOwnerModelProperty: function (oModel, sPath) {
			return this.getOwnerComponent().getModel(oModel).getProperty(sPath);
		},
		setOwnerModelProperty: function (oModel, sPath, value) {
			return this.getOwnerComponent().getModel(oModel).setProperty(sPath, value);
		},
		onCloseCat:function(oEvent){
			this.oDialog.close();
			this.getView().getController().onSave(false);
		},

		handleProdSearch: function (oEvent) {

			var oModelProd = this.getView().getModel("PROD");

			var oProdId = sap.ui.getCore().byId("idProdId").getValue();
			var oProdDes = sap.ui.getCore().byId("idProdDesc").getValue();
			var oCatId = sap.ui.getCore().byId("idCatId").getValue();
			var oCatDes = sap.ui.getCore().byId("idCatDesc").getValue();
			var oRow = sap.ui.getCore().byId("idProdRows").getValue();

			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];
			if (oProdId !== "") {
				var oFilter = new Filter("ProductId", FilterOperator.EQ, oProdId);
				oFilters.push(oFilter);
			}
			if (oProdDes !== "") {
				oFilter = new Filter("ShortText", FilterOperator.EQ, oProdDes);
				oFilters.push(oFilter);
			}
			if (oCatId !== "") {
				oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
				oFilters.push(oFilter);
			}
			if (oCatDes !== "") {
				oFilter = new Filter("CategoryDesc", FilterOperator.EQ, oCatDes);
				oFilters.push(oFilter);
			}
			if (oRow !== "") {
				oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
				oFilters.push(oFilter);
			}

			var oModelSearch = this.getOwnerComponent().getModel("search");
			oBusyDialog.open();
			oModelProd.read("/productSearchSet", {
				filters: oFilters,
				success: function (oData, response) {
					oModelSearch.setData({
						oProd: oData.results
					});
					oBusyDialog.close();
				},
				error: function (oError) {
					oBusyDialog.close();
				}
			});

		},
		handleProdListItemPress: function (oEvent) {
			var oInd = oEvent.getParameter("listItem").getBindingContextPath().split("/")[2];
			var oProd = sap.ui.getCore().byId("ProdTabId").getItems()[oInd].getCells()[0].getText();
			this.getSource().setValue(oProd);
			this.onClose();
		},
		onCatValueHelp: function (oEvent) {
			var oModelSearch = this.getOwnerComponent().getModel("search");
			var oModelHeaderSearch = this.getOwnerComponent().getModel("headersearch");
			oModelSearch.setData({});
			oModelHeaderSearch.setData({});

			this.getView().getController().open("CatSearch").setSource(oEvent.getSource());

		},
		handleCatSearch: function () {

			var oModelProd = this.getView().getModel("PROD");

			var oCatId = sap.ui.getCore().byId("idCategId").getValue();
			var oCatDes = sap.ui.getCore().byId("idCategDesc").getValue();
			var oHyrId = sap.ui.getCore().byId("idHyrType").getSelectedKey();
			var oHyrDes = sap.ui.getCore().byId("idHyrDes").getValue();
			var oRow = sap.ui.getCore().byId("idCatRows").getValue();

			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];
			if (oCatId !== "") {
				var oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
				oFilters.push(oFilter);
			}
			if (oCatDes !== "") {
				oFilter = new Filter("CategoryText", FilterOperator.EQ, oCatDes);
				oFilters.push(oFilter);
			}
			if (oHyrId !== "") {
				oFilter = new Filter("HierarchyId", FilterOperator.EQ, oHyrId);
				oFilters.push(oFilter);
			}
			if (oHyrDes !== "") {
				oFilter = new Filter("HierarchyText", FilterOperator.EQ, oHyrDes);
				oFilters.push(oFilter);
			}
			if (oRow !== "") {
				oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
				oFilters.push(oFilter);
			}

			// var oModelJsonList = new sap.ui.model.json.JSONModel();
			var oModelSearch = this.getOwnerComponent().getModel("search");
			// var that = this;
			oBusyDialog.open();
			oModelProd.read("/categorySearchSet", {
				filters: oFilters,
				success: function (oData, response) {
					oModelSearch.setData({
						oCat: oData.results
					});
					// oModelJsonList.setData(oData);
					// that._oCatHelpDialog.setModel(oModelJsonList, "oCat");
					oBusyDialog.close();
				}.bind(this),
				error: function (oError) {
					oBusyDialog.close();
				}
			});

		},

		handleCatListItemPress: function (oEvent) {
			var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
			var oPartner = sap.ui.getCore().byId("CatTabId").getItems()[oInd].getCells()[0].getText();
			this.getOwnerComponent().getModel("headersearch").setData({
				CatId: oPartner
			});
			this.onClose();
		},

		handleCustomerSearch: function () {

			var oModelCust = this.getView().getModel("CUST");

			var oPartner = sap.ui.getCore().byId("idCustIdent").getValue();
			var oAd = sap.ui.getCore().byId("idCustName").getValue();
			var oSoyad = sap.ui.getCore().byId("idCustLastName").getValue();
			var oTel = sap.ui.getCore().byId("idCustTel").getValue();
			var oTur = sap.ui.getCore().byId("idCustType").getSelectedKey();
			var oTc = sap.ui.getCore().byId("idCustTc").getValue();
			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];
			if (oPartner !== "") {
				var oFilter = new Filter("Partner", FilterOperator.EQ, oPartner);
				oFilters.push(oFilter);
			}
			if (oAd !== "") {
				oFilter = new Filter("Firstname", FilterOperator.EQ, oAd);
				oFilters.push(oFilter);
			}
			if (oSoyad !== "") {
				oFilter = new Filter("Lastname", FilterOperator.EQ, oSoyad);
				oFilters.push(oFilter);
			}
			if (oTel !== "") {
				oFilter = new Filter("Telephonemob", FilterOperator.EQ, oTel);
				oFilters.push(oFilter);
			}
			if (oTur !== "") {
				oFilter = new Filter("Partnertype", FilterOperator.EQ, oTur);
				oFilters.push(oFilter);
			}

			if (oTc !== "") {
				oFilter = new Filter("Tckn", FilterOperator.EQ, oTc);
				oFilters.push(oFilter);
			}
			var oModelSearch = this.getOwnerComponent().getModel("search");
			oBusyDialog.open();
			oModelCust.read("/searchCustomerSet", {
				filters: oFilters,
				success: function (oData, response) {
					oModelSearch.setData({
						oCustomer: oData.results
					});
					oBusyDialog.close();
				}.bind(this),
				error: function (oError) {
					oBusyDialog.close();
				}
			});

		},

		handleCustomerListItemPress: function (oEvent) {
			var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
			var oPartner = sap.ui.getCore().byId("CustTabId").getItems()[oInd].getCells()[0].getText();
			var oPartnerName = sap.ui.getCore().byId("CustTabId").getItems()[oInd].getCells()[1].getText();
			this.getView().byId("inpu2").setValue(oPartner);
			this.getView().byId("inpu201").setValue(oPartnerName);
			this.onClose();
		},
		handleCustEnter: function () {
			var oPartner = this.getView().byId("inpu2").getValue();
			var oModelCust = this.getView().getModel("CUST");
			var oFilters = [];

			if (oPartner !== "") {
				var oFilter = new Filter("Partner", FilterOperator.EQ, oPartner);
				oFilters.push(oFilter);
			}

			var that = this;

			oModelCust.read("/searchCustomerSet", {
				filters: oFilters,
				success: function (oData, response) {
					if (oData.results.length > 0) {
						var oPartnerName = oData.results[0].Firstname + " " + oData.results[0].Lastname;
						that.getView().byId("inpu201").setValue(oPartnerName);
					} else {
						that.getView().byId("inpu201").setValue("");
					}
				}

			});

		},
		handleCreateCustomer: function (oEvent) {
			var oCore = sap.ui.getCore();

			this.open("CreateCustomer").setSource(oEvent.getSource());
			// if (!this.CustomerCreate) {
			// 	this.CustomerCreate = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.CreateCustomer", this);
			// 	this.getView().addDependent(this.CustomerCreate);
			// 	this.CustomerCreate.open();
			// } else {
			// 	this.CustomerCreate.open();
			// }
			oCore.byId("idCustomerType").setSelectedKey("1");
			oCore.byId("idCustomerTax").setVisible(false);
			oCore.byId("idCustomerTaxNo").setVisible(false);
			oCore.byId("idCustCompanyName").setVisible(false);

			var oModel = this.getView().getModel();
			var oView = this.getView();
			var oFilters = [];
			var oFilter = {};
			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0012");
			oFilters.push(oFilter);
			oFilter = new Filter("Extension", FilterOperator.EQ, "1");
			oFilters.push(oFilter);

			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList8 = new sap.ui.model.json.JSONModel(oData);
					this.getOwnerComponent().setModel(oModelJsonList8, "oCustGrup");
				}.bind(this),
				error: function (oError) {}
			});

			var oModelCust = this.getView().getModel("CUST");

			oModelCust.read("/searchHelpCountrySet", {
				success: function (oData, response) {
					var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList.setData(oData);
					oModelJsonList.setSizeLimit(500);
					this.getOwnerComponent().setModel(oModelJsonList, "oUlke");
				}.bind(this)
			});

			oFilters = [];
			oFilter = new Filter("Land1", FilterOperator.EQ, "TR");
			oFilters.push(oFilter);
			oModelCust.read("/searchHelpRegionSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList.setData(oData);
					this.getOwnerComponent().setModel(oModelJsonList, "oSehir");
				}.bind(this)
			});

		},
		onCustomerCreateSave: function () {
			var oModelCust = this.getView().getModel("CUST");
			var oCore = sap.ui.getCore();

			if (this.custAlanKontrol()) {
				var oCustomer = {};

				oCustomer.Lifecyclestage = oCore.byId("idCustomerRole").getSelectedKey();
				oCustomer.BuGroup = oCore.byId("idGroup").getSelectedKey();
				oCustomer.Partnertype = oCore.byId("idCustomerType").getSelectedKey();

				if (oCustomer.Partnertype === "1") // Bireysel müşteri
				{
					oCustomer.Firstname = oCore.byId("idCustomerName").getValue();
					oCustomer.Lastname = oCore.byId("idCustomerSurName").getValue();
					oCustomer.TaxNum = oCore.byId("idCustomerID").getValue();
				} else {
					oCustomer.TaxNum = oCore.byId("idCustomerTaxNo").getValue();
					oCustomer.Name1 = oCore.byId("idCustCompanyName").getValue();
					oCustomer.TaxCenter = oCore.byId("idCustomerTax").getValue();
				}

				var oAdres = {};
				oAdres.StrSuppl1 = oCore.byId("idCustomerAdres").getValue();
				oAdres.StrSuppl3 = oCore.byId("idCustomerAdres2").getValue();
				oAdres.Telephonemob = oCore.byId("idCustMobileNumber").getValue();
				oAdres.Country = oCore.byId("idCustomerCountry").getSelectedKey();
				oAdres.Region = oCore.byId("idCustomerCity").getSelectedKey();
				oAdres.CityNo = oCore.byId("idCustomerDist").getSelectedKey();

				oCustomer.ToAddress = oAdres;

				var oPermission = [];

				oPermission.push({
					Partner: "",
					Zzfld00003iTxt: "Hepsi",
					Zzfld00003i: "HEP",
					Zzfld00003jTxt: "Verilmedi",
					Zzfld00003j: "002",
					Zzfld00003k: "",
					Zzfld00003l: new Date(),
					Zzfld00003m: "",
					Zzfld00003n: ""
				});

				oCustomer.ToPermissionSet = oPermission;

				var that = this;
				var oBusyDialog = new sap.m.BusyDialog();

				oBusyDialog.open();
				oModelCust.create("/customerHeaderSet", oCustomer, {
					success: function (oData, oResponse) {
						// oModelJsonList.setData(oData);
						that.getView().byId("inpu2").setValue(oData.Partner);
						if (oData.FirstName !== "") // Bireysel müşteri
						{
							that.getView().byId("inpu201").setValue(oCustomer.Firstname + oCustomer.Lastname);
						} else {
							that.getView().byId("inpu201").setValue(oCustomer.Name1);
						}
						oBusyDialog.close();
						this.onClose();
						this.getView().getController().showMessageBox("Müşteri yaratıldı");
					}.bind(this),
					error: function (oError) {
						oBusyDialog.close();
						this.getView().getController().showMessageBox(oError);
						// sap.m.MessageBox.show(message, {
						// 	icon: sap.m.MessageBox.Icon.ERROR,
						// 	title: "HATA",
						// 	actions: [sap.m.MessageBox.Action.OK]
						// });
					}.bind(this)
				});

			} else {
				var oMessage = "Lütfen Tüm Zorunlu Alanları Doldurunuz";
				this.getView().getController().showMessageBox(oMessage,"warning");
				// sap.m.MessageBox.show(oMessage, {
				// 	icon: sap.m.MessageBox.Icon.ERROR,
				// 	title: "UYARI",
				// 	actions: [sap.m.MessageBox.Action.OK]
				// });
			}

		},

		custAlanKontrol: function () {
			var oCore = sap.ui.getCore();
			var oVal = true;

			// if (oCore.byId("idGroup").getSelectedKey() === "" && oCore.byId("idGroup").getVisible()) {
			// 	oCore.byId("idGroup").setValueState(sap.ui.core.ValueState.Error);
			// 	oVal = false;
			// } else {
			// 	oCore.byId("idGroup").setValueState(sap.ui.core.ValueState.None);
			// }

			if (oCore.byId("idCustomerRole").getSelectedKey() === "" && oCore.byId("idCustomerRole").getVisible()) {
				oCore.byId("idCustomerRole").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerRole").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustomerName").getValue() === "" && oCore.byId("idCustomerName").getVisible()) {
				oCore.byId("idCustomerName").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerName").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustomerSurName").getValue() === "" && oCore.byId("idCustomerSurName").getVisible()) {
				oCore.byId("idCustomerSurName").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerSurName").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustCompanyName").getValue() === "" && oCore.byId("idCustCompanyName").getVisible()) {
				oCore.byId("idCustCompanyName").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustCompanyName").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustMobileNumber").getValue() === "" && oCore.byId("idCustMobileNumber").getVisible()) {
				oCore.byId("idCustMobileNumber").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustMobileNumber").setValueState(sap.ui.core.ValueState.None);
			}

			return oVal;
		},

		onCountryChange: function () {
			var oView = this.getView();
			var oModelCust = this.getView().getModel("CUST");
			var oCore = sap.ui.getCore();
			var oCountry = oCore.byId("idCustomerCountry").getSelectedKey();
			var oFilters = [];
			var oFilter = {};

			oCore.byId("idCustomerCity").setSelectedKey("");
			oCore.byId("idCustomerDist").setSelectedKey("");

			oFilters = [];
			oFilter = new Filter("Land1", FilterOperator.EQ, oCountry);
			oFilters.push(oFilter);

			oModelCust.read("/searchHelpRegionSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList.setData(oData);
					this.getOwnerComponent().setModel(oModelJsonList, "oSehir");
				}.bind(this)
			});

		},
		onCityChange: function () {
			var oView = this.getView();
			var oModelCust = this.getView().getModel("CUST");
			var oCore = sap.ui.getCore();
			var oCountry = oCore.byId("idCustomerCountry").getSelectedKey();
			var oCity = oCore.byId("idCustomerCity").getSelectedKey();

			var oFilters = [];
			var oFilter = {};

			oCore.byId("idCustomerDist").setSelectedKey("");

			oFilters = [];
			oFilter = new Filter("Country", FilterOperator.EQ, oCountry);
			oFilters.push(oFilter);
			oFilter = new Filter("Region", FilterOperator.EQ, oCity);
			oFilters.push(oFilter);

			oModelCust.read("/searchHelpCitySet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList.setData(oData);
					this.getOwnerComponent().setModel(oModelJsonList, "oIlce");
				}.bind(this)
			});

		},
		onCustomerTypeChange: function () {
			var oCore = sap.ui.getCore();

			var oSelected = oCore.byId("idCustomerType").getSelectedKey();

			if (oSelected === "1") {
				oCore.byId("idCustomerName").setVisible(true);
				oCore.byId("idCustomerSurName").setVisible(true);
				oCore.byId("idCustomerID").setVisible(true);

				oCore.byId("idCustomerTax").setVisible(false);
				oCore.byId("idCustomerTaxNo").setVisible(false);
				oCore.byId("idCustCompanyName").setVisible(false);
			} else if (oSelected === "2") {
				oCore.byId("idCustomerName").setVisible(false);
				oCore.byId("idCustomerSurName").setVisible(false);
				oCore.byId("idCustomerID").setVisible(false);

				oCore.byId("idCustomerTax").setVisible(true);
				oCore.byId("idCustomerTaxNo").setVisible(true);
				oCore.byId("idCustCompanyName").setVisible(true);
			}

		},	onTakim: function (oEvent) {
			this.setOwnerModelProperty("selected", "/Model", []);
			this.setOwnerModelProperty("selected", "/KatalogModel", "");
			var oCode = "0021";
			var oExtension = oEvent.getSource().getSelectedKey();
			this.getView().getController().BaseValueHelp("/valueHelpSet", "/KatalogModel", {
		     Code: oCode,
		    Extension: oExtension
		  });
		},
		handleSelectCatSearch: function (oEvent) {
			debugger;

			var oModelProd = this.getView().getModel("PROD");

			var oHead = this.getView().getModel("oModel").getProperty("/");
			var oTakim = this.getOwnerModelProperty("selected", "/KatalogTakım");
			var KatalogModel = this.getOwnerModelProperty("selected", "/KatalogModel");
			var oModelTnm = this.getOwnerModelProperty("selected", "/KatalogModelTnm");
			var oUniteUrunKod = this.getOwnerModelProperty("selected", "/KatalogUniteUrunKod");
			var oUrunId = this.getOwnerModelProperty("selected", "/KatalogUrunId");
			var oCategId = this.getOwnerModelProperty("selected", "/CategId");
			var oConfigEnable = this.getOwnerModelProperty("selected", "/ConfigEnable");

			var oFilters = [];

			if (oTakim) {
				oFilters.push(new Filter({
					path: 'Zztakim',
					value1: oTakim,
					operator: FilterOperator.EQ
				}));
			}

			if (oModelTnm) {
				oFilters.push(new Filter({
					path: 'CategoryDesc',
					value1: oModelTnm,
					operator: FilterOperator.EQ
				}));
			}
			if (oUniteUrunKod) {
				oFilters.push(new Filter({
					path: 'Description',
					value1: oUniteUrunKod,
					operator: FilterOperator.EQ
				}));
			}
			if (KatalogModel) {
				oFilters.push(new Filter({
					path: 'Zzmodel',
					value1: KatalogModel,
					operator: FilterOperator.EQ
				}));
			}
			if (oUrunId) {
				oFilters.push(new Filter({
					path: 'ProductId',
					value1: oUrunId,
					operator: FilterOperator.EQ
				}));
			}
			if (oCategId) {
				oFilters.push(new Filter({
					path: 'CategoryId',
					value1: oCategId,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ProcessType) {
				oFilters.push(new Filter({
					path: 'ProcessType',
					value1: oHead.ProcessType,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceList) {
				oFilters.push(new Filter({
					path: 'PriceList',
					value1: oHead.ToPricing.PriceList,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.Currency) {
				oFilters.push(new Filter({
					path: 'Currency',
					value1: oHead.ToPricing.Currency,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceDate) {
				oFilters.push(new Filter({
					path: 'PriceDate',
					value1: oHead.ToPricing.PriceDate,
					operator: FilterOperator.EQ
				}));
			}

			BusyIndicator.show();
			oModelProd.read("/productCatalogSearchSet", {
				filters: oFilters,
				success: function (oData, response) {
					this.setOwnerModelProperty("search", "/oCatalog", oData.results);
					sap.ui.getCore().byId("SelProdCatTabId").removeSelections();
					BusyIndicator.hide();
				}.bind(this),
				error: function (oError) {
					BusyIndicator.hide();
				}
			});

		},
		handleSelectCatAdd: function (oEvent) {

			var oTab = this.getView().byId("SepetTabId");
			var List = oTab.getModel("oModel").getProperty("/ToItems/results");
			var oSelProdTab = sap.ui.getCore().byId("SelProdCatTabId");
			var oSelected = oSelProdTab.getSelectedItems();

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oConfig = oSelProdTab.getItems()[oInd].getCells()[0].getText();
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[4].getText();
				// var oIndirim = oSelProdTab.getItems()[oInd].getCells()[5].getText();

				var oRow = {
					OrderedProd: oUrunKod,
					Description: "",
					Quantity: "1.000",
					ProcessQtyUnit: "ADT",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: "",
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: "",
					ShowConfigButton: oConfig,
					// Zzfld000083: oKalemNo,
					// ZzaTeshirblgno: oSipNo,
					// ZzTeshirind: oIndirim
				};

				List.push(oRow);

			}

			oTab.getModel("oModel").setProperty("/ToItems/results", List);
			sap.ui.getCore().byId("SelProdCatTabId").removeSelections();
			oTab.getModel("oModel").refresh();
			//			this._oSelProdCatHelpDialog.close();
		},
		handleSelectProdTree: function (oEvent) {

			// Ürün ağacından seç arama yardımı burada ürün katoloğundan seçilen satırlar kullanıarak ürün ağacı getiririlir.
			var oModelProd = this.getView().getModel("PROD");
			var oHead = this.getView().getModel("oModel").getProperty("/");
			var oSelProdTab = sap.ui.getCore().byId("SelProdCatTabId");
			var oSelected = oSelProdTab.getSelectedItems();
			var oFilterA = [];
			var oFilterProd = [];

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[4].getText();
				if (oUrunKod) {
					oFilterProd.push(new Filter({
						path: 'ProductId',
						value1: oUrunKod,
						operator: FilterOperator.EQ,
						and: false
					}));
				}
			}

			var oFilterOR = new Filter({
				filters: oFilterProd,
				and: false,
			});

			if (oHead.ProcessType) {
				oFilterA.push(new Filter({
					path: 'ProcessType',
					value1: oHead.ProcessType,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceList) {
				oFilterA.push(new Filter({
					path: 'PriceList',
					value1: oHead.ToPricing.PriceList,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.Currency) {
				oFilterA.push(new Filter({
					path: 'Currency',
					value1: oHead.ToPricing.Currency,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceDate) {
				oFilterA.push(new Filter({
					path: 'PriceDate',
					value1: oHead.ToPricing.PriceDate,
					operator: FilterOperator.EQ
				}));
			}

			var oFilterAND = new Filter({
				filters: oFilterA,
				and: true
			});
			var oFilters = [new Filter({
				and: true,
				filters: [oFilterOR, oFilterAND]

			})];

			var oBusyDialog = new sap.m.BusyDialog();
			var oModelJsonList = new sap.ui.model.json.JSONModel();
			BusyIndicator.show();
			oModelProd.read("/productCatalogTreeSearchSet", {
				filters: oFilters,
				success: function (oData, response) {
					this.setOwnerModelProperty("search", "/oProdTree", oData.results);
					BusyIndicator.hide();
				}.bind(this),
				error: function (oError) {
					BusyIndicator.hide();
				}
			});

			this.open("SelectFromProdTree").setSource(oEvent.getSource());

		},
		handleSelectProdTreeAdd: function (oEvent) {

			var oTab = this.getView().byId("SepetTabId");
			var List = oTab.getModel("oModel").getProperty("/ToItems/results");
			var oSelProdTab = sap.ui.getCore().byId("SelProdTreeTabId");
			var oSelected = oSelProdTab.getSelectedItems();

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[0].getText();
				var oText = oSelProdTab.getItems()[oInd].getCells()[1].getText();
				var oQuantity = oSelProdTab.getItems()[oInd].getCells()[5].getText();

				var oRow = {
					OrderedProd: oUrunKod,
					Description: oText,
					Quantity: oQuantity,
					ProcessQtyUnit: "",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: "",
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: "",
					ShowConfigButton: ""
				};
				List.push(oRow);
			}

			oTab.getModel("oModel").setProperty("/ToItems/results", List);
			oTab.getModel("oModel").refresh();
			this.onClose();
		},
	});

	return FragmentController;
});