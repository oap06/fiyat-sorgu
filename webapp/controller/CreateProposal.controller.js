sap.ui.define([
	"./BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox"
], function (BaseController, Filter, FilterOperator, MessageBox) {
	"use strict";
	var aFieldsToBeRemoved = [
		/^Zz*/,
		"ObjectId",
	];
	return BaseController.extend("zcrm.zcrm_inquire_price.controller.CreateProposal", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf zcrm.zcrm_inquire_price.view.CreateProposal
		 */
		onInit: function () {
			BaseController.prototype.onInit.apply(this, arguments);

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("CreateProposal").attachPatternMatched(this._onObjectMatched, this);
			sap.ui.model.SimpleType.extend("sap.ui.model.type.Boolean", {
				formatValue: function (oValue) {
					if (oValue === "X") {
						return true;
					}
					if (oValue === null || oValue === "") {
						return false;
					}
				},
				parseValue: function (oValue) {
					if (oValue === true) {
						return "X";
					} else if (oValue === false) {
						return "";
					}
				},
				validateValue: function (oValue) {
					return oValue;
				}
			});
			this.getView().addStyleClass("sapUiSizeCompact");
			// var oCode = "0020";
			// this.BaseValueHelp(oCode);
		},
		
		_onObjectMatched: function (oEvent) {
			debugger;
			this.oGuid = oEvent.getParameter("arguments").guid;
			this.oOrderType = oEvent.getParameter("arguments").orderType;
			var oModel = new sap.ui.model.json.JSONModel();
			var oModelGlobal = sap.ui.getCore().getModel("oModelGlobal");
			this.getView().setModel(oModel, "oModel");
			var guid = oModelGlobal.getProperty("/Guid");
			// var status = oModelGlobal.getProperty("/ToStatus/Status");
			var status = "E0002";
			oModel.setData(this.getOwnerComponent().resetModel(oModelGlobal.getObject("/"), {
				Guid: guid,
				ProcessType: this.oOrderType,
				ToStatus: {
					Status: status
				},
			}, 
				[ "ToCumulate", "ToItems", "ToPricing", "ToPartners", "ToTexts", "CreatedBy", "CrmRelease", "DescrLanguage", "LogicalSystem", 
				"ObjectType", "Zzfld000057", "Zzfld00008c", "Zzfld00008d","Zzfld00009e","ZzIndirimTutar", "ZzOdenenTutar", "ZzKalanTutar", "ZzPersiptop"]));

			this.disableElements(guid, status, this.oOrderType);
			this.bindElements(oModel.getObject("/"));
			this.bindSearchHelps(oModel.getObject("/"));
	/*		var oModel = sap.ui.getCore().getModel("oModelGlobal");
			this.getView().setModel(oModel, "oModel");
			var oData = oModel.oData;

			this.disableElements(oData.Guid, oData.ToStatus.Status, this.oOrderType);
			this.bindElements(oData);
			this.bindSearchHelps(oData);*/
		},

		onAfterRendering: function () {
			// oBusyDialog.open();
			// var oModel = sap.ui.getCore().getModel("oModelGlobal");
			// this.getView().setModel(oModel, "oModel");
			// var oData = oModel.oData;

			// this.disableElements(oData.Guid, oData.ToStatus.Status, this.oOrderType);
			// this.bindElements(oData);
			// this.bindSearchHelps(oData);
		},

		bindSearchHelps: function (oData) {
			var oModel = this.getView().getModel();
			var oView = this.getView();
			var oFilters = [];
			var oFilter = {};
			var oOdemeKosul = oView.byId("inpu19");
			var oFlist = oView.byId("inpu25");
			var oCalisan = oView.byId("inpu14");
			var oDurum = oView.byId("inpu17");

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0004");
			oFilters.push(oFilter);
			if (oData) {
				oFilter = new Filter("Guid", FilterOperator.EQ, oData.Guid);
				oFilters.push(oFilter);
			}
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList4 = new sap.ui.model.json.JSONModel(oData);
					oCalisan.setModel(oModelJsonList4, "oCalisan");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0005");
			oFilters.push(oFilter);
			if (oData) {
				oFilter = new Filter("Extension", FilterOperator.EQ, oData.ProcessType);
				oFilters.push(oFilter);
			}
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList5 = new sap.ui.model.json.JSONModel(oData);
					oOdemeKosul.setModel(oModelJsonList5, "oOdeme");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0007");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList7 = new sap.ui.model.json.JSONModel(oData);
					oFlist.setModel(oModelJsonList7, "oFlist");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0011");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList10 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList10, "oDepo");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0009");
			oFilters.push(oFilter);
			if (oData) {
				oFilter = new Filter("Extension", FilterOperator.EQ, oData.ProcessType);
				oFilters.push(oFilter);
				oFilter = new Filter("Guid", FilterOperator.EQ, oData.Guid);
				oFilters.push(oFilter);
			}

			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList8 = new sap.ui.model.json.JSONModel(oData);
					oDurum.setModel(oModelJsonList8, "oDurum");
					oDurum.setSelectedKey(oDurum.getSelectedKey());
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0015");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList11 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList11, "oOdemeTur");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0016");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList12 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList12, "oTahsTur");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0017");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList13 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList13, "oBanka");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0018");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList14 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList14, "oTahsDurum");
				},
				error: function (oError) {}
			});

			oModel.read("/unitSet", {
				success: function (oData, response) {
					var oModelJsonList15 = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList15.setSizeLimit(500);
					oView.setModel(oModelJsonList15, "oUnit");
				},
				error: function (oError) {}
			});

			oModel.read("/currencySet", {
				success: function (oData, response) {
					var oModelJsonList16 = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList16.setSizeLimit(500);
					oView.setModel(oModelJsonList16, "oCurr");
				},
				error: function (oError) {}
			});

		},

		disableElements: function (oGuid, oStat, oOrderType) {
			// var oView = this.getView();
			// oView.byId("inpu1").setEditable(false);
			// oView.byId("inpu3").setEditable(false);
			// oView.byId("inpu4").setEditable(false);
			// oView.byId("inpu25").setEditable(false);
			// oView.byId("inpu251").setEditable(false);
			// oView.byId("inpu26").setEditable(false);
			// oView.byId("inpu27").setEditable(false);
			// oView.byId("inpu28").setEditable(false);
			// oView.byId("inpu29").setEditable(false);
			// oView.byId("inpu35").setEditable(false);
			// oView.byId("inpu36").setEditable(false);
			// oView.byId("inpu37").setEditable(false);
			// oView.byId("inpu38").setEditable(false);
			// oView.byId("inpu39").setEditable(false);
			// oView.byId("inpu40").setEditable(false);
		},

		bindElements: function (oData) {
			var oView = this.getView();

			var oPartners = oData.ToPartners.results;
			var oTexts = oData.ToTexts.results;

			if (oPartners) {
				for (var i = 0; i < oPartners.length; i++) {
					if (oPartners[i].PartnerFct === "00000001") {
						oView.byId("inpu2").setValue(oPartners[i].PartnerNo);
						oView.byId("inpu201").setValue(oPartners[i].DescriptionName);
						oView.byId("inpu3").setValue(oPartners[i].AddressShort);
					}
					if (oPartners[i].PartnerFct === "00000015") {
						oView.byId("inpu11").setValue(oPartners[i].PartnerNo);
					}
				}

			}

			if (oTexts) {
				for (i = 0; i < oTexts.length; i++) {
					if (oTexts[i].Tdid === "0001") {
						oView.byId("inpu44").setValue(oTexts[i].ConcLines);
					}
				}
			}

			if (oData.Guid === "00000000-0000-0000-0000-000000000000") {
				oView.byId("idTitle").setText("Kayıt Yarat");
				// Yeni kayıtlarda bazı alanlar default gelecek.
				oView.byId("inpu21").setDateValue(new Date());
				oView.byId("inpu22").setDateValue(new Date());
			} else {
				oView.byId("idTitle").setText("Kayıt Güncelle");
			}

			if (oData.ToPricing.Currency === "") oData.ToPricing.Currency = "TRY";

		},

		toBack: function (oEvent) {
			sap.ui.core.UIComponent.getRouterFor(this).navTo("InquirePrice", true);
			var oView = this.getView();
			// var oModelSearch = this.getOwnerComponent().getModel("search");
			// var oModelHeaderSearch = this.getOwnerComponent().getModel("headersearch");
			// sap.ui.getCore().byId("idNewProdDialog").destroy();
			// sap.ui.getCore().byId("idNewCatDialog").destroy();
			// oModelSearch.setData({});
			// oModelHeaderSearch.setData({});
		},
		handleItemEnter: function (oEvent) {

			var oInd = oEvent.getSource().getParent().getBindingContextPath().slice(-1);
			var oTab = this.getView().byId("SepetTabId").getItems();
			var oQuantity = oTab[oInd].getCells()[4].getValue();
			var oProd = oTab[oInd].getCells()[2].getValue();
			if (oProd !== "" && oQuantity > "0.000") {
				this.onSave(false);
			}
		},
		onPressSave: function () {
			this.onSave(true);
		},

		onSave: function (oSave) {
			debugger;
			var oView = this.getView();

			var oModel = this.getView().getModel("oModel");

			if (this.alanKontrol(oSave) === "true" || !oSave) {

				var oHeaderSet = this.clone(oModel.oData);

				delete oHeaderSet.__metadata;

				delete oHeaderSet.ToStatus.__metadata;
				delete oHeaderSet.ToCustomerExt.__metadata;
				delete oHeaderSet.ToPricing.__metadata;
				delete oHeaderSet.ToCumulate.__metadata;
				delete oHeaderSet.ToCondEasyEntries.__metadata;
				delete oHeaderSet.ToCondEasyEntries.__deferred;
				delete oHeaderSet.ToSales.__metadata;
				delete oHeaderSet.ToSales.__deferred;

				delete oHeaderSet.ToConfig;
				delete oHeaderSet.ToOrgman;
				delete oHeaderSet.ToDates;

				oHeaderSet.ToCondEasyEntries.Header = oHeaderSet.Guid;
				oHeaderSet.ToCustomerExt.Guid = oHeaderSet.Guid;
				oHeaderSet.ToCumulate.Guid = oHeaderSet.Guid;
				oHeaderSet.ToPricing.Guid = oHeaderSet.Guid;
				oHeaderSet.ToSales.Guid = oHeaderSet.Guid;
				oHeaderSet.ToCumulate.Guid = oHeaderSet.Guid;

				if (oSave === true) oHeaderSet.Save = "X";

				if (oHeaderSet.ToPartners.results) {
					oHeaderSet.ToPartners = oHeaderSet.ToPartners.results;
					oHeaderSet.ToTexts = oHeaderSet.ToTexts.results;
				}

				this.getData(oHeaderSet);

				oHeaderSet.ToItems = oHeaderSet.ToItems.results; //Sepet;
				oHeaderSet.ToTahsilat = oHeaderSet.ToTahsilat.results; //Tahsilat;
				oHeaderSet.ToMessages = oHeaderSet.ToMessages.results;

				for (var j = 0; j < oHeaderSet.ToTahsilat.length; j++) {
					if (oHeaderSet.ToTahsilat[j].Zzfld00009h === true) {
						oHeaderSet.ToTahsilat[j].Zzfld00009h = "X";
					} else {
						oHeaderSet.ToTahsilat[j].Zzfld00009h = "";
					}
					if (oHeaderSet.ToTahsilat[j].Zzfld0000bf === true) {
						oHeaderSet.ToTahsilat[j].Zzfld0000bf = "X";
					} else {
						oHeaderSet.ToTahsilat[j].Zzfld0000bf = "";
					}
				}
				this.oSave = oHeaderSet.Save;
				var oDataModel = this.getView().byId("SepetTabId").getModel();
				var that = this;
				var oBusyDialog = new sap.m.BusyDialog();
				oBusyDialog.open();
				oDataModel.create("/headerSet", oHeaderSet, {
					success: function (oData, oResponse) {
						var oModelJsonList = new sap.ui.model.json.JSONModel();
						that.fixTabs(oData);
						oModelJsonList.setData(oData);
						that.showMessages(oData.ToMessages);
						oView.setModel(oModelJsonList, "oModel");
						oBusyDialog.close();
						if (that.oSave === "X") {
							this.getView().getController().showMessageBox("Kayıt işlemi başarıyla tamamlanmıştır.");
							// var message = "Kayıt işlemi başarıyla tamamlanmıştır.";
							// sap.m.MessageBox.show(message, {
							// 	icon: sap.m.MessageBox.Icon.SUCCESS,
							// 	title: "HATA",
							// 	actions: [sap.m.MessageBox.Action.OK]
							// });
						}
					}.bind(this),
					error: function (oError) {
						oBusyDialog.close();
						this.getView().getController().showMessageBox(oError);
						// var message = oError.responseText;
						// sap.m.MessageBox.show(message, {
						// 	icon: sap.m.MessageBox.Icon.ERROR,
						// 	title: "HATA",
						// 	actions: [sap.m.MessageBox.Action.OK]
						// });
					}.bind(this)
				});
			} else {
				var oMessage = "Lütfen Tüm Zorunlu Alanları Doldurunuz";
				sap.m.MessageBox.show(oMessage, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "UYARI",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}

		},

		getData: function (oHeaderSet) {
			var oView = this.getView();
			var oPartners = {};
			var oText = {};
			var oPartner1 = false;
			var oPartner2 = false;
			var oPartner7 = false;
			var oText1 = false;

			for (var i = 0; i < oHeaderSet.ToPartners.length; i++) {
				delete oHeaderSet.ToPartners[i].__metadata;
				if (oHeaderSet.ToPartners[i].PartnerFct === "00000001") {
					oHeaderSet.ToPartners[i].PartnerNo = oView.byId("inpu2").getValue();
					oHeaderSet.ToPartners[i].AddressShort = oView.byId("inpu3").getValue();
					oPartner1 = true;
				}
				// if (oHeaderSet.ToPartners[i].PartnerFct === "00000002") {
				// 	oHeaderSet.ToPartners[i].AddressShort = oView.byId("inpu61").getValue();
				// 	oPartner2 = true;
				// }
				if (oHeaderSet.ToPartners[i].PartnerFct === "00000015") {
					oHeaderSet.ToPartners[i].PartnerNo = oView.byId("inpu11").getValue();
					oPartner7 = true;
				}
			}

			if (!oPartner1) {
				oPartners.PartnerFct = "00000001";
				oPartners.Guid = oHeaderSet.Guid;
				oPartners.PartnerNo = oView.byId("inpu2").getValue();
				oPartners.AddressShort = oView.byId("inpu3").getValue();
				oHeaderSet.ToPartners.push(oPartners);
			}
			if (!oPartner2) {
				// oPartners.PartnerFct = "00000002";
				// oPartners.PartnerNo = "1";
				// oPartners.Guid = oHeaderSet.Guid;
				// oPartners.AddressShort = oView.byId("inpu61").getValue();
				// oHeaderSet.ToPartners.push(oPartners);
			}
			if (!oPartner7) {
				oPartners.PartnerFct = "00000015";
				oPartners.Guid = oHeaderSet.Guid;
				oPartners.PartnerNo = oView.byId("inpu11").getValue();
				oHeaderSet.ToPartners.push(oPartners);
			}

			for (i = 0; i < oHeaderSet.ToTexts.length; i++) {
				delete oHeaderSet.ToTexts[i].__metadata;
				if (oHeaderSet.ToTexts[i].Tdid === "0001") {
					oHeaderSet.ToTexts[i].ConcLines = oView.byId("inpu44").getValue();
					oText1 = true;
				}
			}

			if (!oText1) {
				oText.Tdid = "0001";
				oText.ConcLines = oView.byId("inpu44").getValue();
				oHeaderSet.ToTexts.push(oText);
			}
		},

		toAddSepet: function () {
			if (this.getView().getModel("oModel").oData.ToStatus.Status === 'E0002' || this.getView().getModel("oModel").oData.Guid ===
				"00000000-0000-0000-0000-000000000000") {
				var oRow = {
					OrderedProd: "",
					Description: "",
					Quantity: "0.000",
					ProcessQtyUnit: "ADT",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: this.getView().byId("inpu251").getSelectedKey(),
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: ""
				};
				var oTab = this.getView().byId("SepetTabId");
				var List = oTab.getModel("oModel").getProperty("/ToItems/results");
				List.push(oRow);
				oTab.getModel("oModel").setProperty("/ToItems/results", List);
				oTab.getModel("oModel").refresh();

			} else {
				var oMessage = "Yalnızca E0002 statüsü için sepet verileri değiştirilebilir.";
				sap.m.MessageBox.show(oMessage, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "UYARI",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}

		},
		deleteSepetRow: function (oEvent) {

			if (this.getView().getModel("oModel").oData.ToStatus.Status === 'E0002' || this.getView().getModel("oModel").oData.Guid ===
				"00000000-0000-0000-0000-000000000000") {

				var oTab = this.getView().byId("SepetTabId");
				var path = oEvent.getParameter("listItem").getBindingContext("oModel").getPath();
				var idx = parseInt(path.substring(path.lastIndexOf("/") + 1));
				oTab.getModel("oModel").oData.ToItems.results.splice(idx, 1);
				oTab.getModel("oModel").refresh();

			} else {
				var oMessage = "Yalnızca E0002 statüsü için sepet verileri değiştirilebilir.";
				sap.m.MessageBox.show(oMessage, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "UYARI",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}

		},
		alanKontrol: function (oSave) {
			var oView = this.getView();
			var oVal = "true";

			if (oSave) {
				if (!oView.byId("inpu2").getValue()) {
					oView.byId("inpu2").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu2").setValueState(sap.ui.core.ValueState.None);
				}
				// if (!oView.byId("inpu4").getValue()) {
				// 	oView.byId("inpu4").setValueState(sap.ui.core.ValueState.Error);
				// 	oVal = "false";
				// } else {
				// 	oView.byId("inpu4").setValueState(sap.ui.core.ValueState.None);
				// }
				// if (!oView.byId("inpu8").getValue()) {
				// 	oView.byId("inpu8").setValueState(sap.ui.core.ValueState.Error);
				// 	oVal = "false";
				// } else {
				// 	oView.byId("inpu8").setValueState(sap.ui.core.ValueState.None);
				// }
				if (!oView.byId("inpu14").getValue()) {
					oView.byId("inpu14").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu14").setValueState(sap.ui.core.ValueState.None);
				}
				// if (!oView.byId("inpu24").getDateValue()) {
				// 	oView.byId("inpu24").setValueState(sap.ui.core.ValueState.Error);
				// 	oVal = "false";
				// } else {
				// 	oView.byId("inpu24").setValueState(sap.ui.core.ValueState.None);
				// }
				if (!oView.byId("inpu25").getValue()) {
					oView.byId("inpu25").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu25").setValueState(sap.ui.core.ValueState.None);
				}
				if (!oView.byId("inpu251").getSelectedKey()) {
					oView.byId("inpu251").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu251").setValueState(sap.ui.core.ValueState.None);
				}
			}

			return oVal;
		},

		toAddTahsilat: function () {
			var oOdenecekTutar = "0.000";
			var oToplamTutar = "0.000";
			var oOdenenTutar = "0.000";

			var oTip = "OT-10";

			var oTab = this.getView().byId("TahsilatTabId");
			var oTahsilatList = oTab.getModel("oModel").getProperty("/ToTahsilat");
			var oSepetList = this.getView().byId("SepetTabId").getModel("oModel").getProperty("/ToItems/results");

			if (oSepetList) var oLength = oSepetList.length;
			else oLength = 0;

			for (var i = 0; i < oLength; i++) {
				oToplamTutar = parseFloat(oToplamTutar) + parseFloat(oSepetList[i].Brmfytkdvli);
			}

			if (oTahsilatList) oLength = oTahsilatList.results.length;
			else {
				oLength = 0;
				oTahsilatList = {
					results: []
				};
			}

			if (oLength === 0) {
				oOdenecekTutar = oToplamTutar;
			} else {
				oTip = "OT-" + ((oLength + 1) * 10);

				for (i = 0; i < oLength; i++) {
					if (oTahsilatList.results[i].Zzfld000044 !== "01" && oTahsilatList.results[i].Zzfld000044 !== "02" &&
						oTahsilatList.results[i].Zzfld000044 !== "03" && oTahsilatList.results[i].Zzfld000044 !== "10" &&
						oTahsilatList.results[i].Zzfld000044 !== "11") {
						oOdenenTutar = parseFloat(oOdenenTutar) + parseFloat(oTahsilatList.results[i].Zzfld00007y);
					}
				}
				oOdenecekTutar = parseFloat(oToplamTutar) - parseFloat(oOdenenTutar);
			}

			if (oOdenecekTutar === 0) oOdenecekTutar = "0.000";

			var oRow = {
				Zzfld00005v: oTip,
				Zzfld00005w: "",
				Zzfld000085: "",
				Zzfld000044: "", ////
				Zzfld00007u: "",
				Zzfld000081: "",
				Zzfld0000an: "",
				Zzfld0000at: "",
				Zzfld0000c2: "",
				Zzfld000045: "",
				Zzfld00007y: oOdenecekTutar,
				Zzfld0000bp: oOdenecekTutar,
				Zzfld00007w: "0.000",
				Zzfld00007x: "0.000",
				Zzfld00009h: "",
				Zzfld0000bf: ""
			};

			oTahsilatList.results.push(oRow);
			oTab.getModel("oModel").setProperty("/ToTahsilat", oTahsilatList);
			oTab.getModel("oModel").refresh();
		},

		deleteTahsilatRow: function (oEvent) {
			var oTab = this.getView().byId("TahsilatTabId");
			var path = oEvent.getParameter("listItem").getBindingContext("oModel").getPath();
			var idx = parseInt(path.substring(path.lastIndexOf("/") + 1));

			if (oTab.getModel("oModel").oData.ToTahsilat.results[idx].Zzfld00009h === "" ||
				oTab.getModel("oModel").oData.ToTahsilat.results[idx].Zzfld00009h === false) {
				oTab.getModel("oModel").oData.ToTahsilat.results.splice(idx, 1);
				oTab.getModel("oModel").refresh();
			} else {
				var oMessage = "Kasa kontrol işaretli olan kayıtlar silinemez.";
				sap.m.MessageBox.show(oMessage, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "UYARI",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}

		},

		handleTahsilatEnter: function (oEvent) {
			var oInd = oEvent.getSource().getParent().getBindingContextPath().slice(-1);
			var oModel = this.getView().byId("TahsilatTabId").getModel("oModel");
			var oTab = oModel.getProperty("/ToTahsilat/results");
			oTab[oInd].Zzfld0000bp = oTab[oInd].Zzfld00007y;
			oModel.setProperty("/ToTahsilat/results", oTab);
		},

		onOdemeTurChange: function (oEvent) {
			var oInd = oEvent.getSource().getParent().getBindingContextPath().slice(-1);
			var oModel = this.getView().byId("TahsilatTabId").getModel("oModel");
			var oTab = oModel.getProperty("/ToTahsilat/results");

			if (oTab[oInd].Zzfld000044 !== "11" && oTab[oInd].Zzfld000044 !== " ") {
				oTab[oInd].Zzfld000047 = new Date();
			}
			if (oTab[oInd].Zzfld000044 === "01" ||
				oTab[oInd].Zzfld000044 === "02" ||
				oTab[oInd].Zzfld000044 === "03" ||
				oTab[oInd].Zzfld000044 === "10" ||
				oTab[oInd].Zzfld000044 === "11") {
				oTab[oInd].Zzfld000045 = "01";
			} else if (oTab[oInd].Zzfld000044 === "04" ||
				oTab[oInd].Zzfld000044 === "05" ||
				oTab[oInd].Zzfld000044 === "06" ||
				oTab[oInd].Zzfld000044 === "07" ||
				oTab[oInd].Zzfld000044 === "08") {
				oTab[oInd].Zzfld000045 = "02";
			}

			oModel.setProperty("/ToTahsilat/results", oTab);

			if (oTab[oInd].Zzfld000044 === "01" ||
				oTab[oInd].Zzfld000044 === "02" ||
				oTab[oInd].Zzfld000044 === "03" ||
				oTab[oInd].Zzfld000044 === "10" ||
				oTab[oInd].Zzfld000044 === "11") {

				var oModelRef = this.getView().getModel("oRefId");

				var oId = {
					Key: oTab[oInd].Zzfld00005v
				};

				if (oModelRef) {
					var oDat = this.getView().getModel("oRefId").getProperty("/results");
					if (oDat.includes(oId) < 1) {
						oDat.push(oId);
					}
					oModelRef.setProperty("/results", oDat);
				} else {
					oDat = {
						results: []
					};
					oDat.results.push(oId);
					var oModelJsonList = new sap.ui.model.json.JSONModel(oDat);
					this.getView().setModel(oModelJsonList, "oRefId");
				}

			}

		},

		onTahsDurumChange: function (oEvent) {

			var oInd = oEvent.getSource().getParent().getBindingContextPath().slice(-1);
			var oModel = this.getView().byId("TahsilatTabId").getModel("oModel");
			var oTab = oModel.getProperty("/ToTahsilat/results");

			if (oTab[oInd].Zzfld000045 === "02" &&
				(oTab[oInd].Zzfld000044 === "01" ||
					oTab[oInd].Zzfld000044 === "02" ||
					oTab[oInd].Zzfld000044 === "03" ||
					oTab[oInd].Zzfld000044 === "10" ||
					oTab[oInd].Zzfld000044 === "11")) {

				for (var i = 0; i < oTab.length; i++) {
					if (oTab[i].Zzfld00005w === oTab[oInd].Zzfld00005v) {
						oTab[i].Zzfld000045 = "02";
					}
				}

			}

			oModel.setProperty("/ToTahsilat/results", oTab);

		},

		onBayiChange: function (oEvent) {
			var val = this.getView().byId("inpu5").getSelectedKey();
			var oAdres = this.getView().byId("inpu6");
			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var oFilters = [];
			var oFilter = new Filter("Code", FilterOperator.EQ, "0003");
			oFilters.push(oFilter);
			oFilter = new Filter("Extension", FilterOperator.EQ, val);
			oFilters.push(oFilter);
			this.getView().getModel().read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					oModelJsonList.setData(oData);
					oAdres.setModel(oModelJsonList, "oAdres");
				},
				error: function (oError) {}
			});
		},
		onSelectFromProd: function (oEvent) {

			// Teşhirden Satış Mağaza ZA15
			var oProcessType = this.getView().getModel("oModel").oData.ProcessType;

			if (oProcessType === "ZA15") {
				if (sap.ui.getCore().byId("idSelectFromProd")) {
					sap.ui.getCore().byId("idSelectFromProd").destroy();
				}
				this._oSelProdHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.selectFromProd", this);
				this.getView().addDependent(this._oSelProdHelpDialog);
				this._oSelProdHelpDialog.open();

			} else {
				sap.m.MessageBox.show("Teşhirden seçme yalnızca Teşhirden Satış Mağaza - ZA15 türündeki siparişlerde geçerlidir", {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}

		},

		handleSelectProdCancel: function (oEvent) {
			this._oSelProdHelpDialog.close();
		},
		handleSelectProdSearch: function (oEvent) {

			var oModelProd = this.getView().getModel();

			var oGuid = this.getView().getModel("oModel").oData.Guid;
			var oSipNo = sap.ui.getCore().byId("idSipNo").getValue();
			var oKalemNo = sap.ui.getCore().byId("idKalemNo").getValue();
			var oUrunKod = sap.ui.getCore().byId("idUrunKod").getValue();
			var oUrunTanim = sap.ui.getCore().byId("idUrunTanim").getValue();

			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];

			if (oGuid !== "00000000-0000-0000-0000-000000000000") {
				oFilters.push(new Filter({
					path: 'OrderGuid',
					value1: oGuid,
					operator: FilterOperator.EQ
				}));
			}

			if (oSipNo) {
				oFilters.push(new Filter({
					path: 'SiparisNo',
					value1: oSipNo,
					operator: FilterOperator.EQ
				}));
			}

			if (oKalemNo) {
				oFilters.push(new Filter({
					path: 'KalemNo',
					value1: oKalemNo,
					operator: FilterOperator.EQ
				}));
			}
			if (oUrunKod) {
				oFilters.push(new Filter({
					path: 'UrunKodu',
					value1: oUrunKod,
					operator: FilterOperator.EQ
				}));
			}
			if (oUrunTanim) {
				oFilters.push(new Filter({
					path: 'UrunTanimi',
					value1: oUrunTanim,
					operator: FilterOperator.EQ
				}));
			}

			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var that = this;
			oBusyDialog.open();
			oModelProd.read("/teshirdenSecSet", {
				filters: oFilters,
				success: function (oData, response) {
					that.stockFilter(oData);
					oModelJsonList.setData(oData);
					that._oSelProdHelpDialog.setModel(oModelJsonList, "oTeshir");
					oBusyDialog.close();
				},
				error: function (oError) {
					oBusyDialog.close();
				}
			});

		},

		stockFilter: function (oData) {

			var oSepetList = this.getView().getModel("oModel").getProperty("/ToItems/results");

			for (var i = 0; i < oSepetList.length; i++) {
				if (oSepetList[i].ZzaTeshirblgno !== "" && oSepetList[i].Zzfld000083 !== "") {
					for (var j = 0; j < oData.results.length; j++) {
						if (oData.results[j].SiparisNo === oSepetList[i].ZzaTeshirblgno && oData.results[j].KalemNo === oSepetList[i].Zzfld000083) {
							oData.results[j].Stok = oData.results[j].Stok - oSepetList[i].Quantity;
							if (oData.results[j].Stok <= 0) {
								oData.results.splice(j, 1);
							}
						}
					}
				}
			}

			return oData;
		},

		handleSelectProdAdd: function (oEvent) {

			var oTab = this.getView().byId("SepetTabId");
			var List = oTab.getModel("oModel").getProperty("/ToItems/results");
			var oSelProdTab = sap.ui.getCore().byId("SelProdTabId");
			var oSelected = oSelProdTab.getSelectedItems();

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oSipNo = oSelProdTab.getItems()[oInd].getCells()[0].getText();
				var oKalemNo = oSelProdTab.getItems()[oInd].getCells()[1].getText();
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[2].getText();
				var oIndirim = oSelProdTab.getItems()[oInd].getCells()[5].getText();

				var oRow = {
					OrderedProd: oUrunKod,
					Description: "",
					Quantity: "0.000",
					ProcessQtyUnit: "ADT",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: "",
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: "",
					ShowConfigButton: "X",
					Zzfld000083: oKalemNo,
					ZzaTeshirblgno: oSipNo,
					ZzTeshirind: oIndirim
				};

				List.push(oRow);

			}

			oTab.getModel("oModel").setProperty("/ToItems/results", List);
			oTab.getModel("oModel").refresh();
			this._oSelProdHelpDialog.close();
		},
		// onProdValueHelp: function (oEvent) {

		// 	if (sap.ui.getCore().byId("idNewProdDialog")) {
		// 		sap.ui.getCore().byId("idNewProdDialog").destroy();
		// 	}

		// 	this._oProdHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.ProdSearch", this);
		// 	this.getView().addDependent(this._oProdHelpDialog);
		// 	this._oProdHelpDialog.open();

		// 	this.Source = oEvent.getSource();

		// },

		// handleProdCancel: function (oEvent) {
		// 	this._oProdHelpDialog.close();
		// },
		// handleProdSearch: function (oEvent) {

		// 	var oModelProd = this.getView().getModel("PROD");

		// 	var oProdId = sap.ui.getCore().byId("idProdId").getValue();
		// 	var oProdDes = sap.ui.getCore().byId("idProdDesc").getValue();
		// 	var oCatId = sap.ui.getCore().byId("idCatId").getValue();
		// 	var oCatDes = sap.ui.getCore().byId("idCatDesc").getValue();
		// 	var oRow = sap.ui.getCore().byId("idProdRows").getValue();

		// 	var oBusyDialog = new sap.m.BusyDialog();

		// 	var oFilters = [];
		// 	if (oProdId !== "") {
		// 		var oFilter = new Filter("ProductId", FilterOperator.EQ, oProdId);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oProdDes !== "") {
		// 		oFilter = new Filter("ShortText", FilterOperator.EQ, oProdDes);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oCatId !== "") {
		// 		oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oCatDes !== "") {
		// 		oFilter = new Filter("CategoryDesc", FilterOperator.EQ, oCatDes);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oRow !== "") {
		// 		oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
		// 		oFilters.push(oFilter);
		// 	}

		// 	var oModelJsonList = new sap.ui.model.json.JSONModel();
		// 	var that = this;
		// 	oBusyDialog.open();
		// 	oModelProd.read("/productSearchSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			oModelJsonList.setData(oData);
		// 			that._oProdHelpDialog.setModel(oModelJsonList, "oProd");
		// 			oBusyDialog.close();
		// 		},
		// 		error: function (oError) {
		// 			oBusyDialog.close();
		// 		}
		// 	});

		// },

		// handleProdListItemPress: function (oEvent) {
		// 	var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
		// 	var oProd = sap.ui.getCore().byId("ProdTabId").getItems()[oInd].getCells()[0].getText();
		// 	this.Source.setValue(oProd);
		// 	// var oProdText = sap.ui.getCore().byId("ProdTabId").getItems()[oInd].getCells()[1].getText();
		// 	// var oTextId = this.Source.getId();
		// 	// oTextId = oTextId.substr(0, 7) + "25" + oTextId.substr(9);
		// 	// sap.ui.getCore().byId(oTextId).setValue(oProdText);
		// 	this._oProdHelpDialog.close();
		// },
		// onCatValueHelp: function (oEvent) {

		// 	if (sap.ui.getCore().byId("idNewCatDialog")) {
		// 		sap.ui.getCore().byId("idNewCatDialog").destroy();
		// 	}
		// 	this._oCatHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.CatSearch", this);
		// 	this.getView().addDependent(this._oCatHelpDialog);
		// 	this._oCatHelpDialog.open();

		// },

		// handleCatCancel: function () {
		// 	this._oCatHelpDialog.close();
		// },
		// handleCatSearch: function () {

		// 	var oModelProd = this.getView().getModel("PROD");

		// 	var oCatId = sap.ui.getCore().byId("idCategId").getValue();
		// 	var oCatDes = sap.ui.getCore().byId("idCategDesc").getValue();
		// 	var oHyrId = sap.ui.getCore().byId("idHyrType").getSelectedKey();
		// 	var oHyrDes = sap.ui.getCore().byId("idHyrDes").getValue();
		// 	var oRow = sap.ui.getCore().byId("idCatRows").getValue();

		// 	var oBusyDialog = new sap.m.BusyDialog();

		// 	var oFilters = [];
		// 	if (oCatId !== "") {
		// 		var oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oCatDes !== "") {
		// 		oFilter = new Filter("CategoryText", FilterOperator.EQ, oCatDes);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oHyrId !== "") {
		// 		oFilter = new Filter("HierarchyId", FilterOperator.EQ, oHyrId);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oHyrDes !== "") {
		// 		oFilter = new Filter("HierarchyText", FilterOperator.EQ, oHyrDes);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oRow !== "") {
		// 		oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
		// 		oFilters.push(oFilter);
		// 	}

		// 	var oModelJsonList = new sap.ui.model.json.JSONModel();
		// 	var that = this;
		// 	oBusyDialog.open();
		// 	oModelProd.read("/categorySearchSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			oModelJsonList.setData(oData);
		// 			that._oCatHelpDialog.setModel(oModelJsonList, "oCat");
		// 			oBusyDialog.close();
		// 		},
		// 		error: function (oError) {
		// 			oBusyDialog.close();
		// 		}
		// 	});

		// },

		// handleCatListItemPress: function (oEvent) {
		// 	var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
		// 	var oPartner = sap.ui.getCore().byId("CatTabId").getItems()[oInd].getCells()[0].getText();
		// 	sap.ui.getCore().byId("idCatId").setValue(oPartner);
		// 	this._oCatHelpDialog.close();
		// },
		// onCustomerValueHelp: function () {

		// 	if (sap.ui.getCore().byId("idNewCustDialog")) {
		// 		sap.ui.getCore().byId("idNewCustDialog").destroy();
		// 	}

		// 	this._oValueHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.CustomerSearch", this);
		// 	this.getView().addDependent(this._oValueHelpDialog);
		// 	this._oValueHelpDialog.open();

		// },

		// handleCustCancel: function () {
		// 	this._oValueHelpDialog.close();
		// },
		// handleCustomerSearch: function () {

		// 	var oModelCust = this.getView().getModel("CUST");

		// 	var oPartner = sap.ui.getCore().byId("idCustIdent").getValue();
		// 	var oAd = sap.ui.getCore().byId("idCustName").getValue();
		// 	var oSoyad = sap.ui.getCore().byId("idCustLastName").getValue();
		// 	var oTel = sap.ui.getCore().byId("idCustTel").getValue();
		// 	var oTur = sap.ui.getCore().byId("idCustType").getSelectedKey();
		// 	var oTc = sap.ui.getCore().byId("idCustTc").getValue();
		// 	var oBusyDialog = new sap.m.BusyDialog();

		// 	var oFilters = [];
		// 	if (oPartner !== "") {
		// 		var oFilter = new Filter("Partner", FilterOperator.EQ, oPartner);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oAd !== "") {
		// 		oFilter = new Filter("Firstname", FilterOperator.EQ, oAd);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oSoyad !== "") {
		// 		oFilter = new Filter("Lastname", FilterOperator.EQ, oSoyad);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oTel !== "") {
		// 		oFilter = new Filter("Telephonemob", FilterOperator.EQ, oTel);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oTur !== "") {
		// 		oFilter = new Filter("Partnertype", FilterOperator.EQ, oTur);
		// 		oFilters.push(oFilter);
		// 	}

		// 	if (oTc !== "") {
		// 		oFilter = new Filter("Tckn", FilterOperator.EQ, oTc);
		// 		oFilters.push(oFilter);
		// 	}

		// 	var oModelJsonList = new sap.ui.model.json.JSONModel();
		// 	var that = this;
		// 	oBusyDialog.open();
		// 	oModelCust.read("/searchCustomerSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			debugger;
		// 			oModelJsonList.setData(oData);
		// 			that._oValueHelpDialog.setModel(oModelJsonList, "oCustomer");
		// 			oBusyDialog.close();
		// 		},
		// 		error: function (oError) {
		// 			oBusyDialog.close();
		// 		}
		// 	});

		// },

		// handleCustomerListItemPress: function (oEvent) {
		// 	var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
		// 	var oPartner = sap.ui.getCore().byId("CustTabId").getItems()[oInd].getCells()[0].getText();
		// 	var oPartnerName = sap.ui.getCore().byId("CustTabId").getItems()[oInd].getCells()[1].getText();
		// 	this.getView().byId("inpu2").setValue(oPartner);
		// 	this.getView().byId("inpu201").setValue(oPartnerName);
		// 	this._oValueHelpDialog.close();
		// },
		// handleCustEnter: function () {
		// 	var oPartner = this.getView().byId("inpu2").getValue();
		// 	var oModelCust = this.getView().getModel("CUST");
		// 	var oFilters = [];

		// 	if (oPartner !== "") {
		// 		var oFilter = new Filter("Partner", FilterOperator.EQ, oPartner);
		// 		oFilters.push(oFilter);
		// 	}

		// 	var that = this;

		// 	oModelCust.read("/searchCustomerSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			if (oData.results.length > 0) {
		// 				var oPartnerName = oData.results[0].Firstname + " " + oData.results[0].Lastname;
		// 				that.getView().byId("inpu201").setValue(oPartnerName);
		// 			} else {
		// 				that.getView().byId("inpu201").setValue("");
		// 			}
		// 		}

		// 	});

		// },

		onItemConfig: function (oEvent) {

			var oInd = oEvent.getSource().getBindingContext("oModel").getPath().slice(-1);
			var oItems = this.getView().getModel("oModel").getProperty("/ToItems/results");
			var oItem = oItems[oInd];
			var oFilters = [];

			if (oItem.Parent && oItem.Parent !== "00000000-0000-0000-0000-000000000000") {
				var oFilter = new Filter("ParentId", FilterOperator.EQ, oItem.Parent);
				oFilters.push(oFilter);
			} else {
				oItem.NumberInt = (parseInt(oInd) + 1) * 10;
				oFilter = new Filter("ZzorderedProd", FilterOperator.EQ, oItem.OrderedProd);
				oFilters.push(oFilter);
				oFilter = new Filter("ItemNoForConfig", FilterOperator.EQ, oItem.NumberInt);
				oFilters.push(oFilter);
			}

			if (sap.ui.getCore().byId("idItemConfDialog")) {
				sap.ui.getCore().byId("idItemConfDialog").destroy();
			}
			this._oConfigScreen = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.ItemConfig", this);
			this.getView().addDependent(this._oConfigScreen);
			this._oConfigScreen.open();

			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var that = this;

			this.getView().getModel().read("/itemConfigSet", {
				filters: oFilters,
				success: function (oData, response) {
					oModelJsonList.setData(oData);
					that._oConfigScreen.setModel(oModelJsonList, "oConfig");
					that.bindCharacteristics(oData);
				},
				error: function (oError) {
					var message = oError.responseText;
					sap.m.MessageBox.show(message, {
						icon: sap.m.MessageBox.Icon.ERROR,
						title: "HATA",
						actions: [sap.m.MessageBox.Action.OK]
					});
				}
			});

		},
		handleConfigSave: function () {
			this._oConfigScreen.close();
		},
		handleConfigCancel: function () {
			this._oConfigScreen.close();
		},
		toSenetOlustur: function () {

			var message;
			var oCurDate = new Date();
			var oTab = this.getView().byId("TahsilatTabId");
			var oTahsilatList = oTab.getModel("oModel").getProperty("/ToTahsilat/results");

			var oRow = oTahsilatList[oTahsilatList.length - 1];

			if (this.getView().getModel("oModel").getProperty("/ToPricing").PriceList === "02") {
				message = "Kampanyalı fiyat üzerinden senetli satış yapılamaz!";
				sap.m.MessageBox.show(message, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			} else if (oRow.Zzfld0000c2 === "") {
				message = "Lütfen senet adedi giriniz";
				sap.m.MessageBox.show(message, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			} else if (oRow.Zzfld0000c2 > 12) {
				message = "En fazla 12 adet senet oluşturabilirsiniz";
				sap.m.MessageBox.show(message, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			} else if (oRow.Zzfld0000c3.getFullYear() === "") {
				message = "Senet oluşturmadan önce ilk senet tarihini giriniz";
				sap.m.MessageBox.show(message, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			} else if (Math.ceil(Math.abs(oRow.Zzfld0000c3.getTime() - oCurDate.getTime()) / (1000 * 60 * 60 * 24)) > 45) {
				message = "İlk senet tarihi 45 günden ileri bir tarih seçilemez";
				sap.m.MessageBox.show(message, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			} else {

				var oCount = oRow.Zzfld0000c2;
				var oTip;
				var oBirimMiktar = oRow.Zzfld00007y / oCount;
				var oTarih = oRow.Zzfld0000c3;
				var oYeniTarih = new Date(oTarih.valueOf());

				if (oRow.Zzfld000044 === "11") {
					var oTDUrum = "01";
				} else {
					oTDUrum = "";
				}

				while (oCount > 0) {
					oCount--;
					oTip = "OT-" + ((oTahsilatList.length + 1) * 10);

					var oRowNew = {
						Zzfld00005v: oTip,
						Zzfld00005w: oRow.Zzfld00005v,
						Zzfld000085: "",
						Zzfld000044: "10", ////
						Zzfld00007u: "",
						Zzfld000081: "",
						Zzfld0000an: "",
						Zzfld0000at: "",
						Zzfld0000c2: "",
						Zzfld0000c4: new Date(oYeniTarih),
						Zzfld000045: oTDUrum,
						Zzfld00007y: oBirimMiktar,
						Zzfld00007w: "0.000",
						Zzfld00007x: "0.000",
						Zzfld00009h: "",
						Zzfld0000bf: ""
					};
					oTahsilatList.push(oRowNew);
					oYeniTarih.setMonth(oYeniTarih.getMonth() + 1);
				}
				oTab.getModel("oModel").setProperty("/ToTahsilat/results", oTahsilatList);
				oTab.getModel("oModel").refresh();

			}

		},
		oPerSozlSelect: function () {

			var oView = this.getView();

			var oHead = oView.getModel("oModel").getProperty("/");

			if (oView.byId("inpu15").getSelected() && oHead.ProcessType === "ZA04") {

				var oMusteri = this.getView().byId("inpu2").getValue();
				var oFilters = [];
				var oFilter = new Filter("Code", FilterOperator.EQ, "0002");
				oFilters.push(oFilter);
				oFilter = new Filter("Extension", FilterOperator.EQ, oMusteri);
				oFilters.push(oFilter);

				oView.getModel().read("/defaultValuesSet", {
					filters: oFilters,
					success: function (oData, response) {
						oHead.ZzPersiptop = oData.ZzPersiptop;
						oView.getModel("oModel").setProperty("/", oHead);
					},
					error: function (oError) {}
				});

			}

		},
		showMessages: function (oMessages) {
			if (oMessages) {
				if (oMessages.results.length > 0) {
					if (sap.ui.getCore().byId("idStockControl")) {
						sap.ui.getCore().byId("idStockControl").destroy();
					}
					this._oStockDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.StockControl", this);
					this.getView().addDependent(this._oStockDialog);
					this._oStockDialog.open();

				}
			}
		},

		onTahsInd: function () {

			var oTahsInd;
			var oTahsIndSonra;

			var oView = this.getView();
			var oTahsIndOnce = oView.byId("inpu35").getValue();
			var oIsk = oView.byId("inpu30").getValue();
			var oTahsilatList = oView.getModel("oModel").getProperty("/ToTahsilat/results");

			if (oIsk) {
				oTahsInd = oIsk;
			}

			for (var i = 0; i < oTahsilatList.length; i++) {
				if (oTahsilatList[i].Zzfld00007x) oTahsInd = parseFloat(oTahsInd) + parseFloat(oTahsilatList[i].Zzfld00007x);
			}

			oView.byId("inpu37").setValue(oTahsInd);

			oTahsIndSonra = oTahsIndOnce - oTahsInd;
			oView.byId("inpu36").setValue(oTahsIndSonra);

		},

		handleStockCancel: function () {
			this._oStockDialog.close();
		},
		toKampanya: function () {
			this.getView().getModel("oModel").setProperty("/KampanyaBelirle", "X");
			this.onSave(false);
		},
		onPlanUygula: function () {
			this.getView().getModel("oModel").setProperty("/PlaniUygula", "X");
			this.onSave(false);
		},
		onStokKontrol: function () {
			this.getView().getModel("oModel").setProperty("/StokSorgu", "X");
			this.onSave(false);
		},
		onSelectFromCat: function () {

			if (sap.ui.getCore().byId("idSelectCatProd")) {
				sap.ui.getCore().byId("idSelectCatProd").destroy();
			}
			this._oSelProdCatHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.selectFromCat", this);
			this.getView().addDependent(this._oSelProdCatHelpDialog);
			this._oSelProdCatHelpDialog.open();

		},

		handleSelectFromCatCancel: function (oEvent) {
			this._oSelProdCatHelpDialog.close();
		},
		handleSelectCatSearch: function (oEvent) {

			var oModelProd = this.getView().getModel("PROD");
			var oHead = this.getView().getModel("oModel").getProperty("/");
var oTakim = sap.ui.getCore().byId("idTakim").getSelectedKey();
		    var oModel = sap.ui.getCore().byId("idModel").getSelectedKey();
			// var oTakim = sap.ui.getCore().byId("idTakim").getValue();
			// var oModel = sap.ui.getCore().byId("idModel").getValue();
			var oModelTnm = sap.ui.getCore().byId("idTModelTnm").getValue();
			var oUniteUrunKod = sap.ui.getCore().byId("idUniteUrunKod").getValue();
			var oUrunId = sap.ui.getCore().byId("idUrunId").getValue();
			var oCategId = sap.ui.getCore().byId("idCategId").getValue();

			if (sap.ui.getCore().byId("idConfigEnable").getSelected()) var oConfigEnable = "X";

			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];

			if (oTakim) {
				oFilters.push(new Filter({
					path: 'Zztakim',
					value1: oTakim,
					operator: FilterOperator.EQ
				}));
			}

			if (oModelTnm) {
				oFilters.push(new Filter({
					path: 'CategoryDesc',
					value1: oModelTnm,
					operator: FilterOperator.EQ
				}));
			}
			if (oUniteUrunKod) {
				oFilters.push(new Filter({
					path: 'Description',
					value1: oUniteUrunKod,
					operator: FilterOperator.EQ
				}));
			}
			if (oModel) {
				oFilters.push(new Filter({
					path: 'Zzmodel',
					value1: oModel,
					operator: FilterOperator.EQ
				}));
			}
			if (oUrunId) {
				oFilters.push(new Filter({
					path: 'ProductId',
					value1: oUrunId,
					operator: FilterOperator.EQ
				}));
			}
			if (oCategId) {
				oFilters.push(new Filter({
					path: 'CategoryId',
					value1: oCategId,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ProcessType) {
				oFilters.push(new Filter({
					path: 'ProcessType',
					value1: oHead.ProcessType,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceList) {
				oFilters.push(new Filter({
					path: 'PriceList',
					value1: oHead.ToPricing.PriceList,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.Currency) {
				oFilters.push(new Filter({
					path: 'Currency',
					value1: oHead.ToPricing.Currency,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceDate) {
				oFilters.push(new Filter({
					path: 'PriceDate',
					value1: oHead.ToPricing.PriceDate,
					operator: FilterOperator.EQ
				}));
			}
			// if (oHead.ProcessType) {
			// 	oFilters.push(new Filter({
			// 		path: 'SoldToPartnerGuid',
			// 		value1: oConfigEnable,
			// 		operator: FilterOperator.EQ
			// 	}));
			// }

			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var that = this;
			oBusyDialog.open();
			oModelProd.read("/productCatalogSearchSet", {
				filters: oFilters,
				success: function (oData, response) {
					oModelJsonList.setData(oData);
					that._oSelProdCatHelpDialog.setModel(oModelJsonList, "oCatalog");
					oBusyDialog.close();
				},
				error: function (oError) {
					oBusyDialog.close();
				}
			});

		},

		handleSelectCatAdd: function (oEvent) {

			var oTab = this.getView().byId("SepetTabId");
			var List = oTab.getModel("oModel").getProperty("/ToItems/results");
			var oSelProdTab = sap.ui.getCore().byId("SelProdCatTabId");
			var oSelected = oSelProdTab.getSelectedItems();

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oConfig = oSelProdTab.getItems()[oInd].getCells()[0].getText();
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[4].getText();
				// var oIndirim = oSelProdTab.getItems()[oInd].getCells()[5].getText();

				var oRow = {
					OrderedProd: oUrunKod,
					Description: "",
					Quantity: "0.000",
					ProcessQtyUnit: "ADT",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: "",
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: "",
					ShowConfigButton: oConfig,
					// Zzfld000083: oKalemNo,
					// ZzaTeshirblgno: oSipNo,
					// ZzTeshirind: oIndirim
				};

				List.push(oRow);

			}

			oTab.getModel("oModel").setProperty("/ToItems/results", List);
			oTab.getModel("oModel").refresh();
			//			this._oSelProdCatHelpDialog.close();
		},

		handleSelectProdTree: function () {
			// Ürün ağacından seç arama yardımı burada ürün katoloğundan seçilen satırlar kullanıarak ürün ağacı getiririlir.
			var oModelProd = this.getView().getModel("PROD");
			var oHead = this.getView().getModel("oModel").getProperty("/");
			var oSelProdTab = sap.ui.getCore().byId("SelProdCatTabId");
			var oSelected = oSelProdTab.getSelectedItems();
			var oFilterA = [];
			var oFilterProd = [];

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[4].getText();
				if (oUrunKod) {
					oFilterProd.push(new Filter({
						path: 'ProductId',
						value1: oUrunKod,
						operator: FilterOperator.EQ,
						and: false
					}));
				}
			}

			var oFilterOR = new Filter({
				filters: oFilterProd,
				and: false,
			});

			if (oHead.ProcessType) {
				oFilterA.push(new Filter({
					path: 'ProcessType',
					value1: oHead.ProcessType,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceList) {
				oFilterA.push(new Filter({
					path: 'PriceList',
					value1: oHead.ToPricing.PriceList,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.Currency) {
				oFilterA.push(new Filter({
					path: 'Currency',
					value1: oHead.ToPricing.Currency,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceDate) {
				oFilterA.push(new Filter({
					path: 'PriceDate',
					value1: oHead.ToPricing.PriceDate,
					operator: FilterOperator.EQ
				}));
			}

			var oFilterAND = new Filter({
				filters: oFilterA,
				and: true
			});
			var oFilters = [new Filter({
				and: true,
				filters: [oFilterOR, oFilterAND]

			})];

			var oBusyDialog = new sap.m.BusyDialog();
			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var that = this;

			oBusyDialog.open();
			oModelProd.read("/productCatalogTreeSearchSet", {
				filters: oFilters,
				success: function (oData, response) {
					oModelJsonList.setData(oData);
					that._oSelProdTreeHelpDialog.setModel(oModelJsonList, "oProdTree");
					oBusyDialog.close();
				},
				error: function (oError) {
					oBusyDialog.close();
				}
			});

			if (!this._oSelProdTreeHelpDialog) {
				this._oSelProdTreeHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.SelectFromProdTree", this);
				this.getView().addDependent(this._oSelProdTreeHelpDialog);
				this._oSelProdTreeHelpDialog.open();
			} else {
				this._oSelProdTreeHelpDialog.open();
			}
		},
		handleSelectProdTreeCancel: function (oEvent) {
			this._oSelProdTreeHelpDialog.close();
		},
		handleSelectProdTreeAdd: function (oEvent) {

			var oTab = this.getView().byId("SepetTabId");
			var List = oTab.getModel("oModel").getProperty("/ToItems/results");
			var oSelProdTab = sap.ui.getCore().byId("SelProdTreeTabId");
			var oSelected = oSelProdTab.getSelectedItems();

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[0].getText();
				var oText = oSelProdTab.getItems()[oInd].getCells()[1].getText();
				var oQuantity = oSelProdTab.getItems()[oInd].getCells()[5].getText();

				var oRow = {
					OrderedProd: oUrunKod,
					Description: oText,
					Quantity: oQuantity,
					ProcessQtyUnit: "",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: "",
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: "",
					ShowConfigButton: ""
				};
				List.push(oRow);
			}

			oTab.getModel("oModel").setProperty("/ToItems/results", List);
			oTab.getModel("oModel").refresh();
			this._oSelProdTreeHelpDialog.close();
		},

	});

});