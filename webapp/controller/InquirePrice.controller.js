sap.ui.define(
  [
    "./BaseController",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/m/MessageBox",
    "sap/ui/core/BusyIndicator",
	"zcrm/zcrm_inquire_price/controls/ExtScanner"
  ],
  function (BaseController, Filter, FilterOperator, MessageBox, BusyIndicator,ExtScanner) {
    "use strict";

    return BaseController.extend(
      "zcrm.zcrm_inquire_price.controller.InquirePrice",
      {
        onInit: function () {
			this.oScanner = new ExtScanner({
				settings: true,
				laser: true,
				valueScanned: this.onScanned.bind(this),
				decoderKey: "text",
				decoders: this.getDecoders(),
			  });
          BaseController.prototype.onInit.apply(this, arguments);

          this.oModel = sap.ui.getCore().getModel("oModelGlobal");
          this.getView().addStyleClass("sapUiSizeCompact");
          if (sap.ui.getCore().getModel("oModelGlobal")) {
            sap.ui.getCore().setModel(null, "oModelGlobal");
          }
        },

		      // Barcode Scanner Begin
			  onScanned: function (oEvent) {
				var that = this;
				var value = oEvent.getParameter("value");
		
				var oFilters = [];
				if (value !== "") {
				  var oFilter = new Filter("Gtin", FilterOperator.EQ, value);
				  oFilters.push(oFilter);
				}
				var oBusyDialog = new sap.m.BusyDialog();
				oBusyDialog.open();
				var oModelProd = this.getView().getModel("PROD");
				oModelProd.read("/productGtinSet", {
				  filters: oFilters,
				  success: function (oData, response) {
					try {
					  var ProductID = oData.results[0].Product;
					  that._setbarcode(ProductID);
					} catch (error) {}
		
					oBusyDialog.close();
					debugger;
				  },
				  error: function (oError) {
					oBusyDialog.close();
					debugger;
				  },
				});
				// this.oMainModel.setProperty('/scannedValue', oEvent.getParameter('value'));
			  },
		
			  onScan: function () {
				this.oScanner.open();
			  },
		
			  getDecoders: function () {
				return [
				  {
					key: "PDF417-UII",
					text: "PDF417-UII",
					decoder: this.parserPDF417UII,
				  },
				  {
					key: "text",
					text: "TEXT",
					decoder: this.parserText,
				  },
				];
			  },
		
			  parserText: function (oResult) {
				var sText = "";
				var iLength = oResult.text.length;
				for (var i = 0; i !== iLength; i++) {
				  if (oResult.text.charCodeAt(i) < 32) {
					sText += " ";
				  } else {
					sText += oResult.text[i];
				  }
				}
				return sText;
			  },
		
			  parserPDF417UII: function (oResult) {
				// we expect that
				// first symbol of UII (S - ASCII = 83) or it just last group
				var sText = oResult.text || "";
				if (oResult.format && oResult.format === 10) {
				  sText = "";
				  var iLength = oResult.text.length;
				  var aChars = [];
				  for (var i = 0; i !== iLength; i++) {
					aChars.push(oResult.text.charCodeAt(i));
				  }
				  var iStart = -1;
				  var iGRCounter = 0;
				  var iGroupUII = -1;
				  var sTemp = "";
				  aChars.forEach(function (code, k) {
					switch (code) {
					  case 30:
						if (iStart === -1) {
						  iStart = k;
						  sTemp = "";
						} else {
						  sText = sTemp;
						  iGRCounter = -1;
						}
						break;
					  case 29:
						iGRCounter += 1;
						break;
					  default:
						if (iGRCounter > 2 && code === 83 && iGRCounter > iGroupUII) {
						  sTemp = "";
						  iGroupUII = iGRCounter;
						}
						if (iGroupUII === iGRCounter) {
						  sTemp += String.fromCharCode(code);
						}
					}
				  });
				  if (sText) {
					sText = sText.slice(1);
				  }
				}
				return sText;
			  },
		
		
				// Barcode Scanner End

        onAfterRendering: function () {
          this.oModel = sap.ui.getCore().getModel("oModelGlobal");

          var oGuid = "00000000-0000-0000-0000-000000000000";
          var oOrderType = "ZS05";
          var oSet = "/headerSet(Guid=guid'" + oGuid + "')";
          var oModelJsonList = new sap.ui.model.json.JSONModel();
          var oBusyDialog = new sap.m.BusyDialog();
          var oView = this.getView();

          var that = this;
          var oSystem = sap.ushell.Container.getLogonSystem();
          debugger;
          var systemId = oSystem.getName() || "SID";
          var PartnerNo;
          if (systemId === "DGP") {
            PartnerNo = "17118004";
          } else {
            PartnerNo = "17110684";
          }
          if (!this.oModel) {
            oBusyDialog.open();
            oView.getModel().read(oSet, {
              urlParameters: {
                $expand:
                  "ToTexts,ToCumulate,ToCustomerExt,ToDates,ToItems,ToOrgman,ToPartners,ToPricing,ToStatus,ToTahsilat,ToMessages,ToConfig",
              },
              success: function (data, response) {
                data.ProcessType = oOrderType;
                data.ToPartners.results.push({
                  PartnerFct: "00000001",
                  PartnerNo: PartnerNo,
                  // PartnerNo: "25005502"
                });

                oModelJsonList.setData(data);
                oView.setModel(oModelJsonList, "oModel");
                that.bindSearchHelps(data);
                that.showMessages(data.ToMessages);
                oBusyDialog.close();
              },
              error: function (oError) {
                oBusyDialog.close();
              },
            });
          } else {
            oView.setModel(this.oModel, "oModel");
            oBusyDialog.close();
          }
        },

        bindSearchHelps: function (oData) {
          var oModel = this.getView().getModel();
          var oView = this.getView();
          var oFilters = [];
          var oFilter = {};
          var oFlist = oView.byId("inpu25");
          var oOdemeList = oView.byId("inpu26");

          oFilter = new Filter("Code", FilterOperator.EQ, "0007");
          oFilters.push(oFilter);
          oModel.read("/valueHelpSet", {
            filters: oFilters,
            success: function (oData, response) {
              var oModelJsonList7 = new sap.ui.model.json.JSONModel(oData);
              oFlist.setModel(oModelJsonList7, "oFlist");
            },
            error: function (oError) {},
          });

          oFilters = [];
          oFilter = new Filter("Code", FilterOperator.EQ, "0022");
          oFilters.push(oFilter);
          oModel.read("/valueHelpSet", {
            filters: oFilters,
            success: function (oData, response) {
              var oModelJsonListOdeme = new sap.ui.model.json.JSONModel(oData);
              oView.setModel(oModelJsonListOdeme, "oOdemeList");
            },
            error: function (oError) {},
          });

          oFilters = [];
          oFilter = new Filter("Code", FilterOperator.EQ, "0011");
          oFilters.push(oFilter);
          oModel.read("/valueHelpSet", {
            filters: oFilters,
            success: function (oData, response) {
              var oModelJsonList10 = new sap.ui.model.json.JSONModel(oData);
              oView.setModel(oModelJsonList10, "oDepo");
            },
            error: function (oError) {},
          });

          oModel.read("/unitSet", {
            success: function (oData, response) {
              var oModelJsonList15 = new sap.ui.model.json.JSONModel(oData);
              oModelJsonList15.setSizeLimit(500);
              oView.setModel(oModelJsonList15, "oUnit");
            },
            error: function (oError) {},
          });

          oModel.read("/currencySet", {
            success: function (oData, response) {
              var oModelJsonList16 = new sap.ui.model.json.JSONModel(oData);
              oModelJsonList16.setSizeLimit(500);
              oView.setModel(oModelJsonList16, "oCurr");
            },
            error: function (oError) {},
          });
        },

        handleItemEnter: function (oEvent) {
          // var oProd = oEvent.getSource().getBindingContext("oModel").getProperty("OrderedProd")
          var oInd = oEvent
            .getSource()
            .getParent()
            .getBindingContextPath()
            .slice(-1);
          var oTab = this.getView().byId("SepetTabId").getItems();
          var oQuantity = oTab[oInd].getCells()[4].getValue();
          var oProd = oTab[oInd].getCells()[2].getValue();
          if (oProd !== "" && oQuantity > "0.000") {
            var selectFiyatListesi = this.getView()
              .byId("inpu25")
              .getSelectedKey();
            if (!selectFiyatListesi) {
              this.getView()
                .getController()
                .showMessageBox("Fiyat listesi seçiniz!", "error");
              return;
            }

            this.onSave(false);
          }
        },

        onSave: function (oSave, oKampanya) {
          debugger;
          var oView = this.getView();

          var oModel = this.getView().getModel("oModel");

          var oKapampanyaBelirle = this.getView()
            .getModel("KampanyaBelirle")
            .getProperty("/");
          if (oKapampanyaBelirle === "X") {
            oModel.setProperty("/KampanyaBelirle", "X");
          }

          var oHeaderSet = this.clone(oModel.oData);

          delete oHeaderSet.__metadata;

          delete oHeaderSet.ToStatus.__metadata;
          delete oHeaderSet.ToCustomerExt.__metadata;
          delete oHeaderSet.ToPricing.__metadata;
          delete oHeaderSet.ToCumulate.__metadata;
          delete oHeaderSet.ToCondEasyEntries.__metadata;
          delete oHeaderSet.ToCondEasyEntries.__deferred;
          delete oHeaderSet.ToSales.__metadata;
          delete oHeaderSet.ToSales.__deferred;

          // delete oHeaderSet.ToConfig;
          delete oHeaderSet.ToOrgman;
          delete oHeaderSet.ToDates;

          oHeaderSet.ToCondEasyEntries.Header = oHeaderSet.Guid;
          oHeaderSet.ToCustomerExt.Guid = oHeaderSet.Guid;
          oHeaderSet.ToCumulate.Guid = oHeaderSet.Guid;
          oHeaderSet.ToPricing.Guid = oHeaderSet.Guid;
          oHeaderSet.ToSales.Guid = oHeaderSet.Guid;
          oHeaderSet.ToCumulate.Guid = oHeaderSet.Guid;

          if (oSave === true) oHeaderSet.Save = "X";

          if (oHeaderSet.ToPartners.results) {
            oHeaderSet.ToPartners = oHeaderSet.ToPartners.results;
            oHeaderSet.ToTexts = oHeaderSet.ToTexts.results;
          }
          oHeaderSet.ToItems = oHeaderSet.ToItems.results; //Sepet;
          oHeaderSet.ToTahsilat = oHeaderSet.ToTahsilat.results; //Tahsilat;
          oHeaderSet.ToMessages = oHeaderSet.ToMessages.results;
          // oHeaderSet.KampanyaBelirle = oKampanya;

          if (oHeaderSet.ToConfig) {
            oHeaderSet.ToConfig = oHeaderSet.ToConfig.results.map(
              function removeMetaData(line) {
                var oLine = Object.assign({}, line);
                delete oLine.__metadata;
                delete oLine.__oKarakteristik;
                return oLine;
              }
            );
          } else {
            oHeaderSet.ToConfig = [];
          }

          var oDataModel = this.getView().byId("SepetTabId").getModel();
          var that = this;
          var oBusyDialog = new sap.m.BusyDialog();
          oBusyDialog.open();

          oDataModel.create("/headerSet", oHeaderSet, {
            success: function (oData, oResponse) {
              debugger;
              var oModelJsonList = new sap.ui.model.json.JSONModel();
              that.fixTabs(oData);
              oData.ToConfig = oModel.getProperty("/ToConfig");
              that.showMessages(oData.ToMessages);
              oModelJsonList.setData(oData);
              oView.setModel(oModelJsonList, "oModel");
              that.onSptInd();
              oBusyDialog.close();
            },
            error: function (oError) {
              oBusyDialog.close();
              var message = oError.responseText;
              sap.m.MessageBox.show(message, {
                icon: sap.m.MessageBox.Icon.ERROR,
                title: "HATA",
                actions: [sap.m.MessageBox.Action.OK],
              });
            },
          });
        },

        clone: function (obj) {
          if (null === obj || "object" != typeof obj) return obj;
          var copy = obj.constructor();
          for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
          }
          return copy;
        },

        toAddSepet: function () {
          var oRow = {
            OrderedProd: "",
            Description: "",
            Quantity: "1.000",
            ProcessQtyUnit: "ADT",
            Conditionamount1: "0.000",
            Brmfytkdvli: "0.000",
            KampInd: "0.000",
            ThslatInd: "0.000",
            Zzfld00008x: "0.000",
            Currency: "TRY",
            TeslimatTrh: new Date(),
            Zzerpstatus: "",
            Zzspecad: "",
            Zzteshirdepo: "",
          };
          var oTab = this.getView().byId("SepetTabId");
          var List = oTab.getModel("oModel").getProperty("/ToItems/results");
          List.push(oRow);
          oTab.getModel("oModel").setProperty("/ToItems/results", List);
          oTab.getModel("oModel").refresh();
        },

        deleteSepetRow: function (oEvent) {
          var oTab = this.getView().byId("SepetTabId");
          var path = oEvent
            .getParameter("listItem")
            .getBindingContext("oModel")
            .getPath();
          var idx = parseInt(path.substring(path.lastIndexOf("/") + 1));
          oTab.getModel("oModel").oData.ToItems.results.splice(idx, 1);
          oTab.getModel("oModel").refresh();
          this.onSave(false);
        },

        onSelectFromProd: function (oEvent) {
          // Teşhirden Satış Mağaza ZA15
          var oProcessType =
            this.getView().getModel("oModel").oData.ProcessType;

          if (!this._oSelProdHelpDialog) {
            this._oSelProdHelpDialog = sap.ui.xmlfragment(
              "zcrm.zcrm_inquire_price.fragments.selectFromProd",
              this
            );
            this.getView().addDependent(this._oSelProdHelpDialog);
            this._oSelProdHelpDialog.open();
          } else {
            this._oSelProdHelpDialog.open();
          }
        },

        onPricListChange: function () {
          this.onSave(false);
        },

        onSptInd: function () {
          debugger;

          var oBusyDialog = new sap.m.BusyDialog();

          var oTahsInd;
          var oTahsIndSonra;

          var oView = this.getView();
          var oTahsIndOnce = oView.getModel("oModel").getData().Zzfld00008d;
          var oIsk = oView.byId("idIskonto").getValue();

          var oFilters = [];
          var oFilter = new Filter("Code", FilterOperator.EQ, "0005");
          oFilters.push(oFilter);
          oFilter = new Filter("ZzSptiskonto", FilterOperator.EQ, oIsk);
          oFilters.push(oFilter);
          // oFilter = new Filter("Zzfld00008d", FilterOperator.EQ, oTahsIndOnce);
          // oFilters.push(oFilter);
          oBusyDialog.open();

          oView.getModel().read("/defaultValuesSet", {
            filters: oFilters,
            success: function (oData, response) {
              oView
                .byId("idIskonto")
                .setValue(response.data.results[0].ZzSptiskonto);
              oIsk = oView.byId("idIskonto").getValue();
              if (oIsk) {
                oTahsInd = oIsk;
              }

              oTahsIndSonra = oTahsIndOnce - oTahsInd;
              oView.byId("idTotal").setText(oTahsIndSonra);
              oBusyDialog.close();
            },
            error: function (oError) {
              oBusyDialog.close();
            },
          });
        },

        handleSelectProdCancel: function (oEvent) {
          this._oSelProdHelpDialog.close();
        },
        handleSelectProdSearch: function (oEvent) {
          var oModelProd = this.getView().getModel();

          var oGuid = this.getView().getModel("oModel").oData.Guid;
          var oSipNo = sap.ui.getCore().byId("idSipNo").getValue();
          var oKalemNo = sap.ui.getCore().byId("idKalemNo").getValue();
          var oUrunKod = sap.ui.getCore().byId("idUrunKod").getValue();
          var oUrunTanim = sap.ui.getCore().byId("idUrunTanim").getValue();

          var oBusyDialog = new sap.m.BusyDialog();

          var oFilters = [];

          if (oGuid !== "00000000-0000-0000-0000-000000000000") {
            oFilters.push(
              new Filter({
                path: "OrderGuid",
                value1: oGuid,
                operator: FilterOperator.EQ,
              })
            );
          }

          if (oSipNo) {
            oFilters.push(
              new Filter({
                path: "SiparisNo",
                value1: oSipNo,
                operator: FilterOperator.EQ,
              })
            );
          }

          if (oKalemNo) {
            oFilters.push(
              new Filter({
                path: "KalemNo",
                value1: oKalemNo,
                operator: FilterOperator.EQ,
              })
            );
          }
          if (oUrunKod) {
            oFilters.push(
              new Filter({
                path: "UrunKodu",
                value1: oUrunKod,
                operator: FilterOperator.EQ,
              })
            );
          }
          if (oUrunTanim) {
            oFilters.push(
              new Filter({
                path: "UrunTanimi",
                value1: oUrunTanim,
                operator: FilterOperator.EQ,
              })
            );
          }

          var oModelJsonList = new sap.ui.model.json.JSONModel();
          var that = this;
          oBusyDialog.open();
          oModelProd.read("/teshirdenSecSet", {
            filters: oFilters,
            success: function (oData, response) {
              that.stockFilter(oData);
              oModelJsonList.setData(oData);
              that._oSelProdHelpDialog.setModel(oModelJsonList, "oTeshir");
              oBusyDialog.close();
            },
            error: function (oError) {
              oBusyDialog.close();
            },
          });
        },

        stockFilter: function (oData) {
          var oSepetList = this.getView()
            .getModel("oModel")
            .getProperty("/ToItems/results");

          for (var i = 0; i < oSepetList.length; i++) {
            if (
              oSepetList[i].ZzaTeshirblgno !== "" &&
              oSepetList[i].Zzfld000083 !== ""
            ) {
              for (var j = 0; j < oData.results.length; j++) {
                if (
                  oData.results[j].SiparisNo === oSepetList[i].ZzaTeshirblgno &&
                  oData.results[j].KalemNo === oSepetList[i].Zzfld000083
                ) {
                  oData.results[j].Stok =
                    oData.results[j].Stok - oSepetList[i].Quantity;
                  if (oData.results[j].Stok <= 0) {
                    oData.results.splice(j, 1);
                  }
                }
              }
            }
          }

          return oData;
        },

        handleSelectProdAdd: function (oEvent) {
          var oTab = this.getView().byId("SepetTabId");
          var List = oTab.getModel("oModel").getProperty("/ToItems/results");
          var oSelProdTab = sap.ui.getCore().byId("SelProdTabId");
          var oSelected = oSelProdTab.getSelectedItems();

          for (var i = 0; i < oSelected.length; i++) {
            var oInd = oSelected[i].getBindingContextPath().split("/")[2];
            var oSipNo = oSelProdTab.getItems()[oInd].getCells()[0].getText();
            var oKalemNo = oSelProdTab.getItems()[oInd].getCells()[1].getText();
            var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[2].getText();
            var oIndirim = oSelProdTab.getItems()[oInd].getCells()[5].getText();

            var oRow = {
              OrderedProd: oUrunKod,
              Description: "",
              Quantity: "1.000",
              ProcessQtyUnit: "ADT",
              Conditionamount1: "0.000",
              Brmfytkdvli: "0.000",
              KampInd: "0.000",
              ThslatInd: "0.000",
              Zzfld00008x: "0.000",
              Currency: "",
              Zzerpstatus: "",
              Zzspecad: "",
              Zzteshirdepo: "",
              ShowConfigButton: "X",
              Zzfld000083: oKalemNo,
              ZzaTeshirblgno: oSipNo,
              ZzTeshirind: oIndirim,
            };

            List.push(oRow);
          }

          oTab.getModel("oModel").setProperty("/ToItems/results", List);
          oTab.getModel("oModel").refresh();
          this._oProdHelpDialog.close();
        },
        // onProdValueHelp: function (oEvent) {
        // 	var oModelSearch = this.getOwnerComponent().getModel("search");
        // 	var oModelHeaderSearch = this.getOwnerComponent().getModel("headersearch");
        // 	// if (this.oModel || !this._oProdHelpDialog) {
        // 	if (sap.ui.getCore().byId("idNewProdDialog")) {
        // 		oModelSearch.setData({});
        // 		oModelHeaderSearch.setData({});
        // 	} else {
        // 		this._oProdHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.ProdSearch", this);
        // 		this.getView().addDependent(this._oProdHelpDialog);
        // 	}
        // 	this._oProdHelpDialog.open();

        // 	// } else {
        // 	// 	this._oProdHelpDialog.open();
        // 	// }

        // 	this.Source = oEvent.getSource();

        // },

        // handleProdCancel: function (oEvent) {
        // 	this._oProdHelpDialog.close();
        // },
        // handleProdSearch: function (oEvent) {

        // 	var oModelProd = this.getView().getModel("PROD");

        // 	var oProdId = sap.ui.getCore().byId("idProdId").getValue();
        // 	var oProdDes = sap.ui.getCore().byId("idProdDesc").getValue();
        // 	var oCatId = sap.ui.getCore().byId("idCatId").getValue();
        // 	var oCatDes = sap.ui.getCore().byId("idCatDesc").getValue();
        // 	var oRow = sap.ui.getCore().byId("idProdRows").getValue();

        // 	var oBusyDialog = new sap.m.BusyDialog();

        // 	var oFilters = [];
        // 	if (oProdId !== "") {
        // 		var oFilter = new Filter("ProductId", FilterOperator.EQ, oProdId);
        // 		oFilters.push(oFilter);
        // 	}
        // 	if (oProdDes !== "") {
        // 		oFilter = new Filter("ShortText", FilterOperator.EQ, oProdDes);
        // 		oFilters.push(oFilter);
        // 	}
        // 	if (oCatId !== "") {
        // 		oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
        // 		oFilters.push(oFilter);
        // 	}
        // 	if (oCatDes !== "") {
        // 		oFilter = new Filter("CategoryDesc", FilterOperator.EQ, oCatDes);
        // 		oFilters.push(oFilter);
        // 	}
        // 	if (oRow !== "") {
        // 		oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
        // 		oFilters.push(oFilter);
        // 	}

        // 	// var oModelJsonList = new sap.ui.model.json.JSONModel();
        // 	var oModelSearch = this.getOwnerComponent().getModel("search");
        // 	// var that = this;
        // 	oBusyDialog.open();
        // 	oModelProd.read("/productSearchSet", {
        // 		filters: oFilters,
        // 		success: function (oData, response) {
        // 			// oModelJsonList.setData(oData);
        // 			// that._oProdHelpDialog.setModel(oModelJsonList, "oProd");
        // 			oModelSearch.setData({
        // 				oProd: oData.results
        // 			});
        // 			oBusyDialog.close();
        // 		}.bind(this),
        // 		error: function (oError) {
        // 			oBusyDialog.close();
        // 		}
        // 	});

        // },

        // handleProdListItemPress: function (oEvent) {
        // 	var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
        // 	var oProd = sap.ui.getCore().byId("ProdTabId").getItems()[oInd].getCells()[0].getText();
        // 	this.Source.setValue(oProd);
        // 	// var oProdText = sap.ui.getCore().byId("ProdTabId").getItems()[oInd].getCells()[1].getText();
        // 	// var oTextId = this.Source.getId();
        // 	// oTextId = oTextId.substr(0, 7) + "13" + oTextId.substr(9);
        // 	// sap.ui.getCore().byId(oTextId).setValue(oProdText);
        // 	this._oProdHelpDialog.close();
        // },
        // onCatValueHelp: function (oEvent) {
        // 	var oModelSearch = this.getOwnerComponent().getModel("search");
        // 	var oModelHeaderSearch = this.getOwnerComponent().getModel("headersearch");

        // 	if (sap.ui.getCore().byId("idNewCatDialog")) {
        // 		oModelSearch.setData({});
        // 		oModelHeaderSearch.setData({});
        // 	} else {
        // 		this._oCatHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.CatSearch", this);
        // 		this.getView().addDependent(this._oCatHelpDialog);
        // 		this._oCatHelpDialog.open();

        // 	}
        // 	this._oCatHelpDialog.open();

        // },

        // handleCatCancel: function () {
        // 	this._oCatHelpDialog.close();
        // },
        // handleCatSearch: function () {

        // 	var oModelProd = this.getView().getModel("PROD");

        // 	var oCatId = sap.ui.getCore().byId("idCategId").getValue();
        // 	var oCatDes = sap.ui.getCore().byId("idCategDesc").getValue();
        // 	var oHyrId = sap.ui.getCore().byId("idHyrType").getSelectedKey();
        // 	var oHyrDes = sap.ui.getCore().byId("idHyrDes").getValue();
        // 	var oRow = sap.ui.getCore().byId("idCatRows").getValue();

        // 	var oBusyDialog = new sap.m.BusyDialog();

        // 	var oFilters = [];
        // 	if (oCatId !== "") {
        // 		var oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
        // 		oFilters.push(oFilter);
        // 	}
        // 	if (oCatDes !== "") {
        // 		oFilter = new Filter("CategoryText", FilterOperator.EQ, oCatDes);
        // 		oFilters.push(oFilter);
        // 	}
        // 	if (oHyrId !== "") {
        // 		oFilter = new Filter("HierarchyId", FilterOperator.EQ, oHyrId);
        // 		oFilters.push(oFilter);
        // 	}
        // 	if (oHyrDes !== "") {
        // 		oFilter = new Filter("HierarchyText", FilterOperator.EQ, oHyrDes);
        // 		oFilters.push(oFilter);
        // 	}
        // 	if (oRow !== "") {
        // 		oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
        // 		oFilters.push(oFilter);
        // 	}

        // 	// var oModelJsonList = new sap.ui.model.json.JSONModel();
        //     var oModelSearch = this.getOwnerComponent().getModel("search");
        // 	// var that = this;
        // 	oBusyDialog.open();
        // 	oModelProd.read("/categorySearchSet", {
        // 		filters: oFilters,
        // 		success: function (oData, response) {
        // 			oModelSearch.setData({
        // 				oCat: oData.results
        // 			});
        // 			// oModelJsonList.setData(oData);
        // 			// that._oCatHelpDialog.setModel(oModelJsonList, "oCat");
        // 			oBusyDialog.close();
        // 		}.bind(this),
        // 		error: function (oError) {
        // 			oBusyDialog.close();
        // 		}
        // 	});

        // },

        // handleCatListItemPress: function (oEvent) {
        // 	var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
        // 	var oPartner = sap.ui.getCore().byId("CatTabId").getItems()[oInd].getCells()[0].getText();
        // 	this.getOwnerComponent().getModel("headersearch").setData({
        // 		CatId: oPartner
        // 	});
        // 	// sap.ui.getCore().byId("idCatId").setValue(oPartner);
        // 	this._oCatHelpDialog.close();
        // },
        onItemConfig: function (oEvent) {
          // sepet tablosunda konfigürasyon butonuna basılınca konfigürasyon ekranı açılır.
          var oInd = oEvent
            .getSource()
            .getBindingContext("oModel")
            .getPath()
            .slice(-1);
          var oItems = this.getView()
            .getModel("oModel")
            .getProperty("/ToItems/results");
          var oItem = oItems[oInd];
          var oFilters = [];
          var currentItemNumber;
          var oModel = this.getView().getModel("oModel");
          var aConfig = oModel.getProperty("/ToConfig/results");
          var oModelJsonList = new sap.ui.model.json.JSONModel();
          oModelJsonList.setSizeLimit(99999);

          if (
            oItem.Parent &&
            oItem.Parent !== "00000000-0000-0000-0000-000000000000"
          ) {
            var oFilter = new Filter(
              "ParentId",
              FilterOperator.EQ,
              oItem.Parent
            );
            oFilters.push(oFilter);
          } else {
            currentItemNumber = String((parseInt(oInd, 10) + 1) * 10);
            oItem.NumberInt = currentItemNumber;
            oFilter = new Filter(
              "ZzorderedProd",
              FilterOperator.EQ,
              oItem.OrderedProd
            );
            oFilters.push(oFilter);
            oFilter = new Filter(
              "ItemNoForConfig",
              FilterOperator.EQ,
              currentItemNumber
            );

            oFilters.push(oFilter);
          }

          aConfig = aConfig.filter(function removeOldConfig(item) {
            return +item.ItemNoForConfig === +currentItemNumber;
          });

          if (!this._oConfigScreen) {
            this._oConfigScreen = sap.ui.xmlfragment(
              "zcrm.zcrm_inquire_price.fragments.ItemConfig",
              this
            );
            this.getView().addDependent(this._oConfigScreen);
            this._oConfigScreen.open();
          } else {
            this._oConfigScreen.open();
          }
          var that = this;
          // item config bilgileri

          if (aConfig.length) {
            oModelJsonList.setData({
              results: aConfig,
            });
            this.getOwnerComponent().setModel(oModelJsonList, "oConfig");
          } else {
            this.getView()
              .getModel()
              .read("/itemConfigSet", {
                filters: oFilters,
                success: function (oData, response) {
                  oModelJsonList.setData(oData);

                  this.getOwnerComponent().setModel(oModelJsonList, "oConfig");
                  oData.results.forEach(
                    function (oLine) {
                      this.loadConfigSearchHelp(oModelJsonList, oLine);
                    }.bind(this)
                  );
                  this.bindCharacteristics(oData);
                }.bind(this),
                error: function (oError) {
                  var message = oError.responseText;
                  sap.m.MessageBox.show(message, {
                    icon: sap.m.MessageBox.Icon.ERROR,
                    title: "HATA",
                    actions: [sap.m.MessageBox.Action.OK],
                  });
                },
              });
          }
        },
        // onItemConfig: function (oEvent) {

        // 	var oInd = oEvent.getSource().getBindingContext("oModel").getPath().slice(-1);
        // 	var oItems = this.getView().getModel("oModel").getProperty("/ToItems/results");
        // 	var oItem = oItems[oInd];
        // 	var oFilters = [];

        // 	if (oItem.Parent && oItem.Parent !== "00000000-0000-0000-0000-000000000000") {
        // 		var oFilter = new Filter("ParentId", FilterOperator.EQ, oItem.Parent);
        // 		oFilters.push(oFilter);
        // 	} else {
        // 	oItem.NumberInt = String((parseInt(oInd, 10) + 1) * 10);
        // 		oFilter = new Filter("ZzorderedProd", FilterOperator.EQ, oItem.OrderedProd);
        // 		oFilters.push(oFilter);
        // 		oFilter = new Filter("ItemNoForConfig", FilterOperator.EQ, oItem.NumberInt);
        // 		oFilters.push(oFilter);
        // 	}

        // 	if (!this._oConfigScreen) {
        // 		this._oConfigScreen = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.ItemConfig", this);
        // 		this.getView().addDependent(this._oConfigScreen);
        // 		this._oConfigScreen.open();
        // 	} else {

        // 		this._oConfigScreen.open();
        // 	}

        // 	var oModelJsonList = new sap.ui.model.json.JSONModel();
        // 	var that = this;

        // 	this.getView().getModel().read("/itemConfigSet", {
        // 		filters: oFilters,
        // 		success: function (oData, response) {
        // 			oModelJsonList.setData(oData);
        // 			this.getOwnerComponent().setModel(oModelJsonList, "oConfig");
        // 			setTimeout(function(){
        //                  sap.ui.getCore().byId("itemConfigId").getItems().forEach(function(row){
        //                  	row.getCells()[3].attachLoadItems(this.loadConfigItem.bind(this));
        //                  }.bind(this));
        // 			}.bind(this), 100);
        // 		}.bind(this),
        // 		error: function (oError) {
        // 			var message = oError.responseText;
        // 			sap.m.MessageBox.show(message, {
        // 				icon: sap.m.MessageBox.Icon.ERROR,
        // 				title: "HATA",
        // 				actions: [sap.m.MessageBox.Action.OK]
        // 			});
        // 		}.bind(this)
        // 	});

        // },
        handleConfigSave: function () {
          debugger;
          var oModel = this.getView().getModel("oModel");
          var aConfig = oModel.getProperty("/ToConfig/results");
          var oConfigModel = this.getView()
            .getModel("oConfig")
            .getData().results;
          if (aConfig && oConfigModel) {
            var currentItemNumber = oConfigModel[0].ItemNoForConfig;

            aConfig = aConfig
              .filter(function removeOldConfig(item) {
                return item.ItemNoForConfig !== currentItemNumber;
              })
              .concat(oConfigModel);
          }
          this.getView().getModel("oModel").setProperty("/ToConfig", {
            results: aConfig,
          });
          // this.onSave(false);
          this.onSave(
            false,
            function () {
              // this.recalculateAllFields();
            }.bind(this)
          );
          this._oConfigScreen.close();
          // this.getView().getModel("oModel").setProperty("/ToConfig", this.getView().getModel("oConfig").getData());
          // this.onSave(false);
          // this._oConfigScreen.close();
        },
        handleConfigCancel: function () {
          this._oConfigScreen.close();
        },
        toSenetOlustur: function () {
          var message;
          var oCurDate = new Date();
          var oTab = this.getView().byId("TahsilatTabId");
          var oTahsilatList = oTab
            .getModel("oModel")
            .getProperty("/ToTahsilat/results");

          var oRow = oTahsilatList[oTahsilatList.length - 1];

          if (
            this.getView().getModel("oModel").getProperty("/ToPricing")
              .PriceList === "02"
          ) {
            message = "Kampanyalı fiyat üzerinden senetli satış yapılamaz!";
            sap.m.MessageBox.show(message, {
              icon: sap.m.MessageBox.Icon.ERROR,
              title: "HATA",
              actions: [sap.m.MessageBox.Action.OK],
            });
          } else if (oRow.Zzfld0000c2 === "") {
            message = "Lütfen senet adedi giriniz";
            sap.m.MessageBox.show(message, {
              icon: sap.m.MessageBox.Icon.ERROR,
              title: "HATA",
              actions: [sap.m.MessageBox.Action.OK],
            });
          } else if (oRow.Zzfld0000c2 > 12) {
            message = "En fazla 12 adet senet oluşturabilirsiniz";
            sap.m.MessageBox.show(message, {
              icon: sap.m.MessageBox.Icon.ERROR,
              title: "HATA",
              actions: [sap.m.MessageBox.Action.OK],
            });
          } else if (oRow.Zzfld0000c3.getFullYear() === "") {
            message = "Senet oluşturmadan önce ilk senet tarihini giriniz";
            sap.m.MessageBox.show(message, {
              icon: sap.m.MessageBox.Icon.ERROR,
              title: "HATA",
              actions: [sap.m.MessageBox.Action.OK],
            });
          } else if (
            Math.ceil(
              Math.abs(oRow.Zzfld0000c3.getTime() - oCurDate.getTime()) /
                (1000 * 60 * 60 * 24)
            ) > 45
          ) {
            message = "İlk senet tarihi 45 günden ileri bir tarih seçilemez";
            sap.m.MessageBox.show(message, {
              icon: sap.m.MessageBox.Icon.ERROR,
              title: "HATA",
              actions: [sap.m.MessageBox.Action.OK],
            });
          } else {
            var oCount = oRow.Zzfld0000c2;
            var oTip;
            var oBirimMiktar = oRow.Zzfld00007y / oCount;
            var oTarih = oRow.Zzfld0000c3;
            var oYeniTarih = new Date(oTarih.valueOf());

            while (oCount > 0) {
              oCount--;
              oTip = "OT-" + (oTahsilatList.length + 1) * 10;

              var oRowNew = {
                Zzfld00005v: oTip,
                Zzfld00005w: oRow.Zzfld00005v,
                Zzfld000085: "",
                Zzfld000044: "10", ////
                Zzfld00007u: "",
                Zzfld000081: "",
                Zzfld0000an: "",
                Zzfld0000at: "",
                Zzfld0000c2: "",
                Zzfld0000c4: new Date(oYeniTarih),
                Zzfld000045: "",
                Zzfld00007y: oBirimMiktar,
                Zzfld00007w: "0.000",
                Zzfld00007x: "0.000",
                Zzfld00009h: "",
                Zzfld0000bf: "",
              };
              oTahsilatList.push(oRowNew);
              oYeniTarih.setMonth(oYeniTarih.getMonth() + 1);
            }
            oTab
              .getModel("oModel")
              .setProperty("/ToTahsilat/results", oTahsilatList);
            oTab.getModel("oModel").refresh();
          }
        },

        showMessages: function (oMessages) {
          if (oMessages.results) {
            if (oMessages.results.length > 0) {
              if (!this._oStockDialog) {
                this._oStockDialog = sap.ui.xmlfragment(
                  "zcrm.zcrm_inquire_price.fragments.StockControl",
                  this
                );
                this.getView().addDependent(this._oStockDialog);
                this._oStockDialog.open();
              } else {
                this._oStockDialog.open();
              }
            }
          }
        },
        handleStockCancel: function () {
          this._oStockDialog.close();
        },
        onStokKontrol: function () {
          this.getView().getModel("oModel").setProperty("/StokSorgu", "X");
          this.onSave(false);
        },
        onPressOrder: function () {
          sap.ui
            .getCore()
            .setModel(this.getView().getModel("oModel"), "oModelGlobal");

          var oView = this.getView();

          if (sap.ui.getCore().byId("idNewOrderDialog")) {
            sap.ui.getCore().byId("idNewOrderDialog").destroy();
          }
          this._OrderType = sap.ui.xmlfragment(
            "zcrm.zcrm_inquire_price.fragments.NewOrderType",
            this
          );
          this.getView().addDependent(this._OrderType);
          this._OrderType.open();
        },

        handleOrderTypeSelect: function () {
          var oText = sap.ui
            .getCore()
            .byId("idOrderTypes")
            .getSelectedButton()
            .getText();

          var oId = oText.substr(oText.length - 4);
          this._OrderType.close();

          var partnerLen =
            this.getView().getModel("oModel").oData.ToPartners.results.length;

          this.getView()
            .getModel("oModel")
            .oData.ToPartners.results.splice(0, partnerLen);
          this.getView().getModel("oModel").oData.ProcessType = oId;
          debugger;
          var oKapampanyaBelirle = this.getView()
            .getModel("KampanyaBelirle")
            .getProperty("/");

          if (oKapampanyaBelirle === "X") {
            this.getView().getModel("oModel").oData.KampanyaBelirle = "X";
          }
          this.getView().getModel("oModel").refresh();
          debugger;

          var crossData = this.getOwnerComponent().resetModel(
            this.getView().getModel("oModel").oData,
            {
              Guid: "00000000-0000-0000-0000-000000000000",
              ProcessType: oId,
              ToStatus: {
                Status: status,
              },
            },
            [
              "ToCumulate",
              "ToItems",
              "ToPricing",
              "ToCustomerExt",
              "ObjectType",
              "Zzfld000057",
              "Zzfld00008c",
              "Zzfld00008d",
              "Zzfld00009e",
              "ZzIndirimTutar",
              "ZzOdenenTutar",
              "ZzKalanTutar",
              "ZzPersiptop",
              "ToConfig",
              "KampanyaBelirle",
            ]
          );

          crossData.ToCustomerExt.RefGuid =
            "00000000-0000-0000-0000-000000000000";
          crossData.ToPricing.RefGuid = "00000000-0000-0000-0000-000000000000";

          crossData.PostingDate = crossData.PostingDate || new Date();
          crossData.ToCustomerExt.Zzfld00009x =
            crossData.ToCustomerExt.Zzfld00009x || new Date();
          crossData.ToPricing.PriceDate =
            crossData.ToPricing.PriceDate || new Date();

          sap.ui.getCore().setModel(
            new sap.ui.model.json.JSONModel({
              sendToSiparis: crossData,
            }),
            "crossData"
          );
          var xnavservice =
            sap.ushell &&
            sap.ushell.Container &&
            sap.ushell.Container.getService &&
            sap.ushell.Container.getService("CrossApplicationNavigation");

          xnavservice.toExternal({
            target: {
              semanticObject: "ZCRM_SALES",
              action: "display",
            },
          });

          // sap.ui.core.UIComponent.getRouterFor(this).navTo("CreateOrder", {
          // 	guid: "00000000-0000-0000-0000-000000000000",
          // 	orderType: oId
          // });
          // var oModelSearch = this.getOwnerComponent().getModel("search");
          // var oModelHeaderSearch = this.getOwnerComponent().getModel("headersearch");
          // sap.ui.getCore().byId("idNewProdDialog").destroy();
          // sap.ui.getCore().byId("idNewCatDialog").destroy();
          // oModelSearch.setData({});
          // oModelHeaderSearch.setData({});
        },
        handleOrderTypeCancel: function () {
          this._OrderType.close();
        },

        onPressProposal: function () {
          var partnerLen =
            this.getView().getModel("oModel").oData.ToPartners.results.length;

          this.getView()
            .getModel("oModel")
            .oData.ToPartners.results.splice(0, partnerLen);

          var oKapampanyaBelirle = this.getView()
            .getModel("KampanyaBelirle")
            .getProperty("/");

          if (oKapampanyaBelirle === "X") {
            this.getView().getModel("oModel").oData.KampanyaBelirle = "X";
          }
          this.getView().getModel("oModel").refresh();

          var crossData = this.getOwnerComponent().resetModel(
            this.getView().getModel("oModel").oData,
            {
              Guid: "00000000-0000-0000-0000-000000000000",
              ProcessType: "ZS05",
              ToStatus: {
                Status: status,
              },
            },
            [
              "ToCumulate",
              "ToItems",
              "ToPricing",
              "ToCustomerExt",
              "ObjectType",
              "Zzfld000057",
              "Zzfld00008c",
              "Zzfld00008d",
              "Zzfld00009e",
              "ZzIndirimTutar",
              "ZzOdenenTutar",
              "ZzKalanTutar",
              "ZzPersiptop",
              "KampanyaBelirle",
              "ToConfig",
            ]
          );
          crossData.ToCustomerExt.RefGuid =
            "00000000-0000-0000-0000-000000000000";
          crossData.ToPricing.RefGuid = "00000000-0000-0000-0000-000000000000";

          crossData.PostingDate = crossData.PostingDate || new Date();
          crossData.ToCustomerExt.Zzfld00009x =
            crossData.ToCustomerExt.Zzfld00009x || new Date();
          crossData.ToPricing.PriceDate =
            crossData.ToPricing.PriceDate || new Date();

          sap.ui.getCore().setModel(
            new sap.ui.model.json.JSONModel({
              sendToTeklif: crossData,
            }),
            "crossData"
          );
          var xnavservice =
            sap.ushell &&
            sap.ushell.Container &&
            sap.ushell.Container.getService &&
            sap.ushell.Container.getService("CrossApplicationNavigation");

          xnavservice.toExternal({
            target: {
              semanticObject: "ZCRM_QUOT",
              action: "display",
            },
          });

          sap.ui
            .getCore()
            .setModel(this.getView().getModel("oModel"), "oModelGlobal");
          // sap.ui.core.UIComponent.getRouterFor(this).navTo("CreateProposal", {
          // 	guid: "00000000-0000-0000-0000-000000000000",
          // 	orderType: "ZS05"
          // });
          // var oModelSearch = this.getOwnerComponent().getModel("search");
          // var oModelHeaderSearch = this.getOwnerComponent().getModel("headersearch");
          // sap.ui.getCore().byId("idNewProdDialog").destroy();
          // sap.ui.getCore().byId("idNewCatDialog").destroy();
          // oModelSearch.setData({});
          // oModelHeaderSearch.setData({});
        },
        // onSelectFromCat: function () {

        // 	if (this.oModel || !this._oSelProdCatHelpDialog) {
        // 		if (sap.ui.getCore().byId("idSelectCatProd")) {
        // 			sap.ui.getCore().byId("idSelectCatProd").destroy();
        // 		}
        // 		this._oSelProdCatHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.selectFromCat", this);
        // 		this.getView().addDependent(this._oSelProdCatHelpDialog);
        // 		this._oSelProdCatHelpDialog.open();
        // 	} else {
        // 		this._oSelProdCatHelpDialog.open();
        // 	}

        // },

        handleSelectFromCatCancel: function (oEvent) {
          this._oSelProdCatHelpDialog.close();
        },
        // handleSelectCatSearch: function (oEvent) {
        // 	var oModelProd = this.getView().getModel("PROD");
        // 	var oHead = this.getView().getModel("oModel").getProperty("/");

        // 	// var oTakim = sap.ui.getCore().byId("idTakim").getValue();
        // 	// var oModel = sap.ui.getCore().byId("idModel").getValue();
        // 	var oTakim = sap.ui.getCore().byId("idTakim").getSelectedKey();
        // 	var oModel = sap.ui.getCore().byId("idModel").getSelectedKey();
        // 	var oModelTnm = sap.ui.getCore().byId("idTModelTnm").getValue();
        // 	var oUniteUrunKod = sap.ui.getCore().byId("idUniteUrunKod").getValue();
        // 	var oUrunId = sap.ui.getCore().byId("idUrunId").getValue();
        // 	var oCategId = sap.ui.getCore().byId("idCategId").getValue();

        // 	if (sap.ui.getCore().byId("idConfigEnable").getSelected()) var oConfigEnable = "X";

        // 	var oBusyDialog = new sap.m.BusyDialog();

        // 	var oFilters = [];

        // 	if (oTakim) {
        // 		oFilters.push(new Filter({
        // 			path: 'Zztakim',
        // 			value1: oTakim,
        // 			operator: FilterOperator.EQ
        // 		}));
        // 	}

        // 	if (oModelTnm) {
        // 		oFilters.push(new Filter({
        // 			path: 'CategoryDesc',
        // 			value1: oModelTnm,
        // 			operator: FilterOperator.EQ
        // 		}));
        // 	}
        // 	if (oUniteUrunKod) {
        // 		oFilters.push(new Filter({
        // 			path: 'Description',
        // 			value1: oUniteUrunKod,
        // 			operator: FilterOperator.EQ
        // 		}));
        // 	}
        // 	if (oModel) {
        // 		oFilters.push(new Filter({
        // 			path: 'Zzmodel',
        // 			value1: oModel,
        // 			operator: FilterOperator.EQ
        // 		}));
        // 	}
        // 	if (oUrunId) {
        // 		oFilters.push(new Filter({
        // 			path: 'ProductId',
        // 			value1: oUrunId,
        // 			operator: FilterOperator.EQ
        // 		}));
        // 	}
        // 	if (oCategId) {
        // 		oFilters.push(new Filter({
        // 			path: 'CategoryId',
        // 			value1: oCategId,
        // 			operator: FilterOperator.EQ
        // 		}));
        // 	}
        // 	if (oHead.ProcessType) {
        // 		oFilters.push(new Filter({
        // 			path: 'ProcessType',
        // 			value1: oHead.ProcessType,
        // 			operator: FilterOperator.EQ
        // 		}));
        // 	}
        // 	if (oHead.ToPricing.PriceList) {
        // 		oFilters.push(new Filter({
        // 			path: 'PriceList',
        // 			value1: oHead.ToPricing.PriceList,
        // 			operator: FilterOperator.EQ
        // 		}));
        // 	}
        // 	if (oHead.ToPricing.Currency) {
        // 		oFilters.push(new Filter({
        // 			path: 'Currency',
        // 			value1: oHead.ToPricing.Currency,
        // 			operator: FilterOperator.EQ
        // 		}));
        // 	}
        // 	if (oHead.ToPricing.PriceDate) {
        // 		oFilters.push(new Filter({
        // 			path: 'PriceDate',
        // 			value1: oHead.ToPricing.PriceDate,
        // 			operator: FilterOperator.EQ
        // 		}));
        // 	}
        // 	// if (oHead.ProcessType) {
        // 	// 	oFilters.push(new Filter({
        // 	// 		path: 'SoldToPartnerGuid',
        // 	// 		value1: oConfigEnable,
        // 	// 		operator: FilterOperator.EQ
        // 	// 	}));
        // 	// }

        // 	var oModelJsonList = new sap.ui.model.json.JSONModel();
        // 	var that = this;
        // 	oBusyDialog.open();
        // 	oModelProd.read("/productCatalogSearchSet", {
        // 		filters: oFilters,
        // 		success: function (oData, response) {
        // 			oModelJsonList.setData(oData);
        // 			that._oSelProdCatHelpDialog.setModel(oModelJsonList, "oCatalog");
        // 			oBusyDialog.close();
        // 		},
        // 		error: function (oError) {
        // 			oBusyDialog.close();
        // 		}
        // 	});

        // },

        handleSelectCatAdd: function (oEvent) {
          var oTab = this.getView().byId("SepetTabId");
          var List = oTab.getModel("oModel").getProperty("/ToItems/results");
          var oSelProdTab = sap.ui.getCore().byId("SelProdCatTabId");
          var oSelected = oSelProdTab.getSelectedItems();

          for (var i = 0; i < oSelected.length; i++) {
            var oInd = oSelected[i].getBindingContextPath().split("/")[2];
            var oConfig = oSelProdTab.getItems()[oInd].getCells()[0].getText();
            var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[4].getText();
            // var oIndirim = oSelProdTab.getItems()[oInd].getCells()[5].getText();

            var oRow = {
              OrderedProd: oUrunKod,
              Description: "",
              Quantity: "1.000",
              ProcessQtyUnit: "ADT",
              Conditionamount1: "0.000",
              Brmfytkdvli: "0.000",
              KampInd: "0.000",
              ThslatInd: "0.000",
              Zzfld00008x: "0.000",
              Currency: "",
              Zzerpstatus: "",
              Zzspecad: "",
              Zzteshirdepo: "",
              ShowConfigButton: oConfig,
              // Zzfld000083: oKalemNo,
              // ZzaTeshirblgno: oSipNo,
              // ZzTeshirind: oIndirim
            };

            List.push(oRow);
          }

          oTab.getModel("oModel").setProperty("/ToItems/results", List);
          oTab.getModel("oModel").refresh();
          //			this._oSelProdCatHelpDialog.close();
        },

        handleSelectProdTree: function () {
          // Ürün ağacından seç arama yardımı burada ürün katoloğundan seçilen satırlar kullanıarak ürün ağacı getiririlir.
          var oModelProd = this.getView().getModel("PROD");
          var oHead = this.getView().getModel("oModel").getProperty("/");
          var oSelProdTab = sap.ui.getCore().byId("SelProdCatTabId");
          var oSelected = oSelProdTab.getSelectedItems();
          var oFilterA = [];
          var oFilterProd = [];

          for (var i = 0; i < oSelected.length; i++) {
            var oInd = oSelected[i].getBindingContextPath().split("/")[2];
            var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[4].getText();
            if (oUrunKod) {
              oFilterProd.push(
                new Filter({
                  path: "ProductId",
                  value1: oUrunKod,
                  operator: FilterOperator.EQ,
                  and: false,
                })
              );
            }
          }

          var oFilterOR = new Filter({
            filters: oFilterProd,
            and: false,
          });

          if (oHead.ProcessType) {
            oFilterA.push(
              new Filter({
                path: "ProcessType",
                value1: oHead.ProcessType,
                operator: FilterOperator.EQ,
              })
            );
          }
          if (oHead.ToPricing.PriceList) {
            oFilterA.push(
              new Filter({
                path: "PriceList",
                value1: oHead.ToPricing.PriceList,
                operator: FilterOperator.EQ,
              })
            );
          }
          if (oHead.ToPricing.Currency) {
            oFilterA.push(
              new Filter({
                path: "Currency",
                value1: oHead.ToPricing.Currency,
                operator: FilterOperator.EQ,
              })
            );
          }
          if (oHead.ToPricing.PriceDate) {
            oFilterA.push(
              new Filter({
                path: "PriceDate",
                value1: oHead.ToPricing.PriceDate,
                operator: FilterOperator.EQ,
              })
            );
          }

          var oFilterAND = new Filter({
            filters: oFilterA,
            and: true,
          });
          var oFilters = [
            new Filter({
              and: true,
              filters: [oFilterOR, oFilterAND],
            }),
          ];

          var oBusyDialog = new sap.m.BusyDialog();
          var oModelJsonList = new sap.ui.model.json.JSONModel();
          var that = this;

          oBusyDialog.open();
          oModelProd.read("/productCatalogTreeSearchSet", {
            filters: oFilters,
            success: function (oData, response) {
              oModelJsonList.setData(oData);
              that._oSelProdTreeHelpDialog.setModel(
                oModelJsonList,
                "oProdTree"
              );
              oBusyDialog.close();
            },
            error: function (oError) {
              oBusyDialog.close();
            },
          });

          if (!this._oSelProdTreeHelpDialog) {
            this._oSelProdTreeHelpDialog = sap.ui.xmlfragment(
              "zcrm.zcrm_inquire_price.fragments.SelectFromProdTree",
              this
            );
            this.getView().addDependent(this._oSelProdTreeHelpDialog);
            this._oSelProdTreeHelpDialog.open();
          } else {
            this._oSelProdTreeHelpDialog.open();
          }
        },
        loadConfigItem: function (oEvent) {
          debugger;
          var oModel = oEvent.getSource().getModel("oConfig");
          var oContextData = oEvent
            .getSource()
            .getBindingContext("oConfig")
            .getObject();

          var sClass = oContextData.Zzclass;
          var sOrderedProd = oContextData.ZzorderedProd;

          this.BaseValueHelp(
            "/itemConfigCharacteristicSet",
            "/oKarakteristik",
            {
              Class: sClass,
              OrderedProd: sOrderedProd,
            },
            undefined,
            function (oData) {
              oContextData.__oKarakteristik = oData.results;
              oModel.updateBindings();
            }.bind(this)
          );
        },
        loadConfigSearchHelp: function (oModel, oLine) {
          var sClass = oLine.Zzclass;
          var sOrderedProd = oLine.ZzorderedProd;

          this.BaseValueHelp(
            "/itemConfigCharacteristicSet",
            "/oKarakteristik",
            {
              Class: sClass,
              OrderedProd: sOrderedProd,
            },
            undefined,
            function (oData) {
              oLine.__oKarakteristik = oData.results;
              oModel.updateBindings();
            }
          );
        },
        toKampanya: function () {
          this.getView().getModel("KampanyaBelirle").setProperty("/", "X");
          this.onSave(false);
        },

        _setbarcode: function (Barcode) {
          var that = this;
          var oModelProd = this.getView().getModel("PROD");
          var oTab = this.getView().byId("SepetTabId");
          var List = oTab.getModel("oModel").getProperty("/ToItems/results");

          var oFilters = [];

          oFilters.push(
            new Filter({
              path: "ProductId",
              value1: Barcode,
              operator: FilterOperator.EQ,
            })
          );

          BusyIndicator.show();
          var Promise1 = new Promise(function (resolve, reject) {
            oModelProd.read("/productCatalogSearchSet", {
              filters: oFilters,
              success: function (oData, response) {
                // this.setOwnerModelProperty("search", "/oCatalog", oData.results);
                BusyIndicator.hide();
                try {
                  var config = oData.results[0].Config;
                } catch (error) {
                  config = "";
                }
                resolve(config);
              }.bind(this),
              error: function (oError) {
                BusyIndicator.hide();
              },
            });
          });

          Promise1.then(
            function (result) {
              var oRow = {
                OrderedProd: Barcode,
                Description: "",
                Quantity: "1.000",
                ProcessQtyUnit: "ADT",
                Conditionamount1: "0.000",
                Brmfytkdvli: "0.000",
                KampInd: "0.000",
                ThslatInd: "0.000",
                Zzfld00008x: "0.000",
                Currency: "",
                Zzerpstatus: "",
                Zzspecad: "",
                Zzteshirdepo: "",
                ShowConfigButton: result,
                // Zzfld000083: oKalemNo,
                // ZzaTeshirblgno: oSipNo,
                // ZzTeshirind: oIndirim
              };

              List.push(oRow);

              oTab.getModel("oModel").setProperty("/ToItems/results", List);
              oTab.getModel("oModel").refresh();
              that.onSave(false);
            },
            function (err) {
              console.log(err); // Error:
            }
          );
        }
        // handleSelectProdTreeCancel: function (oEvent) {
        // 	this._oSelProdTreeHelpDialog.close();
        // },
        // handleSelectProdTreeAdd: function (oEvent) {

        // 	var oTab = this.getView().byId("SepetTabId");
        // 	var List = oTab.getModel("oModel").getProperty("/ToItems/results");
        // 	var oSelProdTab = sap.ui.getCore().byId("SelProdTreeTabId");
        // 	var oSelected = oSelProdTab.getSelectedItems();

        // 	for (var i = 0; i < oSelected.length; i++) {

        // 		var oInd = oSelected[i].getBindingContextPath().split('/')[2];
        // 		var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[0].getText();
        // 		var oText = oSelProdTab.getItems()[oInd].getCells()[1].getText();
        // 		var oQuantity = oSelProdTab.getItems()[oInd].getCells()[5].getText();

        // 		var oRow = {
        // 			OrderedProd: oUrunKod,
        // 			Description: oText,
        // 			Quantity: oQuantity,
        // 			ProcessQtyUnit: "",
        // 			Conditionamount1: "0.000",
        // 			Brmfytkdvli: "0.000",
        // 			KampInd: "0.000",
        // 			ThslatInd: "0.000",
        // 			Zzfld00008x: "0.000",
        // 			Currency: "",
        // 			Zzerpstatus: "",
        // 			Zzspecad: "",
        // 			Zzteshirdepo: "",
        // 			ShowConfigButton: ""
        // 		};
        // 		List.push(oRow);
        // 	}

        // 	oTab.getModel("oModel").setProperty("/ToItems/results", List);
        // 	oTab.getModel("oModel").refresh();
        // 	this._oSelProdTreeHelpDialog.close();
        // },
      }
    );
  }
);
