sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/Dialog",
	"sap/m/NotificationListItem",
	"sap/m/List",
	"sap/m/Button",
	"sap/m/BusyDialog",
	"sap/ui/core/BusyIndicator"
], function (Controller, Filter, FilterOperator, Dialog, NotificationListItem, List, Button, BusyDialog,BusyIndicator) {
	"use strict";

	return Controller.extend("zcrm.zcrm_inquire_price.controller.BaseController", {
		onInit: function () {

			this.createActionResultDialog();

		},
		
		getOwnerModelProperty: function (oModel, sPath) {
			return this.getOwnerComponent().getModel(oModel).getProperty(sPath);
		},
		setOwnerModelProperty: function (oModel, sPath, value) {
			return this.getOwnerComponent().getModel(oModel).setProperty(sPath, value);
		},
	
		BaseValueHelp: function (sEntity, sTarget, oFilter, sTargetModelPath, onSuccess, sModel) {
			debugger;
			var aFinalFilter = Object.entries(oFilter || {}).map(function (aFilter) {
				var sFieldName = aFilter[0];
				var value = aFilter[1];
				if (typeof value === "object" && value.path) {
					return new Filter(value);
				}
				return new Filter({
					path: sFieldName,
					value1: value,
					operator: FilterOperator.EQ
				});
			});
			var oModel = this.getOwnerComponent().getModel(sModel);
			BusyIndicator.show();
			oModel.read(sEntity, {
				filters: aFinalFilter,
				success: function (oData, response) {
					if (sTarget) {
						this.setOwnerModelProperty(sTargetModelPath || "headersearch", sTarget, oData.results);
					}
					BusyIndicator.hide();
					if (typeof onSuccess === "function") {
						onSuccess(oData);
					}
				}.bind(this),
				error: function (oError) {
					BusyIndicator.hide();
				}
			});

		},

		clone: function (obj) {
			if (null === obj || "object" != typeof obj) return obj;
			var copy = obj.constructor();
			for (var attr in obj) {
				if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
			}
			return copy;
		},
		editableFormatter: function (oValue) {
			if (this.getView().getModel("oModel").oData.Guid === "00000000-0000-0000-0000-000000000000") {
				return true;
			} else {
				if (!oValue) {
					return true;
				} else {
					return false;
				}
			}
		},

		editableFormatter2: function (oValue) {
			if (this.getView().getModel("oModel").oData.Guid === "00000000-0000-0000-0000-000000000000") {
				return true;
			} else {
				if (this.getView().getModel("oModel").oData.ToStatus.Status === 'E0002' || !oValue) {
					return true;
				} else {
					return false;
				}
			}
		},
		editableFormatterT: function (oOdemeId, oKasa) {
			if (this.getView().getModel("oModel").oData.Guid === "00000000-0000-0000-0000-000000000000") {
				return true;
			} else {
				if (!oOdemeId) {
					return true;
				} else {
					return false;
				}
			}
		},
		editableFormatterT2: function (oOdemeId, oKasa) {
			if (this.getView().getModel("oModel").oData.Guid === "00000000-0000-0000-0000-000000000000") {
				return true;
			} else {
				if (!oOdemeId) {
					return true;
				} else if (oKasa !== "X") {
					return true;
				} else {
					return false;
				}
			}
		},
		editableFormatterTip1: function (oOdemeTip) {
			if (oOdemeTip === "04" || oOdemeTip === "05" || oOdemeTip === "") {
				return false;
			} else {
				return true;

			}
		},
		editableFormatterTip2: function (oOdemeTip) {
			if (oOdemeTip === "06" || oOdemeTip === "08" || oOdemeTip === "") {
				return false;
			} else {
				return true;
			}
		},
		formatterToplam: function (o1, o2) {
			return Number(o1) + Number(o2);
		},

		formatterBirlestir: function (o1, o2, o3) {
			var oSonuc = "";

			if (o1) oSonuc = o1;
			if (o2) oSonuc += o2;
			if (o3) oSonuc += o3;

			return oSonuc;
		},
		onTakim: function (oEvent) {
			var oCode = "0021";
			var oExtension = oEvent.getSource().getSelectedKey();
			this.BaseValueHelp(oCode, oExtension);
		},

		onOdemeKosul: function (oEvent) {
			// var oCode = "0006";
			// var oExtension = oEvent.getSource().getSelectedKey();
			// this.BaseValueHelp2(oCode, oExtension);
		},
		// handleCreateCustomer: function () {

		// 	var oCore = sap.ui.getCore();

		// 	if (!this.CustomerCreate) {
		// 		this.CustomerCreate = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.CreateCustomer", this);
		// 		this.getView().addDependent(this.CustomerCreate);
		// 		this.CustomerCreate.open();
		// 	} else {
		// 		this.CustomerCreate.open();
		// 	}
		// 	oCore.byId("idCustomerType").setSelectedKey("1");
		// 	oCore.byId("idCustomerTax").setVisible(false);
		// 	oCore.byId("idCustomerTaxNo").setVisible(false);
		// 	oCore.byId("idCustCompanyName").setVisible(false);

		// 	var oModel = this.getView().getModel();
		// 	var oView = this.getView();
		// 	var oFilters = [];
		// 	var oFilter = {};
		// 	oFilters = [];
		// 	oFilter = new Filter("Code", FilterOperator.EQ, "0012");
		// 	oFilters.push(oFilter);
		// 	oFilter = new Filter("Extension", FilterOperator.EQ, "1");
		// 	oFilters.push(oFilter);

		// 	oModel.read("/valueHelpSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			var oModelJsonList8 = new sap.ui.model.json.JSONModel(oData);
		// 			oView.setModel(oModelJsonList8, "oCustGrup");
		// 		},
		// 		error: function (oError) {}
		// 	});

		// 	var oModelCust = this.getView().getModel("CUST");

		// 	oModelCust.read("/searchHelpCountrySet", {
		// 		success: function (oData, response) {
		// 			var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
		// 			oModelJsonList.setData(oData);
		// 			oModelJsonList.setSizeLimit(500);
		// 			oView.setModel(oModelJsonList, "oUlke");
		// 		}
		// 	});

		// 	oFilters = [];
		// 	oFilter = new Filter("Land1", FilterOperator.EQ, "TR");
		// 	oFilters.push(oFilter);
		// 	oModelCust.read("/searchHelpRegionSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
		// 			oModelJsonList.setData(oData);
		// 			oView.setModel(oModelJsonList, "oSehir");
		// 		}
		// 	});

		// },
		// onCustomerCreateCancel: function () {
		// 	this.CustomerCreate.close();
		// },
		// onCustomerCreateSave: function () {

		// 	var oModelCust = this.getView().getModel("CUST");
		// 	var oCore = sap.ui.getCore();

		// 	if (this.custAlanKontrol()) {
		// 		var oCustomer = {};

		// 		oCustomer.Lifecyclestage = oCore.byId("idCustomerRole").getSelectedKey();
		// 		oCustomer.BuGroup = oCore.byId("idGroup").getSelectedKey();
		// 		oCustomer.Partnertype = oCore.byId("idCustomerType").getSelectedKey();

		// 		if (oCustomer.Partnertype === "1") // Bireysel müşteri
		// 		{
		// 			oCustomer.Firstname = oCore.byId("idCustomerName").getValue();
		// 			oCustomer.Lastname = oCore.byId("idCustomerSurName").getValue();
		// 			oCustomer.TaxNum = oCore.byId("idCustomerID").getValue();
		// 		} else {
		// 			oCustomer.TaxNum = oCore.byId("idCustomerTaxNo").getValue();
		// 			oCustomer.Name1 = oCore.byId("idCustCompanyName").getValue();
		// 			oCustomer.TaxCenter = oCore.byId("idCustomerTax").getValue();
		// 		}

		// 		var oAdres = {};
		// 		oAdres.StrSuppl1 = oCore.byId("idCustomerAdres").getValue();
		// 		oAdres.StrSuppl3 = oCore.byId("idCustomerAdres2").getValue();
		// 		oAdres.Telephonemob = oCore.byId("idCustMobileNumber").getValue();
		// 		oAdres.Country = oCore.byId("idCustomerCountry").getSelectedKey();
		// 		oAdres.Region = oCore.byId("idCustomerCity").getSelectedKey();
		// 		oAdres.CityNo = oCore.byId("idCustomerDist").getSelectedKey();

		// 		oCustomer.ToAddress = oAdres;

		// 		var oPermission = [];

		// 		oPermission.push({
		// 			Partner: "",
		// 			Zzfld00003iTxt: "Hepsi",
		// 			Zzfld00003i: "HEP",
		// 			Zzfld00003jTxt: "Verilmedi",
		// 			Zzfld00003j: "002",
		// 			Zzfld00003k: "",
		// 			Zzfld00003l: new Date(),
		// 			Zzfld00003m: "",
		// 			Zzfld00003n: ""
		// 		});

		// 		oCustomer.ToPermissionSet = oPermission;

		// 		var that = this;
		// 		var oBusyDialog = new sap.m.BusyDialog();

		// 		oBusyDialog.open();

		// 		oModelCust.create("/customerHeaderSet", oCustomer, {
		// 			success: function (oData, oResponse) {
		// 				// oModelJsonList.setData(oData);
		// 				that.getView().byId("inpu2").setValue(oData.Partner);
		// 				if (oData.FirstName !== "") // Bireysel müşteri
		// 				{
		// 					that.getView().byId("inpu201").setValue(oCustomer.Firstname + oCustomer.Lastname);
		// 				} else {
		// 					that.getView().byId("inpu201").setValue(oCustomer.Name1);
		// 				}
		// 				oBusyDialog.close();
		// 				that.CustomerCreate.close();
		// 			},
		// 			error: function (oError) {
		// 				oBusyDialog.close();
		// 				var message = oError.responseText;
		// 				sap.m.MessageBox.show(message, {
		// 					icon: sap.m.MessageBox.Icon.ERROR,
		// 					title: "HATA",
		// 					actions: [sap.m.MessageBox.Action.OK]
		// 				});
		// 			}
		// 		});

		// 	} else {
		// 		var oMessage = "Lütfen Tüm Zorunlu Alanları Doldurunuz";
		// 		sap.m.MessageBox.show(oMessage, {
		// 			icon: sap.m.MessageBox.Icon.ERROR,
		// 			title: "UYARI",
		// 			actions: [sap.m.MessageBox.Action.OK]
		// 		});
		// 	}

		// },

		// custAlanKontrol: function () {
		// 	var oCore = sap.ui.getCore();
		// 	var oVal = true;

		// 	// if (oCore.byId("idGroup").getSelectedKey() === "" && oCore.byId("idGroup").getVisible()) {
		// 	// 	oCore.byId("idGroup").setValueState(sap.ui.core.ValueState.Error);
		// 	// 	oVal = false;
		// 	// } else {
		// 	// 	oCore.byId("idGroup").setValueState(sap.ui.core.ValueState.None);
		// 	// }

		// 	if (oCore.byId("idCustomerRole").getSelectedKey() === "" && oCore.byId("idCustomerRole").getVisible()) {
		// 		oCore.byId("idCustomerRole").setValueState(sap.ui.core.ValueState.Error);
		// 		oVal = false;
		// 	} else {
		// 		oCore.byId("idCustomerRole").setValueState(sap.ui.core.ValueState.None);
		// 	}

		// 	if (oCore.byId("idCustomerName").getValue() === "" && oCore.byId("idCustomerName").getVisible()) {
		// 		oCore.byId("idCustomerName").setValueState(sap.ui.core.ValueState.Error);
		// 		oVal = false;
		// 	} else {
		// 		oCore.byId("idCustomerName").setValueState(sap.ui.core.ValueState.None);
		// 	}

		// 	if (oCore.byId("idCustomerSurName").getValue() === "" && oCore.byId("idCustomerSurName").getVisible()) {
		// 		oCore.byId("idCustomerSurName").setValueState(sap.ui.core.ValueState.Error);
		// 		oVal = false;
		// 	} else {
		// 		oCore.byId("idCustomerSurName").setValueState(sap.ui.core.ValueState.None);
		// 	}

		// 	if (oCore.byId("idCustCompanyName").getValue() === "" && oCore.byId("idCustCompanyName").getVisible()) {
		// 		oCore.byId("idCustCompanyName").setValueState(sap.ui.core.ValueState.Error);
		// 		oVal = false;
		// 	} else {
		// 		oCore.byId("idCustCompanyName").setValueState(sap.ui.core.ValueState.None);
		// 	}

		// 	if (oCore.byId("idCustMobileNumber").getValue() === "" && oCore.byId("idCustMobileNumber").getVisible()) {
		// 		oCore.byId("idCustMobileNumber").setValueState(sap.ui.core.ValueState.Error);
		// 		oVal = false;
		// 	} else {
		// 		oCore.byId("idCustMobileNumber").setValueState(sap.ui.core.ValueState.None);
		// 	}

		// 	return oVal;
		// },

		// onCountryChange: function () {
		// 	var oView = this.getView();
		// 	var oModelCust = this.getView().getModel("CUST");
		// 	var oCore = sap.ui.getCore();
		// 	var oCountry = oCore.byId("idCustomerCountry").getSelectedKey();
		// 	var oFilters = [];
		// 	var oFilter = {};

		// 	oCore.byId("idCustomerCity").setSelectedKey("");
		// 	oCore.byId("idCustomerDist").setSelectedKey("");

		// 	oFilters = [];
		// 	oFilter = new Filter("Land1", FilterOperator.EQ, oCountry);
		// 	oFilters.push(oFilter);

		// 	oModelCust.read("/searchHelpRegionSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
		// 			oModelJsonList.setData(oData);
		// 			oView.setModel(oModelJsonList, "oSehir");
		// 		}
		// 	});

		// },
		// onCityChange: function () {
		// 	var oView = this.getView();
		// 	var oModelCust = this.getView().getModel("CUST");
		// 	var oCore = sap.ui.getCore();
		// 	var oCountry = oCore.byId("idCustomerCountry").getSelectedKey();
		// 	var oCity = oCore.byId("idCustomerCity").getSelectedKey();

		// 	var oFilters = [];
		// 	var oFilter = {};

		// 	oCore.byId("idCustomerDist").setSelectedKey("");

		// 	oFilters = [];
		// 	oFilter = new Filter("Country", FilterOperator.EQ, oCountry);
		// 	oFilters.push(oFilter);
		// 	oFilter = new Filter("Region", FilterOperator.EQ, oCity);
		// 	oFilters.push(oFilter);

		// 	oModelCust.read("/searchHelpCitySet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
		// 			oModelJsonList.setData(oData);
		// 			oView.setModel(oModelJsonList, "oIlce");
		// 		}
		// 	});

		// },
		// onCustomerTypeChange: function () {
		// 	var oCore = sap.ui.getCore();

		// 	var oSelected = oCore.byId("idCustomerType").getSelectedKey();

		// 	if (oSelected === "1") {
		// 		oCore.byId("idCustomerName").setVisible(true);
		// 		oCore.byId("idCustomerSurName").setVisible(true);
		// 		oCore.byId("idCustomerID").setVisible(true);

		// 		oCore.byId("idCustomerTax").setVisible(false);
		// 		oCore.byId("idCustomerTaxNo").setVisible(false);
		// 		oCore.byId("idCustCompanyName").setVisible(false);
		// 	} else if (oSelected === "2") {
		// 		oCore.byId("idCustomerName").setVisible(false);
		// 		oCore.byId("idCustomerSurName").setVisible(false);
		// 		oCore.byId("idCustomerID").setVisible(false);

		// 		oCore.byId("idCustomerTax").setVisible(true);
		// 		oCore.byId("idCustomerTaxNo").setVisible(true);
		// 		oCore.byId("idCustCompanyName").setVisible(true);
		// 	}

		// },
		fixTabs: function (data) {

			if (!data.ToTahsilat) {
				data.ToTahsilat = {
					results: []
				};
			}
			if (!data.ToPartners) {
				data.ToPartners = {
					results: []
				};
			}
			if (!data.ToMessages) {
				data.ToMessages = {
					results: []
				};
			}
			if (!data.ToItems) {
				data.ToItems = {
					results: []
				};
			}
			if (!data.ToTexts) {
				data.ToTexts = {
					results: []
				};
			}
			if (!data.ToDates) {
				data.ToDates = {
					results: []
				};
			}

		},
		handleProductDetail: function (oEvent) {
			var ProdId = oEvent.getSource().getBindingContext("oModel").getProperty("OrderedProd");
			var aFilter = [];
			if (ProdId) {
				aFilter.push(new Filter("ProductId", FilterOperator.EQ, ProdId));
			}
			if (!this._oDialog) {
				this._oDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.ProdDetail", this);
				this.getView().addDependent(this._oDialog);
				this._oDialog.open();
			} else {

				this._oDialog.open();
			}

			var oTable = sap.ui.getCore().byId("ProdDetail");
			var oBinding = oTable.getBinding("items");
			oBinding.filter(aFilter);

		},
		handleProdDetailCancel: function () {

			this._oDialog.close();
		},

		handlePopoverProd: function (oEvent) {
			var Img = oEvent.getSource().getSrc();
			this.getView().getModel("local").setData({
				Img: Img
			});
			if (!this._oPopover) {
				this._oPopover = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.ProdImage", this);
				this.getView().addDependent(this._oPopover);
			}

			var oButton = oEvent.getSource();
			jQuery.sap.delayedCall(0, this, function () {
				this._oPopover.openBy(oButton);
			});
		},
		onProdValueHelp: function (oEvent) {
			var oModelSearch = this.getOwnerComponent().getModel("search");
			var oModelHeaderSearch = this.getOwnerComponent().getModel("headersearch");
			oModelSearch.setData({});
			oModelHeaderSearch.setData({});

			this.open("ProdSearch").setSource(oEvent.getSource());

		},
		onCustomerValueHelp: function (oEvent) {
			var oModelSearch = this.getOwnerComponent().getModel("search");
			var oModelHeaderSearch = this.getOwnerComponent().getModel("headersearch");
			oModelSearch.setData({});
			oModelHeaderSearch.setData({});
			this.open("CustomerSearch").setSource(oEvent.getSource());

		},

		open: function (sFragmentName) {
			return this.getOwnerComponent().openFragment(sFragmentName, this.getView());
		},
		createActionResultDialog: function () {
			if (!this.oActionResults) {
				this.oActionResults = new Dialog({
					title: "Mesaj",
					content: new List({
						items: {
							path: "actionResult>/",
							template: new NotificationListItem({
								// title: "{actionResult>title}",
								description: "{actionResult>message}",
								showCloseButton: false,
								priority: "{= {'error': 'High', 'warning': 'Medium', 'success': 'Low'}[${actionResult>severity}] }"
							})
						}
					}),
					beginButton: new Button({
						text: "Close",
						press: function () {
							this.oActionResults.close();
						}.bind(this)
					})
				});
				this.getView().addDependent(this.oActionResults);
			}
		},

		showMessageBox: function (oData, type) {
			var list = [];

			if (typeof oData === "string") {
				list = [{
					severity: type || "success",
					message: oData
				}];
			} else if (Array.isArray(oData)) {
				list = oData;
			} else if (oData.responseText) {
				var message = oData.responseText;
				try {
					list = JSON.parse(message).error.innererror.errordetails;
				} catch (e) {
					list = [{
						code: "/Dogtas",
						message: "Bilinmeyen hata",
						severity: "error",
					}];
				}
			}

			this.getView().setModel(new sap.ui.model.json.JSONModel(list), "actionResult");
			this.getView().getController().oActionResults.open();
		},

		onSelectFromCat: function (oEvent) {
			debugger;
			this.setOwnerModelProperty("headersearch", "/", {});
			this.setOwnerModelProperty("search", "/", {});
			this.setOwnerModelProperty("selected", "/", {});
			var oCode = "0020";
			// this.BaseValueHelp(oCode);
			this.BaseValueHelp("/valueHelpSet", "/KatalogTakim", {
				Code: oCode
			});
			this.open("selectFromCat").setSource(oEvent.getSource());

		},
		onKarakteristik: function (oEvent) {
			var oContext = oEvent.getSource().getBindingContext("oConfig");
			var sProperty = oContext.getProperty();
			var sOrderedProd = sProperty.ZzorderedProd;
			var sclass = sProperty.Zzclass;
			var scharCode = sProperty.ZzcharCode;
			this.BaseValueHelp("/defaultValuesSet", /* targetproperty: */null, {
				Code:"0006",
				OrderedProd: sOrderedProd,
				Zzclass: sclass,
				ZzcharCode:scharCode
				
			}, /* targetmodel */null, function onSuccess(oData) {
				if (oData.results && oData.results.length > 0) {
					sProperty.ZzcharValue = oData.results[0].ZzcharValue;
					oContext.getModel().updateBindings();
				}
				
			}.bind(this));

			// ZzcharValue ZzcharValue 
		}


		// handleProdCancel: function (oEvent) {
		// 	this._oProdHelpDialog.close();
		// },
		// handleProdSearch: function (oEvent) {

		// 	var oModelProd = this.getView().getModel("PROD");

		// 	var oProdId = sap.ui.getCore().byId("idProdId").getValue();
		// 	var oProdDes = sap.ui.getCore().byId("idProdDesc").getValue();
		// 	var oCatId = sap.ui.getCore().byId("idCatId").getValue();
		// 	var oCatDes = sap.ui.getCore().byId("idCatDesc").getValue();
		// 	var oRow = sap.ui.getCore().byId("idProdRows").getValue();

		// 	var oBusyDialog = new sap.m.BusyDialog();

		// 	var oFilters = [];
		// 	if (oProdId !== "") {
		// 		var oFilter = new Filter("ProductId", FilterOperator.EQ, oProdId);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oProdDes !== "") {
		// 		oFilter = new Filter("ShortText", FilterOperator.EQ, oProdDes);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oCatId !== "") {
		// 		oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oCatDes !== "") {
		// 		oFilter = new Filter("CategoryDesc", FilterOperator.EQ, oCatDes);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oRow !== "") {
		// 		oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
		// 		oFilters.push(oFilter);
		// 	}

		// 	// var oModelJsonList = new sap.ui.model.json.JSONModel();
		// 	var oModelSearch = this.getOwnerComponent().getModel("search");
		// 	// var that = this;
		// 	oBusyDialog.open();
		// 	oModelProd.read("/productSearchSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			// oModelJsonList.setData(oData);
		// 			// that._oProdHelpDialog.setModel(oModelJsonList, "oProd");
		// 			oModelSearch.setData({
		// 				oProd: oData.results
		// 			});
		// 			oBusyDialog.close();
		// 		}.bind(this),
		// 		error: function (oError) {
		// 			oBusyDialog.close();
		// 		}
		// 	});

		// },

		// handleProdListItemPress: function (oEvent) {
		// 	throw new Error("deprecated");
		// 	var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
		// 	var oProd = sap.ui.getCore().byId("ProdTabId").getItems()[oInd].getCells()[0].getText();
		// 	this.Source.setValue(oProd);
		// 	// var oProdText = sap.ui.getCore().byId("ProdTabId").getItems()[oInd].getCells()[1].getText();
		// 	// var oTextId = this.Source.getId();
		// 	// oTextId = oTextId.substr(0, 7) + "13" + oTextId.substr(9);
		// 	// sap.ui.getCore().byId(oTextId).setValue(oProdText);
		// 	this._oProdHelpDialog.close();
		// },
		// onCatValueHelp: function (oEvent) {
		// 	var oModelSearch = this.getOwnerComponent().getModel("search");
		// 	var oModelHeaderSearch = this.getOwnerComponent().getModel("headersearch");
		// 	oModelSearch.setData({});
		// 	oModelHeaderSearch.setData({});

		// 	this.getView().getController().open("CatSearch").setSource(oEvent.getSource());

		// },

		// handleCatCancel: function () {
		// 	this._oCatHelpDialog.close();
		// },
		// handleCatSearch: function () {

		// 	var oModelProd = this.getView().getModel("PROD");

		// 	var oCatId = sap.ui.getCore().byId("idCategId").getValue();
		// 	var oCatDes = sap.ui.getCore().byId("idCategDesc").getValue();
		// 	var oHyrId = sap.ui.getCore().byId("idHyrType").getSelectedKey();
		// 	var oHyrDes = sap.ui.getCore().byId("idHyrDes").getValue();
		// 	var oRow = sap.ui.getCore().byId("idCatRows").getValue();

		// 	var oBusyDialog = new sap.m.BusyDialog();

		// 	var oFilters = [];
		// 	if (oCatId !== "") {
		// 		var oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oCatDes !== "") {
		// 		oFilter = new Filter("CategoryText", FilterOperator.EQ, oCatDes);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oHyrId !== "") {
		// 		oFilter = new Filter("HierarchyId", FilterOperator.EQ, oHyrId);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oHyrDes !== "") {
		// 		oFilter = new Filter("HierarchyText", FilterOperator.EQ, oHyrDes);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oRow !== "") {
		// 		oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
		// 		oFilters.push(oFilter);
		// 	}

		// 	// var oModelJsonList = new sap.ui.model.json.JSONModel();
		// 	var oModelSearch = this.getOwnerComponent().getModel("search");
		// 	// var that = this;
		// 	oBusyDialog.open();
		// 	oModelProd.read("/categorySearchSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			oModelSearch.setData({
		// 				oCat: oData.results
		// 			});
		// 			// oModelJsonList.setData(oData);
		// 			// that._oCatHelpDialog.setModel(oModelJsonList, "oCat");
		// 			oBusyDialog.close();
		// 		}.bind(this),
		// 		error: function (oError) {
		// 			oBusyDialog.close();
		// 		}
		// 	});

		// },

		// handleCatListItemPress: function (oEvent) {
		// 	var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
		// 	var oPartner = sap.ui.getCore().byId("CatTabId").getItems()[oInd].getCells()[0].getText();
		// 	this.getOwnerComponent().getModel("headersearch").setData({
		// 		CatId: oPartner
		// 	});
		// 	// sap.ui.getCore().byId("idCatId").setValue(oPartner);
		// 	this._oCatHelpDialog.close();
		// },

	});
});