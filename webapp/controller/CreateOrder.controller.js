sap.ui.define([
	"./BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox"
], function (BaseController, Filter, FilterOperator, MessageBox) {
	"use strict";

	return BaseController.extend("zcrm.zcrm_inquire_price.controller.CreateOrder", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf zcrm.zcrm_inquire_price.view.CreateOrder
		 */
		onInit: function () {
			BaseController.prototype.onInit.apply(this, arguments);
			
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("CreateOrder").attachPatternMatched(this._onObjectMatched, this);
			sap.ui.model.SimpleType.extend("sap.ui.model.type.Boolean", {
				formatValue: function (oValue) {
					if (oValue === "X") {
						return true;
					}
					if (oValue === null || oValue === "") {
						return false;
					}
				},
				parseValue: function (oValue) {
					if (oValue === true) {
						return "X";
					} else if (oValue === false) {
						return "";
					}
				},
				validateValue: function (oValue) {
					return oValue;
				}
			});
			this.getView().addStyleClass("sapUiSizeCompact");
		
		},
		_onObjectMatched: function (oEvent) {
			debugger;
			// this.oGuid = oEvent.getParameter("arguments").guid;
			// this.oOrderType = oEvent.getParameter("arguments").orderType;
			// var oModel = sap.ui.getCore().getModel("oModelGlobal");
			// this.getView().setModel(oModel, "oModel");
			// var oData = oModel.oData;

			// this.disableElements(oData.Guid, oData.ToStatus.Status, this.oOrderType);
			// this.bindElements(oData);
			// this.bindSearchHelps(oData);
		this.oGuid = oEvent.getParameter("arguments").guid;
			this.oOrderType = oEvent.getParameter("arguments").orderType;
			var oModel = new sap.ui.model.json.JSONModel();
			var oModelGlobal = sap.ui.getCore().getModel("oModelGlobal");
			this.getView().setModel(oModel, "oModel");
			var guid = oModelGlobal.getProperty("/Guid");
			// var status = oModelGlobal.getProperty("/ToStatus/Status");
			// var status = "E0002";
						oModel.setData(this.getOwnerComponent().resetModel(oModelGlobal.getObject("/"), {
				Guid: guid,
				ProcessType: this.oOrderType,
				ToStatus: {
					Status: status
				},
			}, 
				[ "ToCumulate","ToItems","ToCustomerExt" ,"ToPricing", "ToPartners", "ToTexts", "CreatedBy", "CrmRelease", "DescrLanguage", "LogicalSystem", 
				"ObjectType", "Zzfld000057", "Zzfld00008c", "Zzfld00008d","Zzfld00009e","ZzIndirimTutar", "ZzOdenenTutar", "ZzKalanTutar", "ZzPersiptop"]));

			oModel.setProperty("/ToCustomerExt/RefGuid","00000000-0000-0000-0000-000000000000");
			this.disableElements(guid, status, this.oOrderType);
			this.bindElements(oModel.getObject("/"));
			this.bindSearchHelps(oModel.getObject("/"));
			// 	var oCode = "0005";
			// this.BaseValueHelp2(oCode,this.oOrderType);
		},
		
	
		bindSearchHelps: function (oData) {
			var oModel = this.getView().getModel();
			var oView = this.getView();

			var oFilters = [];
			var oFilter = {};
			var oBayi = oView.byId("inpu5");
			var oAdres = oView.byId("inpu6");
			var oOdemeKosul = oView.byId("inpu19");
			var oOdemeBicim = oView.byId("inpu20");
			var oFlist = oView.byId("inpu25");
			var oCalisan = oView.byId("inpu14");
			var oDurum = oView.byId("inpu17");
			var oSKosul = oView.byId("inpu8");

			oFilter = new Filter("Code", FilterOperator.EQ, "0002");
			oFilters.push(oFilter);

			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList2 = new sap.ui.model.json.JSONModel(oData);
					oBayi.setModel(oModelJsonList2, "oBayi");
				},
				error: function (oError) {}
			});
			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0003");
			oFilters.push(oFilter);
			if (oData) {
				oFilter = new Filter("Extension", FilterOperator.EQ, oData.Zzfld000084);
				oFilters.push(oFilter);
			}

			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList3 = new sap.ui.model.json.JSONModel(oData);
					oAdres.setModel(oModelJsonList3, "oAdres");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0004");
			oFilters.push(oFilter);
			if (oData) {
				oFilter = new Filter("Guid", FilterOperator.EQ, oData.Guid);
				oFilters.push(oFilter);
			}
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList4 = new sap.ui.model.json.JSONModel(oData);
					oCalisan.setModel(oModelJsonList4, "oCalisan");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0005");
			oFilters.push(oFilter);
			if (oData) {
				oFilter = new Filter("Extension", FilterOperator.EQ, oData.ProcessType);
				oFilters.push(oFilter);
			}
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList5 = new sap.ui.model.json.JSONModel(oData);
					oOdemeKosul.setModel(oModelJsonList5, "oOdeme");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0006");
			oFilters.push(oFilter);
			if (oData) {
				oFilter = new Filter("Extension", FilterOperator.EQ, oData.ProcessType);
				oFilters.push(oFilter);
			}
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList6 = new sap.ui.model.json.JSONModel(oData);
					oOdemeBicim.setModel(oModelJsonList6, "oOdemeBicim");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0007");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList7 = new sap.ui.model.json.JSONModel(oData);
					oFlist.setModel(oModelJsonList7, "oFlist");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0009");
			oFilters.push(oFilter);
			if (oData) {
				oFilter = new Filter("Extension", FilterOperator.EQ, oData.ProcessType);
				oFilters.push(oFilter);
				oFilter = new Filter("Guid", FilterOperator.EQ, oData.Guid);
				oFilters.push(oFilter);
			}

			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList8 = new sap.ui.model.json.JSONModel(oData);
					oDurum.setModel(oModelJsonList8, "oDurum");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0010");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList9 = new sap.ui.model.json.JSONModel(oData);
					oSKosul.setModel(oModelJsonList9, "oSKosul");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0011");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList10 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList10, "oDepo");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0012");
			oFilters.push(oFilter);
			oFilter = new Filter("Extension", FilterOperator.EQ, "1");
			oFilters.push(oFilter);

			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList8 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList8, "oCustGrup");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0015");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList11 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList11, "oOdemeTur");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0016");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList12 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList12, "oTahsTur");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0017");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList13 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList13, "oBanka");
				},
				error: function (oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0018");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList14 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList14, "oTahsDurum");
				},
				error: function (oError) {}
			});

			oModel.read("/unitSet", {
				success: function (oData, response) {
					var oModelJsonList15 = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList15.setSizeLimit(500);
					oView.setModel(oModelJsonList15, "oUnit");
				},
				error: function (oError) {}
			});

			oModel.read("/currencySet", {
				success: function (oData, response) {
					var oModelJsonList16 = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList16.setSizeLimit(500);
					oView.setModel(oModelJsonList16, "oCurr");
				},
				error: function (oError) {}
			});
		},

		disableElements: function (oGuid, oStat, oOrderType) {
			var oView = this.getView();
			this.enableElements();
			switch (oOrderType) {
			case 'ZA01':
				oView.byId("inpu2").setVisible(false);
				oView.byId("inpu201").setVisible(false);
				oView.byId("inpu3").setVisible(false);
				oView.byId("inpu7").setVisible(false);
				oView.byId("inpu11").setVisible(false);
				oView.byId("inpu15").setVisible(false);
				oView.byId("inpu16").setVisible(false);
				oView.byId("inpu18").setVisible(false);
				oView.byId("inpu22").setVisible(false);
				oView.byId("inpu23").setVisible(false);
				oView.byId("IconKamp").setVisible(false);
				oView.byId("IconTahsilat").setVisible(false);
				oView.byId("IconDegisim").setVisible(false);
				oView.byId("idTahsScroll").setVisible(false);

				oView.byId("inpu1").setEditable(false);
				oView.byId("inpu20").setEditable(false);
				oView.byId("inpu21").setEditable(false);
				oView.byId("inpu25").setEditable(false);
				oView.byId("inpu251").setEditable(false);
				oView.byId("inpu26").setEditable(false);
				oView.byId("inpu27").setEditable(false);
				oView.byId("inpu28").setEditable(false);
				break;
			case 'ZA04':
				oView.byId("IconNotlar").setVisible(false);

				oView.byId("inpu1").setEditable(false);
				oView.byId("inpu16").setEditable(false);
				oView.byId("inpu19").setEditable(false);
				oView.byId("inpu20").setEditable(false);
				oView.byId("inpu21").setEditable(false);
				oView.byId("inpu26").setEditable(false);
				oView.byId("inpu27").setEditable(false);
				oView.byId("inpu28").setEditable(false);
				oView.byId("inpu29").setEditable(false);
				oView.byId("inpu30").setEditable(false);
				oView.byId("inpu35").setEditable(false);
				oView.byId("inpu36").setEditable(false);
				oView.byId("inpu37").setEditable(false);
				oView.byId("inpu38").setEditable(false);
				oView.byId("inpu39").setEditable(false);
				oView.byId("inpu40").setEditable(false);
				oView.byId("inpu42").setEditable(false);
				oView.byId("inpu43").setEditable(false);
				break;
			case 'ZA14':
				oView.byId("inpu5").setVisible(false);
				oView.byId("inpu6").setVisible(false);
				oView.byId("inpu7").setVisible(false);
				oView.byId("inpu15").setVisible(false);
				oView.byId("inpu16").setVisible(false);
				oView.byId("inpu18").setVisible(false);
				oView.byId("inpu22").setVisible(false);
				oView.byId("inpu23").setVisible(false);
				oView.byId("inpu24").setVisible(false);
				oView.byId("IconDeger").setVisible(false);
				oView.byId("IconKamp").setVisible(false);
				oView.byId("IconTahsilat").setVisible(false);
				oView.byId("IconDegisim").setVisible(false);
				oView.byId("idTahsScroll").setVisible(false);

				oView.byId("inpu1").setEditable(false);
				oView.byId("inpu11").setEditable(false);
				oView.byId("inpu19").setEditable(false);
				oView.byId("inpu20").setEditable(false);

				break;

			case 'ZS20':

				oView.byId("inpu7").setVisible(false);
				oView.byId("inpu15").setVisible(false);
				oView.byId("inpu16").setVisible(false);
				oView.byId("inpu22").setVisible(false);
				oView.byId("inpu23").setVisible(false);
				oView.byId("inpu32").setVisible(false);
				oView.byId("inpu33").setVisible(false);
				oView.byId("inpu34").setVisible(false);
				oView.byId("IconDegisim").setVisible(false);

				oView.byId("inpu1").setEditable(false);
				oView.byId("inpu19").setEditable(false);
				oView.byId("inpu20").setEditable(false);
				oView.byId("inpu21").setEditable(false);
				oView.byId("inpu26").setEditable(false);
				oView.byId("inpu27").setEditable(false);
				oView.byId("inpu28").setEditable(false);
				oView.byId("inpu35").setEditable(false);
				oView.byId("inpu36").setEditable(false);
				oView.byId("inpu37").setEditable(false);
				oView.byId("inpu38").setEditable(false);
				oView.byId("inpu39").setEditable(false);
				oView.byId("inpu40").setEditable(false);

				break;
			case 'ZA15':

				oView.byId("inpu1").setEditable(false);
				oView.byId("inpu2").setEditable(false);
				oView.byId("inpu16").setEditable(false);
				oView.byId("inpu19").setEditable(false);
				oView.byId("inpu20").setEditable(false);
				oView.byId("inpu26").setEditable(false);
				oView.byId("inpu27").setEditable(false);
				oView.byId("inpu28").setEditable(false);
				oView.byId("inpu34").setEditable(false);
				oView.byId("inpu35").setEditable(false);
				oView.byId("inpu36").setEditable(false);
				oView.byId("inpu37").setEditable(false);
				oView.byId("inpu38").setEditable(false);
				oView.byId("inpu39").setEditable(false);
				oView.byId("inpu40").setEditable(false);
				oView.byId("inpu42").setEditable(false);
				oView.byId("inpu43").setEditable(false);

				break;
			}

			// if (oGuid !== "00000000-0000-0000-0000-000000000000") {
			// 	oView.byId("inpu1").setEditable(false);
			// 	oView.byId("inpu2").setEditable(false);
			// 	oView.byId("inpu16").setEditable(false);
			// 	oView.byId("inpu19").setEditable(false);
			// 	oView.byId("inpu20").setEditable(false);

			// 	oView.byId("inpu26").setEditable(false);
			// 	oView.byId("inpu27").setEditable(false);
			// 	oView.byId("inpu28").setEditable(false);
			// 	oView.byId("inpu21").setEditable(false);
			// 	oView.byId("inpu29").setEditable(false);
			// 	oView.byId("inpu30").setEditable(false);
			// 	oView.byId("inpu34").setEditable(false);
			// 	oView.byId("inpu35").setEditable(false);
			// 	oView.byId("inpu36").setEditable(false);
			// 	oView.byId("inpu37").setEditable(false);
			// 	oView.byId("inpu38").setEditable(false);
			// 	oView.byId("inpu39").setEditable(false);
			// 	oView.byId("inpu40").setEditable(false);

			// 	if (oStat !== "E0002") {
			// 		oView.byId("inpu3").setEditable(false);
			// 		oView.byId("inpu4").setEditable(false);
			// 		oView.byId("inpu5").setEditable(false);
			// 		oView.byId("inpu6").setEditable(false);
			// 		oView.byId("inpu7").setEditable(false);
			// 		oView.byId("inpu8").setEditable(false);
			// 		oView.byId("inpu11").setEditable(false);
			// 		oView.byId("inpu14").setEditable(false);
			// 		oView.byId("inpu15").setEditable(false);
			// 		oView.byId("inpu17").setEditable(false);
			// 		oView.byId("inpu18").setEditable(false);
			// 		oView.byId("inpu22").setEditable(false);
			// 		oView.byId("inpu23").setEditable(false);
			// 		oView.byId("inpu24").setEditable(false);
			// 		oView.byId("inpu31").setEditable(false);
			// 		oView.byId("inpu32").setEditable(false);
			// 		oView.byId("inpu33").setEditable(false);
			// 		oView.byId("inpu41").setEditable(false);
			// 		oView.byId("inpu42").setEditable(false);
			// 		oView.byId("inpu43").setEditable(false);
			// 		oView.byId("inpu44").setEditable(false);
			// 		oView.byId("inpu25").setEditable(false);
			// 		oView.byId("inpu251").setEditable(false);
			// 	}
			// } else {
			// 	if (oOrderType === "ZS20") {
			// 		oView.byId("inpu19").setVisible(false);
			// 		oView.byId("inpu20").setVisible(false);
			// 	} else {
			// 		oView.byId("inpu19").setVisible(true);
			// 		oView.byId("inpu20").setVisible(true);
			// 	}

			// }
		},
		enableElements: function () {
			var oView = this.getView();

			oView.byId("inpu2").setVisible(true);
			oView.byId("inpu201").setVisible(true);
			oView.byId("inpu3").setVisible(true);
			oView.byId("inpu4").setVisible(true);
			oView.byId("inpu5").setVisible(true);
			oView.byId("inpu6").setVisible(true);
			oView.byId("inpu7").setVisible(true);
			oView.byId("inpu8").setVisible(true);
			oView.byId("inpu11").setVisible(true);
			oView.byId("inpu14").setVisible(true);
			oView.byId("inpu15").setVisible(true);
			oView.byId("inpu16").setVisible(true);
			oView.byId("inpu17").setVisible(true);
			oView.byId("inpu18").setVisible(true);
			oView.byId("inpu19").setVisible(true);
			oView.byId("inpu20").setVisible(true);
			oView.byId("inpu21").setVisible(true);
			oView.byId("inpu22").setVisible(true);
			oView.byId("inpu23").setVisible(true);
			oView.byId("inpu24").setVisible(true);
			oView.byId("inpu25").setVisible(true);
			oView.byId("inpu251").setVisible(true);
			oView.byId("inpu26").setVisible(true);
			oView.byId("inpu27").setVisible(true);
			oView.byId("inpu28").setVisible(true);
			oView.byId("inpu29").setVisible(true);
			oView.byId("inpu30").setVisible(true);
			oView.byId("inpu31").setVisible(true);
			oView.byId("inpu32").setVisible(true);
			oView.byId("inpu33").setVisible(true);
			oView.byId("inpu34").setVisible(true);
			oView.byId("inpu35").setVisible(true);
			oView.byId("inpu36").setVisible(true);
			oView.byId("inpu37").setVisible(true);
			oView.byId("inpu38").setVisible(true);
			oView.byId("inpu39").setVisible(true);
			oView.byId("inpu40").setVisible(true);
			oView.byId("inpu41").setVisible(true);
			oView.byId("inpu42").setVisible(true);
			oView.byId("inpu43").setVisible(true);
			oView.byId("inpu44").setVisible(true);

			oView.byId("IconDeger").setVisible(true);
			oView.byId("IconKamp").setVisible(true);
			oView.byId("IconTahsilat").setVisible(true);
			oView.byId("IconDegisim").setVisible(true);
			oView.byId("idTahsScroll").setVisible(true);
			oView.byId("IconNotlar").setVisible(true);

			oView.byId("inpu2").setEditable(true);
			oView.byId("inpu3").setEditable(true);
			oView.byId("inpu4").setEditable(true);
			oView.byId("inpu5").setEditable(true);
			oView.byId("inpu6").setEditable(true);
			oView.byId("inpu7").setEditable(true);
			oView.byId("inpu8").setEditable(true);
			oView.byId("inpu11").setEditable(true);
			oView.byId("inpu14").setEditable(true);
			oView.byId("inpu15").setEditable(true);
			oView.byId("inpu16").setEditable(true);
			oView.byId("inpu17").setEditable(true);
			oView.byId("inpu18").setEditable(true);
			oView.byId("inpu19").setEditable(true);
			oView.byId("inpu20").setEditable(true);
			oView.byId("inpu21").setEditable(true);
			oView.byId("inpu22").setEditable(true);
			oView.byId("inpu23").setEditable(true);
			oView.byId("inpu24").setEditable(true);
			oView.byId("inpu25").setEditable(true);
			oView.byId("inpu251").setEditable(true);
			oView.byId("inpu26").setEditable(true);
			oView.byId("inpu27").setEditable(true);
			oView.byId("inpu28").setEditable(true);
			oView.byId("inpu29").setEditable(true);
			oView.byId("inpu30").setEditable(true);
			oView.byId("inpu31").setEditable(true);
			oView.byId("inpu32").setEditable(true);
			oView.byId("inpu33").setEditable(true);
			oView.byId("inpu34").setEditable(true);
			oView.byId("inpu35").setEditable(true);
			oView.byId("inpu36").setEditable(true);
			oView.byId("inpu37").setEditable(true);
			oView.byId("inpu38").setEditable(true);
			oView.byId("inpu39").setEditable(true);
			oView.byId("inpu40").setEditable(true);
			oView.byId("inpu41").setEditable(true);
			oView.byId("inpu42").setEditable(true);
			oView.byId("inpu43").setEditable(true);
			oView.byId("inpu44").setEditable(true);

		},

		bindElements: function (oData) {
			var oView = this.getView();

			var oPartners = oData.ToPartners.results;
			var oTexts = oData.ToTexts.results;

			if (oPartners) {
				for (var i = 0; i < oPartners.length; i++) {
					if (oPartners[i].PartnerFct === "00000001") {
						oView.byId("inpu2").setValue(oPartners[i].PartnerNo);
						oView.byId("inpu3").setSelectedKey(oPartners[i].AddressShort);
						oView.byId("inpu201").setValue(oPartners[i].DescriptionName);
					}
					if (oPartners[i].PartnerFct === "00000015") {
						oView.byId("inpu11").setValue(oPartners[i].PartnerNo);
					}
					if (oPartners[i].PartnerFct === "00000002") {
						oView.byId("inpu61").setValue(oPartners[i].AddressShort);
					}
				}
			}

			if (oTexts) {
				for (i = 0; i < oTexts.length; i++) {
					if (oTexts[i].Tdid === "0001") {
						oView.byId("inpu44").setValue(oTexts[i].ConcLines);
					}
				}
			}

			if (oData.Guid === "00000000-0000-0000-0000-000000000000") {
				oView.byId("idTitle").setText("Kayıt Yarat");
				// Yeni kayıtlarda bazı alanlar default gelecek.
				oView.byId("inpu21").setDateValue(new Date());
				oView.byId("inpu22").setDateValue(new Date());
				oView.byId("inpu23").setDateValue(new Date());
			} else {
				oView.byId("idTitle").setText("Kayıt Güncelle");
			}

			if (oData.ToPricing.Currency === "") oData.ToPricing.Currency = "TRY";

		},

		toBack: function (oEvent) {
			sap.ui.getCore().setModel(this.getView().getModel("oModel"), "oModelGlobal");
			sap.ui.core.UIComponent.getRouterFor(this).navTo("InquirePrice", true);
			var oView = this.getView();

			oView.byId("inpu2").setValueState(sap.ui.core.ValueState.None);
			oView.byId("inpu4").setValueState(sap.ui.core.ValueState.None);
			oView.byId("inpu8").setValueState(sap.ui.core.ValueState.None);
			oView.byId("inpu14").setValueState(sap.ui.core.ValueState.None);
			oView.byId("inpu24").setValueState(sap.ui.core.ValueState.None);
			oView.byId("inpu25").setValueState(sap.ui.core.ValueState.None);
			oView.byId("inpu251").setValueState(sap.ui.core.ValueState.None);

			oView.byId("inpu2").setValue("");
			oView.byId("inpu201").setValue("");
			oView.byId("inpu3").setSelectedKey("");
			oView.byId("inpu5").setValue("");
			oView.byId("inpu11").setValue("");
			oView.byId("inpu44").setValue("");
			oView.byId("inpu61").setValue("");

		},
		handleItemEnter: function (oEvent) {

			var oInd = oEvent.getSource().getParent().getBindingContextPath().slice(-1);
			var oTab = this.getView().byId("SepetTabId").getModel("oModel").getProperty("/ToItems/results");
			if (oTab[oInd].OrderedProd !== "" && oTab[oInd].Quantity > "0.000") {
				this.onSave(false);
			}
		},
		onPressSave: function () {
			this.onSave(true);
		},

		onSave: function (oSave) {

			var oView = this.getView();

			var oModel = this.getView().getModel("oModel");

			if (this.alanKontrol(oSave) === "true" || !oSave) {

				var oHeaderSet = this.clone(oModel.oData);

				delete oHeaderSet.__metadata;

				delete oHeaderSet.ToStatus.__metadata;
				delete oHeaderSet.ToCustomerExt.__metadata;
				delete oHeaderSet.ToPricing.__metadata;
				delete oHeaderSet.ToCumulate.__metadata;
				delete oHeaderSet.ToCondEasyEntries.__metadata;
				delete oHeaderSet.ToCondEasyEntries.__deferred;
				delete oHeaderSet.ToSales.__metadata;
				delete oHeaderSet.ToSales.__deferred;

				delete oHeaderSet.ToConfig;
				delete oHeaderSet.ToOrgman;
				delete oHeaderSet.ToDates;

				oHeaderSet.ToCondEasyEntries.Header = oHeaderSet.Guid;
				oHeaderSet.ToCustomerExt.Guid = oHeaderSet.Guid;
				oHeaderSet.ToCumulate.Guid = oHeaderSet.Guid;
				oHeaderSet.ToPricing.Guid = oHeaderSet.Guid;
				oHeaderSet.ToSales.Guid = oHeaderSet.Guid;
				oHeaderSet.ToCumulate.Guid = oHeaderSet.Guid;

				if (oSave === true) oHeaderSet.Save = "X";

				if (oHeaderSet.ToPartners.results) {
					oHeaderSet.ToPartners = oHeaderSet.ToPartners.results;
					oHeaderSet.ToTexts = oHeaderSet.ToTexts.results;
				}

				this.getData(oHeaderSet);

				if (oView.byId("inpu15").getSelected()) {
					oHeaderSet.Zzfld00008e = "X";
				} else {
					oHeaderSet.Zzfld00008e = "";
				}
				if (oView.byId("inpu34").getSelected()) {
					oHeaderSet.DistributedI1006 = "X";
				} else {
					oHeaderSet.DistributedI1006 = "";
				}
				if (oView.byId("inpu41").getSelected()) {
					oHeaderSet.ToCustomerExt.Zzfld0000ag = "X";
				} else {
					oHeaderSet.ToCustomerExt.Zzfld0000ag = "";
				}
 
				oHeaderSet.ToItems = oHeaderSet.ToItems.results; //Sepet;
				oHeaderSet.ToTahsilat = oHeaderSet.ToTahsilat.results; //Tahsilat;
				oHeaderSet.ToMessages = oHeaderSet.ToMessages.results;

				for (var j = 0; j < oHeaderSet.ToTahsilat.length; j++) {
					if (oHeaderSet.ToTahsilat[j].Zzfld00009h === true) {
						oHeaderSet.ToTahsilat[j].Zzfld00009h = "X";
					} else {
						oHeaderSet.ToTahsilat[j].Zzfld00009h = "";
					}
					if (oHeaderSet.ToTahsilat[j].Zzfld0000bf === true) {
						oHeaderSet.ToTahsilat[j].Zzfld0000bf = "X";
					} else {
						oHeaderSet.ToTahsilat[j].Zzfld0000bf = "";
					}

				}
                this.oSave = oHeaderSet.Save;
				var oDataModel = this.getView().byId("SepetTabId").getModel();
				var that = this;
				var oBusyDialog = new sap.m.BusyDialog();
				oBusyDialog.open();

				oDataModel.create("/headerSet", oHeaderSet, {
					success: function (oData, oResponse) {
						var oModelJsonList = new sap.ui.model.json.JSONModel();
						that.showMessages(oData.ToMessages);
						that.fixTabs(oData);
						oModelJsonList.setData(oData);
						oView.setModel(oModelJsonList, "oModel");
						oBusyDialog.close();
						if (that.oSave === "X") {
							var message = "Kayıt işlemi başarıyla tamamlanmıştır.";
							sap.m.MessageBox.show(message, {
								icon: sap.m.MessageBox.Icon.SUCCESS,
								title: "Başarılı",
								actions: [sap.m.MessageBox.Action.OK]
							});
						}
					},
					error: function (oError) {
						oBusyDialog.close();
						var message = oError.responseText;
						sap.m.MessageBox.show(message, {
							icon: sap.m.MessageBox.Icon.ERROR,
							title: "HATA",
							actions: [sap.m.MessageBox.Action.OK]
						});
					}
				});
			} else {
				var oMessage = "Lütfen Tüm Zorunlu Alanları Doldurunuz";
				sap.m.MessageBox.show(oMessage, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "UYARI",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}

		},

		getData: function (oHeaderSet) {
			var oView = this.getView();
			var oPartners1 = {};
			var oPartners2 = {};
			var oPartners3 = {};
			var oText = {};
			var oPartner1 = false;
			var oPartner2 = false;
			var oPartner7 = false;
			var oText1 = false;

			for (var i = 0; i < oHeaderSet.ToPartners.length; i++) {
				delete oHeaderSet.ToPartners[i].__metadata;
				if (oHeaderSet.ToPartners[i].PartnerFct === "00000001") {
					oHeaderSet.ToPartners[i].PartnerNo = oView.byId("inpu2").getValue();
					oHeaderSet.ToPartners[i].AddressShort = oView.byId("inpu3").getSelectedKey();
					oPartner1 = true;
				}
				if (oHeaderSet.ToPartners[i].PartnerFct === "00000002") {
					if (oView.byId("inpu5").getSelectedKey() !== "") {
						oHeaderSet.ToPartners[i].PartnerNo = oView.byId("inpu5").getSelectedKey();
					} else {
						oHeaderSet.ToPartners[i].PartnerNo = oView.byId("inpu2").getValue();
					}
					oHeaderSet.ToPartners[i].AddressShort = oView.byId("inpu61").getValue();
					oPartner2 = true;
				}
				if (oHeaderSet.ToPartners[i].PartnerFct === "00000015") {
					oHeaderSet.ToPartners[i].PartnerNo = oView.byId("inpu11").getValue();
					oPartner7 = true;
				}
			}

			if (!oPartner1) {
				oPartners1.PartnerFct = "00000001";
				oPartners1.PartnerNo = oView.byId("inpu2").getValue();
				oPartners1.Guid = oHeaderSet.Guid;
				oPartners1.AddressShort = oView.byId("inpu3").getSelectedKey();
				oHeaderSet.ToPartners.push(oPartners1);
			}
			if (!oPartner2) {
				oPartners2.PartnerFct = "00000002";
				if (oView.byId("inpu5").getSelectedKey() !== "") {
					oPartners2.PartnerNo = oView.byId("inpu5").getSelectedKey();
				} else {
					oPartners2.PartnerNo = oView.byId("inpu2").getValue();
				}

				oPartners2.Guid = oHeaderSet.Guid;
				oPartners2.AddressShort = oView.byId("inpu61").getValue();
				oHeaderSet.ToPartners.push(oPartners2);
			}
			if (!oPartner7 && oView.byId("inpu11") !== "") {
				oPartners3.PartnerFct = "00000015";
				oPartners3.PartnerNo = oView.byId("inpu11").getValue();
				oPartners3.Guid = oHeaderSet.Guid;
				oPartners3.DescriptionName = oView.byId("inpu11").getValue();
				oHeaderSet.ToPartners.push(oPartners3);
			}

			for (i = 0; i < oHeaderSet.ToTexts.length; i++) {
				delete oHeaderSet.ToTexts[i].__metadata;
				if (oHeaderSet.ToTexts[i].Tdid === "0001") {
					oHeaderSet.ToTexts[i].ConcLines = oView.byId("inpu44").getValue();
					oText1 = true;
				}
			}

			if (!oText1) {
				oText.Tdid = "0001";
				oText.ConcLines = oView.byId("inpu44").getValue();
				oHeaderSet.ToTexts.push(oText);
			}
		},

		toAddSepet: function () {
			if (this.getView().getModel("oModel").oData.ToStatus.Status === 'E0002' || this.oGuid ===
				"00000000-0000-0000-0000-000000000000") {
				var oRow = {
					OrderedProd: "",
					Description: "",
					Quantity: "0.000",
					ProcessQtyUnit: "ADT",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: this.getView().byId("inpu251").getSelectedKey(),
					TeslimatTrh: this.getView().byId("inpu24").getDateValue(),
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: "",
					ShowConfigButton: "X"
				};
				var oTab = this.getView().byId("SepetTabId");
				var List = oTab.getModel("oModel").getProperty("/ToItems/results");
				List.push(oRow);
				oTab.getModel("oModel").setProperty("/ToItems/results", List);
				oTab.getModel("oModel").refresh();

			} else {
				var oMessage = "Yalnızca E0002 statüsü için sepet verileri değiştirilebilir.";
				sap.m.MessageBox.show(oMessage, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "UYARI",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}

		},
		deleteSepetRow: function (oEvent) {

			if (this.getView().getModel("oModel").oData.ToStatus.Status === 'E0002' || this.oGuid ===
				"00000000-0000-0000-0000-000000000000") {

				var oTab = this.getView().byId("SepetTabId");
				var path = oEvent.getParameter("listItem").getBindingContext("oModel").getPath();
				var idx = parseInt(path.substring(path.lastIndexOf("/") + 1));
				oTab.getModel("oModel").oData.ToItems.results.splice(idx, 1);
				oTab.getModel("oModel").refresh();

			} else {
				var oMessage = "Yalnızca E0002 statüsü için sepet verileri değiştirilebilir.";
				sap.m.MessageBox.show(oMessage, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "UYARI",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}

		},
		alanKontrol: function (oSave) {
			var oView = this.getView();
			var oVal = "true";

			if (oSave) {
				if (!oView.byId("inpu2").getValue() && oView.byId("inpu2").getEditable() && oView.byId("inpu2").getVisible()) {
					oView.byId("inpu2").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu2").setValueState(sap.ui.core.ValueState.None);
				}
				if (!oView.byId("inpu4").getSelectedKey() && oView.byId("inpu4").getEditable() && oView.byId("inpu4").getVisible()) {
					oView.byId("inpu4").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu4").setValueState(sap.ui.core.ValueState.None);
				}
				if (!oView.byId("inpu8").getValue() && oView.byId("inpu8").getEditable() && oView.byId("inpu8").getVisible()) {
					oView.byId("inpu8").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu8").setValueState(sap.ui.core.ValueState.None);
				}
				if (!oView.byId("inpu14").getValue() && oView.byId("inpu14").getEditable() && oView.byId("inpu14").getVisible()) {
					oView.byId("inpu14").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu14").setValueState(sap.ui.core.ValueState.None);
				}
				if (!oView.byId("inpu24").getDateValue() && oView.byId("inpu24").getEditable() && oView.byId("inpu24").getVisible()) {
					oView.byId("inpu24").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu24").setValueState(sap.ui.core.ValueState.None);
				}
				if (!oView.byId("inpu25").getValue() && oView.byId("inpu25").getEditable() && oView.byId("inpu25").getVisible()) {
					oView.byId("inpu25").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu25").setValueState(sap.ui.core.ValueState.None);
				}
				if (!oView.byId("inpu251").getSelectedKey() && oView.byId("inpu251").getEditable() && oView.byId("inpu251").getVisible()) {
					oView.byId("inpu251").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu251").setValueState(sap.ui.core.ValueState.None);
				}
			}

			return oVal;
		},

		toAddTahsilat: function () {

			var oOdenecekTutar = "0.000";
			var oToplamTutar = "0.000";
			var oOdenenTutar = "0.000";

			var oTip = "OT-10";

			var oTab = this.getView().byId("TahsilatTabId");
			var oTahsilatList = oTab.getModel("oModel").getProperty("/ToTahsilat");
			var oSepetList = this.getView().byId("SepetTabId").getModel("oModel").getProperty("/ToItems/results");

			if (oSepetList) var oLength = oSepetList.length;
			else oLength = 0;

			for (var i = 0; i < oLength; i++) {
				oToplamTutar = parseFloat(oToplamTutar) + parseFloat(oSepetList[i].Brmfytkdvli);
			}

			if (oTahsilatList) oLength = oTahsilatList.results.length;
			else {
				oLength = 0;
				oTahsilatList = {
					results: []
				};
			}
			if (oLength === 0) {
				oOdenecekTutar = oToplamTutar;
			} else {
				oTip = "OT-" + ((oLength + 1) * 10);

				for (i = 0; i < oLength; i++) {
					if (oTahsilatList.results[i].Zzfld000044 !== "01" && oTahsilatList.results[i].Zzfld000044 !== "02" &&
						oTahsilatList.results[i].Zzfld000044 !== "03" && oTahsilatList.results[i].Zzfld000044 !== "10" &&
						oTahsilatList.results[i].Zzfld000044 !== "11") {
						oOdenenTutar = parseFloat(oOdenenTutar) + parseFloat(oTahsilatList.results[i].Zzfld00007y);
					}
				}
				oOdenecekTutar = parseFloat(oToplamTutar) - parseFloat(oOdenenTutar);
			}

			if (oOdenecekTutar === 0) oOdenecekTutar = "0.000";

			var oRow = {
				Zzfld00005v: oTip,
				Zzfld00005w: "",
				Zzfld000085: "",
				Zzfld000044: "", ////
				Zzfld00007u: "",
				Zzfld000081: "",
				Zzfld0000an: "",
				Zzfld0000at: "",
				Zzfld0000c2: "",
				Zzfld000045: "",
				Zzfld00007y: oOdenecekTutar,
				Zzfld0000bp: oOdenecekTutar,
				Zzfld00007w: "0.000",
				Zzfld00007x: "0.000",
				Zzfld00009h: false,
				Zzfld0000bf: false
			};

			oTahsilatList.results.push(oRow);
			oTab.getModel("oModel").setProperty("/ToTahsilat", oTahsilatList);
			oTab.getModel("oModel").refresh();
		},

		deleteTahsilatRow: function (oEvent) {
			var oTab = this.getView().byId("TahsilatTabId");
			var path = oEvent.getParameter("listItem").getBindingContext("oModel").getPath();
			var idx = parseInt(path.substring(path.lastIndexOf("/") + 1));

			if (oTab.getModel("oModel").oData.ToTahsilat.results[idx].Zzfld00009h === "" ||
				oTab.getModel("oModel").oData.ToTahsilat.results[idx].Zzfld00009h === false) {
				oTab.getModel("oModel").oData.ToTahsilat.results.splice(idx, 1);
				oTab.getModel("oModel").refresh();
			} else {
				var oMessage = "Kasa kontrol işaretli olan kayıtlar silinemez.";
				sap.m.MessageBox.show(oMessage, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "UYARI",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}

		},

		handleTahsilatEnter: function (oEvent) {
			var oInd = oEvent.getSource().getParent().getBindingContextPath().slice(-1);
			var oModel = this.getView().byId("TahsilatTabId").getModel("oModel");
			var oTab = oModel.getProperty("/ToTahsilat/results");
			oTab[oInd].Zzfld0000bp = oTab[oInd].Zzfld00007y;
			oModel.setProperty("/ToTahsilat/results", oTab);
		},

		onOdemeTurChange: function (oEvent) {
			var oInd = oEvent.getSource().getParent().getBindingContextPath().slice(-1);
			var oModel = this.getView().byId("TahsilatTabId").getModel("oModel");
			var oTab = oModel.getProperty("/ToTahsilat/results");

			if (oTab[oInd].Zzfld000044 !== "11" && oTab[oInd].Zzfld000044 !== " ") {
				oTab[oInd].Zzfld000047 = new Date();
			}
			if (oTab[oInd].Zzfld000044 === "01" ||
				oTab[oInd].Zzfld000044 === "02" ||
				oTab[oInd].Zzfld000044 === "03" ||
				oTab[oInd].Zzfld000044 === "10" ||
				oTab[oInd].Zzfld000044 === "11") {
				oTab[oInd].Zzfld000045 = "01";
			} else if (oTab[oInd].Zzfld000044 === "04" ||
				oTab[oInd].Zzfld000044 === "05" ||
				oTab[oInd].Zzfld000044 === "06" ||
				oTab[oInd].Zzfld000044 === "07" ||
				oTab[oInd].Zzfld000044 === "08") {
				oTab[oInd].Zzfld000045 = "02";
			}

			oModel.setProperty("/ToTahsilat/results", oTab);

			if (oTab[oInd].Zzfld000044 === "01" ||
				oTab[oInd].Zzfld000044 === "02" ||
				oTab[oInd].Zzfld000044 === "03" ||
				oTab[oInd].Zzfld000044 === "10" ||
				oTab[oInd].Zzfld000044 === "11") {

				var oModelRef = this.getView().getModel("oRefId");

				var oId = {
					Key: oTab[oInd].Zzfld00005v
				};

				if (oModelRef) {
					var oDat = this.getView().getModel("oRefId").getProperty("/results");
					oDat.push(oId);
					oModelRef.setProperty("/results", oDat);
				} else {
					oDat = {
						results: []
					};
					oDat.results.push(oId);
					var oModelJsonList = new sap.ui.model.json.JSONModel(oDat);
					this.getView().setModel(oModelJsonList, "oRefId");
				}

			}

		},

		onTahsDurumChange: function (oEvent) {

			var oInd = oEvent.getSource().getParent().getBindingContextPath().slice(-1);
			var oModel = this.getView().byId("TahsilatTabId").getModel("oModel");
			var oTab = oModel.getProperty("/ToTahsilat/results");

			if (oTab[oInd].Zzfld000045 === "02" &&
				(oTab[oInd].Zzfld000044 === "01" ||
					oTab[oInd].Zzfld000044 === "02" ||
					oTab[oInd].Zzfld000044 === "03" ||
					oTab[oInd].Zzfld000044 === "10" ||
					oTab[oInd].Zzfld000044 === "11")) {

				for (var i = 0; i < oTab.length; i++) {
					if (oTab[i].Zzfld00005w === oTab[oInd].Zzfld00005v) {
						oTab[i].Zzfld000045 = "02";
					}
				}

			}

			oModel.setProperty("/ToTahsilat/results", oTab);

		},

		onBayiChange: function (oEvent) {
			var val = this.getView().byId("inpu5").getSelectedKey();
			var oAdres = this.getView().byId("inpu6");
			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var oFilters = [];
			var oFilter = new Filter("Code", FilterOperator.EQ, "0003");
			oFilters.push(oFilter);
			oFilter = new Filter("Extension", FilterOperator.EQ, val);
			oFilters.push(oFilter);
			this.getView().getModel().read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					oModelJsonList.setData(oData);
					oAdres.setModel(oModelJsonList, "oAdres");
				},
				error: function (oError) {}
			});
		},
		onSelectFromProd: function (oEvent) {

			// Teşhirden Satış Mağaza ZA15

			if (this.oOrderType === "ZA15") {

				if (sap.ui.getCore().byId("idSelectFromProd")) {
					sap.ui.getCore().byId("idSelectFromProd").destroy();
				}
				this._oSelProdHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.selectFromProd", this);
				this.getView().addDependent(this._oSelProdHelpDialog);
				this._oSelProdHelpDialog.open();

			} else {
				sap.m.MessageBox.show("Teşhirden seçme yalnızca Teşhirden Satış Mağaza - ZA15 türündeki siparişlerde geçerlidir", {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}

		},

		handleSelectProdCancel: function (oEvent) {
			this._oSelProdHelpDialog.close();
		},
		handleSelectProdSearch: function (oEvent) {

			var oModelProd = this.getView().getModel();
			var oSipNo = sap.ui.getCore().byId("idSipNo").getValue();
			var oKalemNo = sap.ui.getCore().byId("idKalemNo").getValue();
			var oUrunKod = sap.ui.getCore().byId("idUrunKod").getValue();
			var oUrunTanim = sap.ui.getCore().byId("idUrunTanim").getValue();

			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];

			if (this.oGuid !== "00000000-0000-0000-0000-000000000000") {
				oFilters.push(new Filter({
					path: 'OrderGuid',
					value1: this.oGuid,
					operator: FilterOperator.EQ
				}));
			}

			if (oSipNo) {
				oFilters.push(new Filter({
					path: 'SiparisNo',
					value1: oSipNo,
					operator: FilterOperator.EQ
				}));
			}

			if (oKalemNo) {
				oFilters.push(new Filter({
					path: 'KalemNo',
					value1: oKalemNo,
					operator: FilterOperator.EQ
				}));
			}
			if (oUrunKod) {
				oFilters.push(new Filter({
					path: 'UrunKodu',
					value1: oUrunKod,
					operator: FilterOperator.EQ
				}));
			}
			if (oUrunTanim) {
				oFilters.push(new Filter({
					path: 'UrunTanimi',
					value1: oUrunTanim,
					operator: FilterOperator.EQ
				}));
			}

			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var that = this;
			oBusyDialog.open();
			oModelProd.read("/teshirdenSecSet", {
				filters: oFilters,
				success: function (oData, response) {
					that.stockFilter(oData);
					oModelJsonList.setData(oData);
					that._oSelProdHelpDialog.setModel(oModelJsonList, "oTeshir");
					oBusyDialog.close();
				},
				error: function (oError) {
					oBusyDialog.close();
				}
			});

		},

		stockFilter: function (oData) {

			var oSepetList = this.getView().getModel("oModel").getProperty("/ToItems/results");

			for (var i = 0; i < oSepetList.length; i++) {
				if (oSepetList[i].ZzaTeshirblgno !== "" && oSepetList[i].Zzfld000083 !== "") {
					for (var j = 0; j < oData.results.length; j++) {
						if (oData.results[j].SiparisNo === oSepetList[i].ZzaTeshirblgno && oData.results[j].KalemNo === oSepetList[i].Zzfld000083) {
							oData.results[j].Stok = oData.results[j].Stok - oSepetList[i].Quantity;
							if (oData.results[j].Stok <= 0) {
								oData.results.splice(j, 1);
							}
						}
					}
				}
			}

			return oData;
		},

		handleSelectProdAdd: function (oEvent) {

			var oTab = this.getView().byId("SepetTabId");
			var List = oTab.getModel("oModel").getProperty("/ToItems/results");
			var oSelProdTab = sap.ui.getCore().byId("SelProdTabId");
			var oSelected = oSelProdTab.getSelectedItems();

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oSipNo = oSelProdTab.getItems()[oInd].getCells()[0].getText();
				var oKalemNo = oSelProdTab.getItems()[oInd].getCells()[1].getText();
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[2].getText();
				var oIndirim = oSelProdTab.getItems()[oInd].getCells()[5].getText();

				var oRow = {
					OrderedProd: oUrunKod,
					Description: "",
					Quantity: "0.000",
					ProcessQtyUnit: "ADT",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: "",
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: "",
					ShowConfigButton: "X",
					Zzfld000083: oKalemNo,
					ZzaTeshirblgno: oSipNo,
					ZzTeshirind: oIndirim
				};

				List.push(oRow);

			}

			oTab.getModel("oModel").setProperty("/ToItems/results", List);
			oTab.getModel("oModel").refresh();
			this._oSelProdHelpDialog.close();
		},

		// onProdValueHelp: function (oEvent) {

		// 	if (sap.ui.getCore().byId("idNewProdDialog")) {
		// 		sap.ui.getCore().byId("idNewProdDialog").destroy();
		// 	}

		// 	this._oProdHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.ProdSearch", this);
		// 	this.getView().addDependent(this._oProdHelpDialog);
		// 	this._oProdHelpDialog.open();

		// 	this.Source = oEvent.getSource();

		// },

		// handleProdCancel: function (oEvent) {
		// 	this._oProdHelpDialog.close();
		// },
		// handleProdSearch: function (oEvent) {

		// 	var oModelProd = this.getView().getModel("PROD");

		// 	var oProdId = sap.ui.getCore().byId("idProdId").getValue();
		// 	var oProdDes = sap.ui.getCore().byId("idProdDesc").getValue();
		// 	var oCatId = sap.ui.getCore().byId("idCatId").getValue();
		// 	var oCatDes = sap.ui.getCore().byId("idCatDesc").getValue();
		// 	var oRow = sap.ui.getCore().byId("idProdRows").getValue();

		// 	var oBusyDialog = new sap.m.BusyDialog();

		// 	var oFilters = [];
		// 	if (oProdId !== "") {
		// 		var oFilter = new Filter("ProductId", FilterOperator.EQ, oProdId);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oProdDes !== "") {
		// 		oFilter = new Filter("ShortText", FilterOperator.EQ, oProdDes);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oCatId !== "") {
		// 		oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oCatDes !== "") {
		// 		oFilter = new Filter("CategoryDesc", FilterOperator.EQ, oCatDes);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oRow !== "") {
		// 		oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
		// 		oFilters.push(oFilter);
		// 	}

		// 	var oModelJsonList = new sap.ui.model.json.JSONModel();
		// 	var that = this;
		// 	oBusyDialog.open();
		// 	oModelProd.read("/productSearchSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			oModelJsonList.setData(oData);
		// 			that._oProdHelpDialog.setModel(oModelJsonList, "oProd");
		// 			oBusyDialog.close();
		// 		},
		// 		error: function (oError) {
		// 			oBusyDialog.close();
		// 		}
		// 	});

		// },

		// handleProdListItemPress: function (oEvent) {
		// 	var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
		// 	var oProd = sap.ui.getCore().byId("ProdTabId").getItems()[oInd].getCells()[0].getText();
		// 	this.Source.setValue(oProd);
		// 	// var oProdText = sap.ui.getCore().byId("ProdTabId").getItems()[oInd].getCells()[1].getText();
		// 	// var oTextId = this.Source.getId();
		// 	// oTextId = oTextId.substr(0, 7) + "25" + oTextId.substr(9);
		// 	// sap.ui.getCore().byId(oTextId).setValue(oProdText);
		// 	this._oProdHelpDialog.close();
		// },
		// onCatValueHelp: function (oEvent) {

		// 	if (sap.ui.getCore().byId("idNewCatDialog")) {
		// 		sap.ui.getCore().byId("idNewCatDialog").destroy();
		// 	}

		// 	this._oCatHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.CatSearch", this);
		// 	this.getView().addDependent(this._oCatHelpDialog);
		// 	this._oCatHelpDialog.open();

		// },

		// handleCatCancel: function () {
		// 	this._oCatHelpDialog.close();
		// },
		// handleCatSearch: function () {

		// 	var oModelProd = this.getView().getModel("PROD");

		// 	var oCatId = sap.ui.getCore().byId("idCategId").getValue();
		// 	var oCatDes = sap.ui.getCore().byId("idCategDesc").getValue();
		// 	var oHyrId = sap.ui.getCore().byId("idHyrType").getSelectedKey();
		// 	var oHyrDes = sap.ui.getCore().byId("idHyrDes").getValue();
		// 	var oRow = sap.ui.getCore().byId("idCatRows").getValue();

		// 	var oBusyDialog = new sap.m.BusyDialog();

		// 	var oFilters = [];
		// 	if (oCatId !== "") {
		// 		var oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oCatDes !== "") {
		// 		oFilter = new Filter("CategoryText", FilterOperator.EQ, oCatDes);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oHyrId !== "") {
		// 		oFilter = new Filter("HierarchyId", FilterOperator.EQ, oHyrId);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oHyrDes !== "") {
		// 		oFilter = new Filter("HierarchyText", FilterOperator.EQ, oHyrDes);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oRow !== "") {
		// 		oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
		// 		oFilters.push(oFilter);
		// 	}

		// 	var oModelJsonList = new sap.ui.model.json.JSONModel();
		// 	var that = this;
		// 	oBusyDialog.open();
		// 	oModelProd.read("/categorySearchSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			oModelJsonList.setData(oData);
		// 			that._oCatHelpDialog.setModel(oModelJsonList, "oCat");
		// 			oBusyDialog.close();
		// 		},
		// 		error: function (oError) {
		// 			oBusyDialog.close();
		// 		}
		// 	});

		// },

		// handleCatListItemPress: function (oEvent) {
		// 	var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
		// 	var oCatid = sap.ui.getCore().byId("CatTabId").getItems()[oInd].getCells()[0].getText();
		// 	sap.ui.getCore().byId("idCatId").setValue(oCatid);
		// 	this._oCatHelpDialog.close();
		// },
		// onCustomerValueHelp: function () {

		// 	if (sap.ui.getCore().byId("idNewCustDialog")) {
		// 		sap.ui.getCore().byId("idNewCustDialog").destroy();
		// 	}

		// 	this._oValueHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.CustomerSearch", this);
		// 	this.getView().addDependent(this._oValueHelpDialog);
		// 	this._oValueHelpDialog.open();

		// },

		// handleCustCancel: function () {
		// 	this._oValueHelpDialog.close();
		// },
		// handleCustomerSearch: function () {

		// 	var oModelCust = this.getView().getModel("CUST");

		// 	var oPartner = sap.ui.getCore().byId("idCustIdent").getValue();
		// 	var oAd = sap.ui.getCore().byId("idCustName").getValue();
		// 	var oSoyad = sap.ui.getCore().byId("idCustLastName").getValue();
		// 	var oTel = sap.ui.getCore().byId("idCustTel").getValue();
		// 	var oTur = sap.ui.getCore().byId("idCustType").getSelectedKey();
		// 	var oBusyDialog = new sap.m.BusyDialog();

		// 	var oFilters = [];
		// 	if (oPartner !== "") {
		// 		var oFilter = new Filter("Partner", FilterOperator.EQ, oPartner);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oAd !== "") {
		// 		oFilter = new Filter("Firstname", FilterOperator.EQ, oAd);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oSoyad !== "") {
		// 		oFilter = new Filter("Lastname", FilterOperator.EQ, oSoyad);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oTel !== "") {
		// 		oFilter = new Filter("Telephonemob", FilterOperator.EQ, oTel);
		// 		oFilters.push(oFilter);
		// 	}
		// 	if (oTur !== "") {
		// 		oFilter = new Filter("Partnertype", FilterOperator.EQ, oTur);
		// 		oFilters.push(oFilter);
		// 	}

		// 	var oModelJsonList = new sap.ui.model.json.JSONModel();
		// 	var that = this;
		// 	oBusyDialog.open();
		// 	oModelCust.read("/searchCustomerSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			oModelJsonList.setData(oData);
		// 			that._oValueHelpDialog.setModel(oModelJsonList, "oCustomer");
		// 			oBusyDialog.close();
		// 		},
		// 		error: function (oError) {
		// 			oBusyDialog.close();
		// 		}
		// 	});

		// },

		// handleCustomerListItemPress: function (oEvent) {
		// 	var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
		// 	var oPartner = sap.ui.getCore().byId("CustTabId").getItems()[oInd].getCells()[0].getText();
		// 	var oPartnerName = sap.ui.getCore().byId("CustTabId").getItems()[oInd].getCells()[1].getText();
		// 	this.getView().byId("inpu2").setValue(oPartner);
		// 	this.getView().byId("inpu201").setValue(oPartnerName);

		// 	var oFilters = [];

		// 	var oFilter = new Filter("Code", FilterOperator.EQ, "0014");
		// 	oFilters.push(oFilter);
		// 	oFilter = new Filter("Extension", FilterOperator.EQ, this.oOrderType);
		// 	oFilters.push(oFilter);
		// 	oFilter = new Filter("Extension2", FilterOperator.EQ, oPartner);
		// 	oFilters.push(oFilter);

		// 	var oMAdres = this.getView().byId("inpu3");
		// 	var oModelJsonList = new sap.ui.model.json.JSONModel();
		// 	this.getView().getModel().read("/valueHelpSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			oModelJsonList.setData(oData);
		// 			oMAdres.setModel(oModelJsonList, "oMusteriAdres");
		// 		},
		// 		error: function (oError) {

		// 		}
		// 	});

		// 	this._oValueHelpDialog.close();
		// },

		// handleCustEnter: function () {
		// 	var oPartner = this.getView().byId("inpu2").getValue();
		// 	var oModelCust = this.getView().getModel("CUST");
		// 	var oFilters = [];

		// 	if (oPartner !== "") {
		// 		var oFilter = new Filter("Partner", FilterOperator.EQ, oPartner);
		// 		oFilters.push(oFilter);
		// 	}

		// 	var that = this;

		// 	oModelCust.read("/searchCustomerSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			if (oData.results.length > 0) {
		// 				var oPartnerName = oData.results[0].Firstname + " " + oData.results[0].Lastname;
		// 				that.getView().byId("inpu201").setValue(oPartnerName);
		// 			} else {
		// 				that.getView().byId("inpu201").setValue("");
		// 			}
		// 		}

		// 	});

		// 	oFilters = [];
		// 	oFilter = new Filter("Code", FilterOperator.EQ, "0014");
		// 	oFilters.push(oFilter);
		// 	oFilter = new Filter("Extension", FilterOperator.EQ, this.oOrderType);
		// 	oFilters.push(oFilter);
		// 	oFilter = new Filter("Extension2", FilterOperator.EQ, oPartner);
		// 	oFilters.push(oFilter);

		// 	var oMAdres = this.getView().byId("inpu3");
		// 	var oModelJsonList = new sap.ui.model.json.JSONModel();
		// 	this.getView().getModel().read("/valueHelpSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			oModelJsonList.setData(oData);
		// 			oMAdres.setModel(oModelJsonList, "oMusteriAdres");
		// 		},
		// 		error: function (oError) {

		// 		}
		// 	});

		// },
		onItemConfig: function (oEvent) {

			var oInd = oEvent.getSource().getBindingContext("oModel").getPath().slice(-1);
			var oItems = this.getView().getModel("oModel").getProperty("/ToItems/results");
			var oItem = oItems[oInd];
			var oFilters = [];

			if (oItem.Parent && oItem.Parent !== "00000000-0000-0000-0000-000000000000") {
				var oFilter = new Filter("ParentId", FilterOperator.EQ, oItem.Parent);
				oFilters.push(oFilter);
			} else {
				oItem.NumberInt = (parseInt(oInd) + 1) * 10;
				oFilter = new Filter("ZzorderedProd", FilterOperator.EQ, oItem.OrderedProd);
				oFilters.push(oFilter);
				oFilter = new Filter("ItemNoForConfig", FilterOperator.EQ, oItem.NumberInt);
				oFilters.push(oFilter);
			}

			if (sap.ui.getCore().byId("idItemConfDialog")) {
				sap.ui.getCore().byId("idItemConfDialog").destroy();
			}

			this._oConfigScreen = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.ItemConfig", this);
			this.getView().addDependent(this._oConfigScreen);
			this._oConfigScreen.open();

			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var that = this;

			this.getView().getModel().read("/itemConfigSet", {
				filters: oFilters,
				success: function (oData, response) {
					oModelJsonList.setData(oData);
					that._oConfigScreen.setModel(oModelJsonList, "oConfig");
					that.bindCharacteristics(oData);
				},
				error: function (oError) {
					var message = oError.responseText;
					sap.m.MessageBox.show(message, {
						icon: sap.m.MessageBox.Icon.ERROR,
						title: "HATA",
						actions: [sap.m.MessageBox.Action.OK]
					});
				}
			});

		},
		bindCharacteristics: function (oData) {

		},
		handleConfigSave: function () {

			this._oConfigScreen.close();
		},
		handleConfigCancel: function () {
			this._oConfigScreen.close();
		},

		toSenetOlustur: function () {

			var message;
			var oCurDate = new Date();
			var oTab = this.getView().byId("TahsilatTabId");
			var oTahsilatList = oTab.getModel("oModel").getProperty("/ToTahsilat/results");

			var oRow = oTahsilatList[oTahsilatList.length - 1];

			if (this.getView().getModel("oModel").getProperty("/ToPricing").PriceList === "02") {
				message = "Kampanyalı fiyat üzerinden senetli satış yapılamaz!";
				sap.m.MessageBox.show(message, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			} else if (oRow.Zzfld0000c2 === "") {
				message = "Lütfen senet adedi giriniz";
				sap.m.MessageBox.show(message, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			} else if (oRow.Zzfld0000c2 > 12) {
				message = "En fazla 12 adet senet oluşturabilirsiniz";
				sap.m.MessageBox.show(message, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			} else if (oRow.Zzfld0000c3.getFullYear() === "") {
				message = "Senet oluşturmadan önce ilk senet tarihini giriniz";
				sap.m.MessageBox.show(message, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			} else if (Math.ceil(Math.abs(oRow.Zzfld0000c3.getTime() - oCurDate.getTime()) / (1000 * 60 * 60 * 24)) > 45) {
				message = "İlk senet tarihi 45 günden ileri bir tarih seçilemez";
				sap.m.MessageBox.show(message, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			} else {

				var oCount = oRow.Zzfld0000c2;
				var oTip;
				var oBirimMiktar = oRow.Zzfld00007y / oCount;
				var oTarih = oRow.Zzfld0000c3;
				var oYeniTarih = new Date(oTarih.valueOf());

				if (oRow.Zzfld000044 === "11") {
					var oTDUrum = "01";
				} else {
					oTDUrum = "";
				}

				while (oCount > 0) {
					oCount--;
					oTip = "OT-" + ((oTahsilatList.length + 1) * 10);

					var oRowNew = {
						Zzfld00005v: oTip,
						Zzfld00005w: oRow.Zzfld00005v,
						Zzfld000085: "",
						Zzfld000044: "10", ////
						Zzfld00007u: "",
						Zzfld000081: "",
						Zzfld0000an: "",
						Zzfld0000at: "",
						Zzfld0000c2: "",
						Zzfld0000c4: new Date(oYeniTarih),
						Zzfld000045: oTDUrum,
						Zzfld00007y: oBirimMiktar,
						Zzfld00007w: "0.000",
						Zzfld00007x: "0.000",
						Zzfld00009h: "",
						Zzfld0000bf: ""
					};
					oTahsilatList.push(oRowNew);
					oYeniTarih.setMonth(oYeniTarih.getMonth() + 1);
				}
				oTab.getModel("oModel").setProperty("/ToTahsilat/results", oTahsilatList);
				oTab.getModel("oModel").refresh();

			}

		},
		oPerSozlSelect: function () {

			var oView = this.getView();

			var oHead = oView.getModel("oModel").getProperty("/");

			if (oView.byId("inpu15").getSelected() && oHead.ProcessType === "ZA04") {

				var oMusteri = this.getView().byId("inpu2").getValue();
				var oFilters = [];
				var oFilter = new Filter("Code", FilterOperator.EQ, "0002");
				oFilters.push(oFilter);
				oFilter = new Filter("Extension", FilterOperator.EQ, oMusteri);
				oFilters.push(oFilter);

				oView.getModel().read("/defaultValuesSet", {
					filters: oFilters,
					success: function (oData, response) {
						oHead.ZzPersiptop = oData.ZzPersiptop;
						oView.getModel("oModel").setProperty("/", oHead);
					},
					error: function (oError) {}
				});

			}

		},
		showMessages: function (oMessages) {
			if (oMessages) {
				if (oMessages.results.length > 0) {
					if (sap.ui.getCore().byId("idStockControl")) {
						sap.ui.getCore().byId("idStockControl").destroy();
					}

					this._oStockDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.StockControl", this);
					this.getView().addDependent(this._oStockDialog);
					this._oStockDialog.open();

				}
			}
		},
		onTahsInd: function () {

			var oTahsInd;
			var oTahsIndSonra;

			var oView = this.getView();
			var oTahsIndOnce = oView.byId("inpu35").getValue();
			var oIsk = oView.byId("inpu32").getValue();
			var bIsk = oView.byId("inpu33").getValue();
			var oTahsilatList = oView.getModel("oModel").getProperty("/ToTahsilat/results");

			if (oIsk) {
				oTahsInd = oIsk;
			}

			if (bIsk) {
				oTahsInd = parseFloat(oTahsInd) + parseFloat(bIsk);
			}

			for (var i = 0; i < oTahsilatList.length; i++) {
				if (oTahsilatList[i].Zzfld00007x) oTahsInd = parseFloat(oTahsInd) + parseFloat(oTahsilatList[i].Zzfld00007x);
			}

			oView.byId("inpu37").setValue(oTahsInd);

			oTahsIndSonra = oTahsIndOnce - oTahsInd;
			oView.byId("inpu36").setValue(oTahsIndSonra);

		},

		handleStockCancel: function () {
			this._oStockDialog.close();
		},
		toKampanya: function () {
			this.getView().getModel("oModel").setProperty("/KampanyaBelirle", "X");
			this.onSave(false);
		},
		onPlanUygula: function () {
			this.getView().getModel("oModel").setProperty("/PlaniUygula", "X");
			this.onSave(false);
		},
		onStokKontrol: function () {
			this.getView().getModel("oModel").setProperty("/StokSorgu", "X");
			this.onSave(false);
		},
		onSelectFromCat: function () {

			if (sap.ui.getCore().byId("idSelectCatProd")) {
				sap.ui.getCore().byId("idSelectCatProd").destroy();
			}

			this._oSelProdCatHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.selectFromCat", this);
			this.getView().addDependent(this._oSelProdCatHelpDialog);
			this._oSelProdCatHelpDialog.open();

		},

		handleSelectFromCatCancel: function (oEvent) {
			this._oSelProdCatHelpDialog.close();
		},
		handleSelectCatSearch: function (oEvent) {

			var oModelProd = this.getView().getModel("PROD");
			var oHead = this.getView().getModel("oModel").getProperty("/");
var oTakim = sap.ui.getCore().byId("idTakim").getSelectedKey();
		    var oModel = sap.ui.getCore().byId("idModel").getSelectedKey();
			// var oTakim = sap.ui.getCore().byId("idTakim").getValue();
			// var oModel = sap.ui.getCore().byId("idModel").getValue();
			var oModelTnm = sap.ui.getCore().byId("idTModelTnm").getValue();
			var oUniteUrunKod = sap.ui.getCore().byId("idUniteUrunKod").getValue();
			var oUrunId = sap.ui.getCore().byId("idUrunId").getValue();
			var oCategId = sap.ui.getCore().byId("idCategId").getValue();

			if (sap.ui.getCore().byId("idConfigEnable").getSelected()) var oConfigEnable = "X";

			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];

			if (oTakim) {
				oFilters.push(new Filter({
					path: 'Zztakim',
					value1: oTakim,
					operator: FilterOperator.EQ
				}));
			}

			if (oModelTnm) {
				oFilters.push(new Filter({
					path: 'CategoryDesc',
					value1: oModelTnm,
					operator: FilterOperator.EQ
				}));
			}
			if (oUniteUrunKod) {
				oFilters.push(new Filter({
					path: 'Description',
					value1: oUniteUrunKod,
					operator: FilterOperator.EQ
				}));
			}
			if (oModel) {
				oFilters.push(new Filter({
					path: 'Zzmodel',
					value1: oModel,
					operator: FilterOperator.EQ
				}));
			}
			if (oUrunId) {
				oFilters.push(new Filter({
					path: 'ProductId',
					value1: oUrunId,
					operator: FilterOperator.EQ
				}));
			}
			if (oCategId) {
				oFilters.push(new Filter({
					path: 'CategoryId',
					value1: oCategId,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ProcessType) {
				oFilters.push(new Filter({
					path: 'ProcessType',
					value1: oHead.ProcessType,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceList) {
				oFilters.push(new Filter({
					path: 'PriceList',
					value1: oHead.ToPricing.PriceList,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.Currency) {
				oFilters.push(new Filter({
					path: 'Currency',
					value1: oHead.ToPricing.Currency,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceDate) {
				oFilters.push(new Filter({
					path: 'PriceDate',
					value1: oHead.ToPricing.PriceDate,
					operator: FilterOperator.EQ
				}));
			}
			// if (oHead.ProcessType) {
			// 	oFilters.push(new Filter({
			// 		path: 'SoldToPartnerGuid',
			// 		value1: oConfigEnable,
			// 		operator: FilterOperator.EQ
			// 	}));
			// }

			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var that = this;
			oBusyDialog.open();
			oModelProd.read("/productCatalogSearchSet", {
				filters: oFilters,
				success: function (oData, response) {
					oModelJsonList.setData(oData);
					that._oSelProdCatHelpDialog.setModel(oModelJsonList, "oCatalog");
					oBusyDialog.close();
				},
				error: function (oError) {
					oBusyDialog.close();
				}
			});

		},

		handleSelectCatAdd: function (oEvent) {

			var oTab = this.getView().byId("SepetTabId");
			var List = oTab.getModel("oModel").getProperty("/ToItems/results");
			var oSelProdTab = sap.ui.getCore().byId("SelProdCatTabId");
			var oSelected = oSelProdTab.getSelectedItems();

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oConfig = oSelProdTab.getItems()[oInd].getCells()[0].getText();
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[4].getText();
				// var oIndirim = oSelProdTab.getItems()[oInd].getCells()[5].getText();

				var oRow = {
					OrderedProd: oUrunKod,
					Description: "",
					Quantity: "0.000",
					ProcessQtyUnit: "ADT",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: "",
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: "",
					ShowConfigButton: oConfig,
					// Zzfld000083: oKalemNo,
					// ZzaTeshirblgno: oSipNo,
					// ZzTeshirind: oIndirim
				};

				List.push(oRow);

			}

			oTab.getModel("oModel").setProperty("/ToItems/results", List);
			oTab.getModel("oModel").refresh();
			//			this._oSelProdCatHelpDialog.close();
		},

		handleSelectProdTree: function () {

			// Ürün ağacından seç arama yardımı burada ürün katoloğundan seçilen satırlar kullanıarak ürün ağacı getiririlir.
			var oModelProd = this.getView().getModel("PROD");
			var oHead = this.getView().getModel("oModel").getProperty("/");
			var oSelProdTab = sap.ui.getCore().byId("SelProdCatTabId");
			var oSelected = oSelProdTab.getSelectedItems();
			var oFilterA = [];
			var oFilterProd = [];

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[4].getText();
				if (oUrunKod) {
					oFilterProd.push(new Filter({
						path: 'ProductId',
						value1: oUrunKod,
						operator: FilterOperator.EQ,
						and: false
					}));
				}
			}

			var oFilterOR = new Filter({
				filters: oFilterProd,
				and: false,
			});

			if (oHead.ProcessType) {
				oFilterA.push(new Filter({
					path: 'ProcessType',
					value1: oHead.ProcessType,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceList) {
				oFilterA.push(new Filter({
					path: 'PriceList',
					value1: oHead.ToPricing.PriceList,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.Currency) {
				oFilterA.push(new Filter({
					path: 'Currency',
					value1: oHead.ToPricing.Currency,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceDate) {
				oFilterA.push(new Filter({
					path: 'PriceDate',
					value1: oHead.ToPricing.PriceDate,
					operator: FilterOperator.EQ
				}));
			}

			var oFilterAND = new Filter({
				filters: oFilterA,
				and: true
			});
			var oFilters = [new Filter({
				and: true,
				filters: [oFilterOR, oFilterAND]

			})];

			var oBusyDialog = new sap.m.BusyDialog();
			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var that = this;

			oBusyDialog.open();
			oModelProd.read("/productCatalogTreeSearchSet", {
				filters: oFilters,
				success: function (oData, response) {
					oModelJsonList.setData(oData);
					that._oSelProdTreeHelpDialog.setModel(oModelJsonList, "oProdTree");
					oBusyDialog.close();
				},
				error: function (oError) {
					oBusyDialog.close();
				}
			});

			if (!this._oSelProdTreeHelpDialog) {
				this._oSelProdTreeHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.SelectFromProdTree", this);
				this.getView().addDependent(this._oSelProdTreeHelpDialog);
				this._oSelProdTreeHelpDialog.open();
			} else {
				this._oSelProdTreeHelpDialog.open();
			}

		},
		handleSelectProdTreeCancel: function (oEvent) {
			this._oSelProdTreeHelpDialog.close();
		},
		handleSelectProdTreeAdd: function (oEvent) {

			var oTab = this.getView().byId("SepetTabId");
			var List = oTab.getModel("oModel").getProperty("/ToItems/results");
			var oSelProdTab = sap.ui.getCore().byId("SelProdTreeTabId");
			var oSelected = oSelProdTab.getSelectedItems();

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[0].getText();
				var oText = oSelProdTab.getItems()[oInd].getCells()[1].getText();
				var oQuantity = oSelProdTab.getItems()[oInd].getCells()[5].getText();

				var oRow = {
					OrderedProd: oUrunKod,
					Description: oText,
					Quantity: oQuantity,
					ProcessQtyUnit: "",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: "",
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: "",
					ShowConfigButton: ""
				};
				List.push(oRow);
			}

			oTab.getModel("oModel").setProperty("/ToItems/results", List);
			oTab.getModel("oModel").refresh();
			this._oSelProdTreeHelpDialog.close();
		},

	});

});