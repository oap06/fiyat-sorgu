sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"zcrm/zcrm_inquire_price/model/models",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"zcrm/zcrm_inquire_price/controller/FragmentController"
], function (UIComponent, Device, models, JSONModel,Filter,FilterOperator, FragmentController) {
	"use strict";
	
	var BLACKLIST = ["__metadata", "__deferred"];
	
	function getInitialValue(value) {
	    if (Array.isArray(value)) {
	        return [];
	    }
	    if (value && typeof value === "object" && Array.isArray(value.results)) {
	    	return {results: []};
	    }
	    if (value instanceof Date) {
	    	return new Date();
	    }
	    if (typeof value === "string" && value.match(/^\d{8}-\d{4}-\d{4}-\d{4}-\d{12}/)) {
	    	return "00000000-0000-0000-0000-000000000000";
	    }
	    var initials = {
	        "object": {},
	        "string": "",
	        "number": 0,
	        "boolean": false,
	        "undefined": undefined,
	        "function": function(){}
	    };
	    return initials[typeof value];
	}

	return UIComponent.extend("zcrm.zcrm_inquire_price.Component", {

		metadata: {
			manifest: "json"
		},
		fragments: null,

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);
			
			this.fragments = {};

			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			
			this.setModel(new JSONModel({}), "local");
			
			this.setModel(new JSONModel({}), "search");
			
			this.setModel(new JSONModel({}), "headersearch");
			

			this.setModel(new JSONModel({}), "selected");

			this.setModel(new JSONModel({}), "actionResultMessage");
			this.setModel(new JSONModel({}), "KampanyaBelirle");
			
			this.getModel("headersearch").setSizeLimit(9999999);
			
			// this._oProdHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.ProdSearch", this);
			// this.getView().addDependent(this._oProdHelpDialog);
			
			// this._oCatHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.CatSearch", this);
			// thisaddDependent(this._oCatHelpDialog);
		},
		
		exit: function() {
			Object.keys(this.fragments).forEach(function destroyFragment(sFragmentName) {
				this.fragments[sFragmentName].oDialog.destroy();
				delete this.fragments[sFragmentName];
			}.bind(this));
		},
		
		openFragment: function(sFragmentName, oView) {
			if (!this.fragments[sFragmentName]) {
				var oRootView = this.byId(this.getMetadata().getRootView().id);
				var oController = new FragmentController();
				var sPath = [this.getMetadata().getComponentName(), "fragments", sFragmentName].join(".");
				
				this.fragments[sFragmentName] = {
					oDialog: sap.ui.xmlfragment(sPath, oController),
					oController: oController
				};
				oRootView.addDependent(this.fragments[sFragmentName].oDialog);
				oController.setDialog(this.fragments[sFragmentName].oDialog);
				oController.setView(oView);
			}
			this.fragments[sFragmentName].oDialog.open();
			return this.fragments[sFragmentName].oController;
		},
		
		
		resetModel: function(oData, keepCalm, aCopyFromData) {
			Object.entries(oData).forEach(function(aEntryContainer){
				var sKey = aEntryContainer[0];
				var value = aEntryContainer[1];
				
				if (sKey in keepCalm || BLACKLIST.includes(sKey)) {
					return;
				} else if (aCopyFromData.includes(sKey)) {
					keepCalm[sKey] = oData[sKey];
				} else {
					keepCalm[sKey] = getInitialValue(value);
				}
				
			});
			return keepCalm;
		}
	});
});