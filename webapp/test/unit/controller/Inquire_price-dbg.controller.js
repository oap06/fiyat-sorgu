/*global QUnit*/

sap.ui.define([
	"zcrm/zcrm_inquire_price/controller/Inquire_price.controller"
], function (Controller) {
	"use strict";

	QUnit.module("Inquire_price Controller");

	QUnit.test("I should test the Inquire_price controller", function (assert) {
		var oAppController = new Controller();
		oAppController.onInit();
		assert.ok(oAppController);
	});

});